<?php

namespace app\models;

use yii\helpers\FileHelper;

class Directory
{
    /**
     * Create a directory along the specified path
     *
     * @param $id integer name of directory in uploads folder
     * @return string Path for directory
     */
    public function createElementsDirectory($id, $pathname) 
    {
        $path = 'uploads' . DIRECTORY_SEPARATOR . $pathname . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR;

        if(!is_dir($path)) {
            FileHelper::createDirectory($path);
            $oldmask = umask(0);
            mkdir($path, 0777);
            umask($oldmask);
        }
        return $path;
    }

    /**
     * Delete directory along the specified path
     *
     * @param $id integer name of directory in uploads folder
     */
    public function deleteDirectory($id, $path) {
        $path = 'uploads' . DIRECTORY_SEPARATOR . $path . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR;
        if(is_dir($path)) {
            //FileHelper::removeDirectory($path,['recursive'=>TRUE]);
            //$this->rmdir_recursive($path);

            foreach(scandir($path) as $file) {
                if ('.' === $file || '..' === $file) continue;
                if (is_dir("$path/$file")) rmdir_recursive("$path/$file");
                else unlink("$path/$file");
            }
            rmdir($path);
        }
    }

    /**
     * @param $dir string Path to directory
     */
    private function rmdir_recursive($dir) {
        foreach(scandir($dir) as $file) {
            if ('.' === $file || '..' === $file) continue;
            if (is_dir("$dir/$file")) rmdir_recursive("$dir/$file");
            else unlink("$dir/$file");
        }
        rmdir($dir);
    }
}