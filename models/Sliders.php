<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sliders".
 *
 * @property int $id
 * @property string $type Тип
 * @property string $title_uz Sarlavha
 * @property string $title_ru Заголовок
 * @property string $title_en Title
 * @property string $text_uz Matn
 * @property string $text_ru Текст
 * @property string $text_en Text
 * @property string $link Ссылка
 * @property string $photo Фото
 */
class Sliders extends \yii\db\ActiveRecord
{

    public $file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sliders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text_ru', 'title_ru'], 'required'],
            [['text_uz', 'text_ru', 'text_en'], 'string'],
            [['type'], 'string', 'max' => 50],
            [['title_uz', 'title_ru', 'title_en', 'link', 'photo'], 'string', 'max' => 255],
            [['file'],'file','extensions' => 'jpg,jpeg,png'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'title_uz' => 'Sarlavha',
            'title_ru' => 'Заголовок',
            'title_en' => 'Title',
            'text_uz' => 'Matn',
            'text_ru' => 'Текст',
            'text_en' => 'Text',
            'link' => 'Ссылка',
            'photo' => 'Фото',
            'file' =>'Фото'
        ];
    }

    public function afterDelete()
    {

        if ($this->photo != null && file_exists('uploads/sliders/' . $this->photo)) unlink(Yii::getAlias('uploads/sliders' . $this->photo));
        parent::afterDelete();
    }
}
