<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vacancy".
 *
 * @property int $id
 * @property string $name_uz Nomi
 * @property string $name_ru Наименование
 * @property string $name_en Name
 * @property string $text_uz Matn
 * @property string $text_ru Текст
 * @property string $text_en Text
 */
class Vacancy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vacancy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text_ru', 'name_ru'], 'required'],
            [['text_uz', 'text_ru', 'text_en'], 'string'],
            [['name_uz', 'name_ru', 'name_en'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_uz' => 'Nomi',
            'name_ru' => 'Наиминование',
            'name_en' => 'Name',
            'text_uz' => 'Matn',
            'text_ru' => 'Текст',
            'text_en' => 'Text',
        ];
    }
}
