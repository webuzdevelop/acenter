<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shops".
 *
 * @property int $id
 * @property string $logo Логотип
 * @property string $name Наименование
 * @property string $title_uz Sarlavha
 * @property string $title_ru Заголовок
 * @property string $title_en Title
 * @property string $text_uz Matn
 * @property string $text_ru Текст
 * @property string $text_en Text
 */
class Shops extends \yii\db\ActiveRecord
{

    public $file;
    public $photo1_file;
    public $photo2_file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shops';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text_ru', 'title_ru','text_top_ru', 'text_bottom_ru'], 'required'],
            [['text_uz', 'text_ru', 'text_en'], 'string'],
            [['logo', 'name' , 'photo1', 'photo2'], 'string', 'max' => 255],
            [['title_uz', 'title_ru', 'title_en'], 'string', 'max' => 500],
            [['file'], 'file' ,'extensions' => 'jpg,jpeg,png'],
            [['floor_id'],'integer']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'logo' => 'Логотип',
            'name' => 'Наименование',
            'title_uz' => 'Nomi',
            'title_ru' => 'Загаловок',
            'title_en' => 'Title',
            'text_uz' => 'Matn',
            'text_ru' => 'Текст',
            'text_en' => 'Text',
            'file' => 'Логотип',
            'floor_id' => 'Этаж',
            'photo1' => 'Фото сверху',
            'photo2' => 'Фото внизу',
            'text_top_uz' => 'Matn tepa qism',
            'text_top_ru' => 'Верхняя часть текста',
            'text_top_en' => 'Text top',
            'text_bottom_uz' => 'Matn pastki qism',
            'text_bottom_ru' => 'Нижняя часть текста',
            'text_bottom_en' => 'Text bottom',
            'photo1_file' => 'Фото сверху',
            'photo2_file' => 'Фото внизу',
        ];
    }

    public function afterDelete()
    {

        if ($this->logo != null && file_exists('uploads/shops/' . $this->logo)) unlink(Yii::getAlias('uploads/shops/' . $this->logo));
        parent::afterDelete();

        if ($this->photo1 != null && file_exists('uploads/shops/' . $this->photo1)) unlink(Yii::getAlias('uploads/shops/' . $this->photo1));
        parent::afterDelete();

        if ($this->photo2 != null && file_exists('uploads/shops/' . $this->photo2)) unlink(Yii::getAlias('uploads/shops/' . $this->photo2));
        parent::afterDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFloors()
    {
        return $this->hasOne(Floors::className(), ['id' => 'floor_id']);
    }
}
