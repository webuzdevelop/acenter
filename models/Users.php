<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $name
 * @property string $login
 * @property string $password
 * @property integer $access_for_moving
 * @property string $telephone
 * @property string $role_id
 * @property string $status
 * @property integer $atelier_id
 * @property double $sales_percent
 * @property double $cleaner_percent
 * @property double $sewing_percent
 * @property double $repairs_percent
 * @property string $auth_key
 * @property string $data_cr
 *
 * @property Atelier $atelier
 */
class Users extends \yii\db\ActiveRecord
{
    const USER_ROLE_ADMIN = 'administrator';
    const USER_ROLE_WAREHOUSE_MANAGER = 'warehouse_manager';
    const ACCESS_MOVING_YES = 1;
    const ACCESS_MOVING_NO = 0;
    const USER_STATUS_WORKING = 'working';
    const USER_STATUS_OUT_OF_SERVICE = 'out_of_service';

    public $new_password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'password', 'telephone'], 'required'],
            [['data_cr'], 'safe'],
            [['login'], 'email'],
            [['password', 'new_password'], 'string', 'max' => 255, 'min' => 6],
            [['name', 'login', 'telephone', 'role_id', 'status', 'auth_key'], 'string', 'max' => 255],
            [['login'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'login' => 'Email',
            'password' => 'Пароль',
            'telephone' => 'Телефон',
            'role_id' => 'Должность',
            'status' => 'Статус',
            'auth_key' => 'Пароль',
            'data_cr' => 'Дата регистрации',
            'new_password' => 'Новый пароль',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->auth_key = $this->password;
            $this->password = md5($this->password);
            $this->data_cr = date('Y-m-d');
        }

        if($this->new_password != null) 
        {
            $this->auth_key = $this->new_password;
            $this->password = md5($this->new_password);
        }
        return parent::beforeSave($insert);
    }

    public function getRoleList()
    {
        return ArrayHelper::map([
            ['id' => self::USER_ROLE_ADMIN, 'name' => 'Администратор',],
            ['id' => self::USER_ROLE_WAREHOUSE_MANAGER, 'name' => 'Менежер',],
        ], 'id', 'name');
    }

    public function getRoleDescription()
    {        
        if(self::USER_ROLE_ADMIN == $this->role_id) return 'Администратор';
        if(self::USER_ROLE_WAREHOUSE_MANAGER == $this->role_id) return 'Менежер';
    }


    public function getStatusList()
    {
        return ArrayHelper::map([
            ['id' => self::USER_STATUS_WORKING, 'name' => 'Работает',],
            ['id' => self::USER_STATUS_OUT_OF_SERVICE, 'name' => 'Не работает',],
        ], 'id', 'name');
    }

    
    public function getStatusDescription()
    {        
        if(self::USER_STATUS_WORKING == $this->status) return 'Работает';
        if(self::USER_STATUS_OUT_OF_SERVICE == $this->status) return 'Не работает';       
    }

    public function getAtelierList()
    {
        $ateliers = Atelier::find()->all();
        return ArrayHelper::map($ateliers, 'id', 'name');
    }

}
