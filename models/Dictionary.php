<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dictionary".
 *
 * @property int $id
 * @property string $key Ключ
 * @property string $uzb O'zbekcha
 * @property string $rus Русский
 * @property string $eng English
 */
class Dictionary extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dictionary';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uzb', 'rus', 'eng', 'key'], 'required'],
            [['uzb', 'rus', 'eng'], 'string'],
            [['key'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Ключ',
            'uzb' => 'O\'zbekcha',
            'rus' => 'Русский',
            'eng' => 'English',
        ];
    }
}
