<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property int $id
 * @property string $name ФИО или Название компании
 * @property string $email Е-mail
 * @property string $phone Телефон
 * @property string $message Сообщение
 * @property string $date_cr Дата создание
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message'], 'string'],
            [['date_cr'], 'safe'],
            [['name', 'email', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО или Название компании',
            'email' => 'Email',
            'phone' => 'Телефон',
            'message' => 'Сообщение',
            'date_cr' => 'Дата создание',
        ];
    }
}
