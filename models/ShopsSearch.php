<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Shops;

/**
 * ShopsSearch represents the model behind the search form about `app\models\Shops`.
 */
class ShopsSearch extends Shops
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['logo', 'name', 'photo1', 'photo2', 'title_uz', 'title_ru', 'title_en', 'text_uz', 'text_ru', 'text_en','text_top_uz', 'text_top_ru', 'text_top_en', 'text_bottom_uz', 'text_bottom_ru', 'text_bottom_en'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Shops::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'logo', $this->logo])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'title_uz', $this->title_uz])
            ->andFilterWhere(['like', 'title_ru', $this->title_ru])
            ->andFilterWhere(['like', 'title_en', $this->title_en])
            ->andFilterWhere(['like', 'text_uz', $this->text_uz])
            ->andFilterWhere(['like', 'text_ru', $this->text_ru])
            ->andFilterWhere(['like', 'text_en', $this->text_en])
            ->andFilterWhere(['like', 'photo1', $this->photo1])
            ->andFilterWhere(['like', 'photo2', $this->photo2])
            ->andFilterWhere(['like', 'text_top_uz', $this->text_top_uz])
            ->andFilterWhere(['like', 'text_top_ru', $this->text_top_ru])
            ->andFilterWhere(['like', 'text_top_en', $this->text_top_en])
            ->andFilterWhere(['like', 'text_bottom_uz', $this->text_bottom_uz])
            ->andFilterWhere(['like', 'text_bottom_ru', $this->text_bottom_ru])
            ->andFilterWhere(['like', 'text_bottom_en', $this->text_bottom_en]);

        return $dataProvider;
    }
}
