<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property int $type Тип
 * @property string $fone Фон
 * @property string $photo Фото
 * @property string $date Дата
 * @property string $title_uz Заголовок
 * @property string $title_ru Sarlavha
 * @property string $title_en Title
 * @property string $text_uz Matn
 * @property string $text_ru Текст
 * @property string $text_en Text
 * @property string $description_uz Ma'lumot
 * @property string $description_ru Описание
 * @property string $description_en Description
 */
class News extends \yii\db\ActiveRecord
{
    public $fone_file;
    public $photo_file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text_ru', 'description_ru', 'title_ru'], 'required'],
            [['type'], 'integer'],
            [['date'], 'safe'],
            [['text_uz', 'text_ru', 'text_en', 'description_uz', 'description_ru', 'description_en'], 'string'],
            [['fone', 'photo'], 'string', 'max' => 255],
            [['title_uz', 'title_ru', 'title_en'], 'string', 'max' => 300],
            [['fone_file','photo_file'], 'file', 'extensions' => 'jpg,jpeg,png']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'fone' => 'Фон',
            'photo' => 'Фото',
            'date' => 'Дата',
            'title_uz' => 'Sarlavha',
            'title_ru' => 'Заголовок',
            'title_en' => 'Title',
            'text_uz' => 'Matn',
            'text_ru' => 'Текст',
            'text_en' => 'Text',
            'description_uz' => 'Ma\'lumot',
            'description_ru' => 'Описание',
            'description_en' => 'Description',
            'fone_file' => 'Фон',
            'photo_file' => 'Фото'
        ];
    }

    public function afterDelete()
    {

        if ($this->fone != null && file_exists('uploads/news/' . $this->fone)) unlink(Yii::getAlias('uploads/news/' . $this->fone));
        parent::afterDelete();

        if ($this->photo != null && file_exists('uploads/news/' . $this->photo)) unlink(Yii::getAlias('uploads/news/' . $this->photo));
        parent::afterDelete();
    }
}
