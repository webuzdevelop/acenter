<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "commands".
 *
 * @property int $id
 * @property string $fio ФИО
 * @property string $permission Должность
 * @property string $email E-mail
 * @property string $photo Фотография
 * @property string $description_uz Qisqacha malumot
 * @property string $description_ru Описание
 * @property string $description_en Description
 * @property string $phone Телефон
 */
class Commands extends \yii\db\ActiveRecord
{
    public $image;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'commands';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description_ru',], 'required'],
            [['description_uz', 'description_ru', 'description_en'], 'string'],
            [['fio', 'permission', 'email', 'photo', 'phone'], 'string', 'max' => 255],
            [['image'],'file','extensions'=>'jpg,jpeg,png']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'permission' => 'Должность',
            'email' => 'E-mail',
            'photo' => 'Фотография',
            'description_uz' => 'Qisqacha malumot',
            'description_ru' => 'Описание',
            'description_en' => 'Description',
            'phone' => 'Телефон',
            'image' => 'Фотография'
        ];
    }


    public function afterDelete()
    {

        if ($this->photo != null && file_exists('uploads/commands/' . $this->photo)) unlink(Yii::getAlias('uploads/commands/' . $this->photo));
        parent::afterDelete();
    }
}
