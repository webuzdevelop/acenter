<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partners".
 *
 * @property int $id
 * @property string $name_uz Nomi
 * @property string $name_ru Наименование
 * @property string $name_en Name
 * @property string $text_uz Matn
 * @property string $text_ru Текст
 * @property string $text_en Text
 * @property string $photos Фотографии
 */
class Partners extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $file;
    public static function tableName()
    {
        return 'partners';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text_ru', 'name_ru'], 'required'],
            [['text_uz', 'text_ru', 'text_en', 'photos'], 'string'],
            [['name_uz', 'name_ru', 'name_en'], 'string', 'max' => 255],
            [['file'], 'file','maxFiles' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_uz' => 'Nomi',
            'name_ru' => 'Наиминование',
            'name_en' => 'Name',
            'text_uz' => 'Matn',
            'text_ru' => 'Текст',
            'text_en' => 'Text',
            'photos' => 'Фотографии',
            'file' => 'Фотографии',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        Directory::createElementsDirectory($this->id, 'partners');
        if ($insert) {
        } else {
            //Yii::$app->session->setFlash('success', 'Запись обновлена');
        }
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        Directory::deleteDirectory($this->id, 'partners');        
        return parent::beforeDelete();
    }
}
