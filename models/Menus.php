<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menus".
 *
 * @property int $id
 * @property string $name_uz Nomi
 * @property string $name_ru Наименование
 * @property string $name_en Name
 * @property int $visible Скрыть
 * @property string $key Ключ
 */
class Menus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_uz', 'name_ru', 'name_en','title_uz', 'title_ru', 'title_en', 'key'], 'required'],
            [['visible'], 'integer'],
            [['name_uz', 'name_ru', 'name_en','title_uz', 'title_ru', 'title_en', 'key'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_uz' => 'Nomi',
            'name_ru' => 'Наименование',
            'name_en' => 'Name',
            'title_uz' => 'Sarlavha',
            'title_ru' => 'Заглавие',
            'title_en' => 'Title',
            'visible' => 'Скрыть',
            'key' => 'Ключ',
        ];
    }
}
