<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "about".
 *
 * @property int $id
 * @property string $logo Логотип
 * @property string $email E-mail
 * @property string $phone Телефон
 * @property string $google Google
 * @property string $instagram Instagram
 * @property string $facebook Facebook
 * @property string $twitter Twitter
 * @property string $coordinate_x Coordinate X
 * @property string $coordinate_y Coordinate Y
 * @property string $address_uz Manzil
 * @property string $address_ru Адрес
 * @property string $address_en Address
 * @property string $title_uz Sarlavha
 * @property string $title_ru Заголовок
 * @property string $title_en Title
 * @property string $text_left_uz Matn
 * @property string $text_left_ru Текст
 * @property string $text_left_en Text
 * @property string $text_right_uz Matn
 * @property string $text_right_ru Текст
 * @property string $text_right_en Text
 */
class About extends \yii\db\ActiveRecord
{

    public $logo_image;
    public $about_company_img;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address_uz', 'address_ru', 'address_en','partners_text_uz','partners_text_ru','partners_text_en',
                'footer_text_uz','footer_text_ru','footer_text_en','about_text_uz', 'about_text_ru',
                'logo', 'email', 'phone', 'google', 'instagram', 'facebook', 'twitter', 'image_about_company',
                'coordinate_x', 'coordinate_y', 'title_uz', 'title_ru', 'title_en',
                'about_text_en', 'text_uz', 'text_ru', 'text_en' ],
                'required'],
            [['address_uz', 'address_ru', 'address_en','partners_text_uz','partners_text_ru','partners_text_en','footer_text_uz','footer_text_ru','footer_text_en','about_text_uz', 'about_text_ru', 'about_text_en', 'text_uz', 'text_ru', 'text_en' ], 'string'],
            [['logo', 'email', 'phone', 'google', 'instagram', 'facebook', 'twitter', 'image_about_company', 'coordinate_x', 'coordinate_y', 'title_uz', 'title_ru', 'title_en'], 'string', 'max' => 255],
            [['logo_image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg',]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'logo' => 'Лого',
            'email' => 'E-mail',
            'phone' => 'Телефон номер',
            'google' => 'Google',
            'instagram' => 'Instagram',
            'facebook' => 'Facebook',
            'twitter' => 'Twitter',
            'coordinate_x' => 'Coordinate X',
            'coordinate_y' => 'Coordinate Y',
            'address_uz' => 'Manzil',
            'address_ru' => 'Адрес',
            'address_en' => 'Address',
            'title_uz' => 'Sarlavha',
            'title_ru' => 'Заголовок',
            'title_en' => 'Title',
            'image_about_company' => 'Изображение о компании',
            'about_text_uz' => 'Kompaniya haqida',
            'about_text_ru' => 'О компании',
            'about_text_en' => 'About company',
            'text_uz' => 'Matn',
            'text_ru' => 'Текст',
            'text_en' => 'Text',
            'logo_image' => 'Лого',
            'about_company_img' => 'Изображение о компании',
            'partners_text_uz' => 'Hamkor matni',
            'partners_text_ru' => 'Текст Партнера',
            'partners_text_en' => 'Partner Text',
            'footer_text_uz' => 'Futer matni',
            'footer_text_ru' => 'Текст футера',
            'footer_text_en' => 'Footer Text',
        ];
    }

}
