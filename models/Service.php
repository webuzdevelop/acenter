<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "service".
 *
 * @property int $id
 * @property string $photo Фото
 * @property double $price Цена
 * @property string $name_uz Nomi
 * @property string $name_ru Наименование
 * @property string $name_en Name
 * @property string $text_uz Punktlar
 * @property string $text_ru Предметы
 * @property string $text_en Items
 */
class Service extends \yii\db\ActiveRecord
{

    public $file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text_ru', 'name_ru'], 'required'],
            [['price'], 'number'],
            [['text_uz', 'text_ru', 'text_en'], 'string'],
            [['photo', 'name_uz', 'name_ru', 'name_en'], 'string', 'max' => 255],
            [['file'], 'file', 'extensions' => 'jpeg,jpg,png']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'photo' => 'Фото',
            'price' => 'Цена',
            'name_uz' => 'Nomi',
            'name_ru' => 'Наименование',
            'name_en' => 'Name',
            'text_uz' => 'Tekst',
            'text_ru' => 'Текст',
            'text_en' => 'Text',
            'file' => 'Фото'
        ];
    }

    public function afterDelete()
    {

        if ($this->photo != null && file_exists('uploads/service/' . $this->photo)) unlink(Yii::getAlias('uploads/service/' . $this->photo));
        parent::afterDelete();
    }
}
