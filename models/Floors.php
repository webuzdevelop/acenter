<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "floors".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $photo Фотограция
 *
 * @property Shops[] $shops
 */
class Floors extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'floors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'photo'], 'string', 'max' => 255],
            [['file'],'file','skipOnEmpty' => true, 'extensions' => 'jpg,jpeg,png']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'photo' => 'Фотограция',
            'file' => 'Фотограция'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShops()
    {
        return $this->hasMany(Shops::className(), ['floor_id' => 'id']);
    }

    public function afterDelete()
    {

        if ($this->photo != null && file_exists('uploads/' . $this->photo)) unlink(Yii::getAlias('uploads/' . $this->photo));
        parent::afterDelete();

    }
}
