<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reviews".
 *
 * @property int $id
 * @property string $avatar Фото
 * @property string $fio ФИО
 * @property string $permission_uz Lavozimi
 * @property string $permission_ru Должность
 * @property string $permission_en Permission
 * @property string $title_uz Sarlavha
 * @property string $title_ru Заголовок
 * @property string $title_en Title
 * @property string $text_uz Matn
 * @property string $text_ru Текст
 * @property string $text_en text
 */
class Reviews extends \yii\db\ActiveRecord
{

    public $image;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reviews';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text_ru' , 'title_ru' , 'permission_ru'], 'required'],
            [['title_uz', 'title_ru', 'title_en', 'text_uz', 'text_ru', 'text_en'], 'string'],
            [['avatar', 'fio', 'permission_uz', 'permission_ru', 'permission_en'], 'string', 'max' => 255],
            [['image'],'file','extensions' => 'jpg,jpeg,png']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'avatar' => 'Фото',
            'fio' => 'ФИО',
            'permission_uz' => 'Lavozimi',
            'permission_ru' => 'Должность',
            'permission_en' => 'Permission',
            'title_uz' => 'Sarlavha',
            'title_ru' => 'Заголовок',
            'title_en' => 'Title',
            'text_uz' => 'Matn',
            'text_ru' => 'Текст',
            'text_en' => 'Text',
            'image' => 'Фото'
        ];
    }

    public function afterDelete()
    {

        if ($this->avatar != null && file_exists('uploads/review/' . $this->avatar)) unlink(Yii::getAlias('uploads/review/' . $this->avatar));
        parent::afterDelete();
    }
}
