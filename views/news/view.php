<div class="news-2-content">
    <img class="news-2-content-img wow fadeInUp" src="<?=$news['photo']?>" alt="news2" style="visibility: visible; animation-name: fadeInUp;">
    <p class="news-2-content-text wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;"><?=$news['description']?></p>
</div>