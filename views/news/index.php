<div id="news-content" class="news-content">
    <?php foreach ($items as $item) { ?>       
        <div class="news-content-block wow fadeInUp">
            <div class="news-content-block-left">
                <img class="news-content-block-img" src="<?=$item['fone']?>" alt="">
                <a class="news-content-block-button" href="/<?=$lang?>/news/view?id=<?=$item['id']?>"><?= $dictionary?></a>
            </div>
            <div class="news-content-block-right">
                <h3 class="news-content-block-title"><?= $item['title']?></h3>
                <p class="news-content-block-text-1"><?= $item['date']?></p>
                <p class="news-content-block-text-2"><?= $item['text']?></p>
            </div>
        </div>
    <?php } ?>
    <?php if(count($items) == 0) { ?>
        <h3 style="font-weight: 300; font-size: 50px; color: #ff0505; text-align: center;">
            Нет данные
        </h3>
    <?php } ?>
<!--     <div class="news-content-block wow fadeInUp">
        <div class="news-content-block-left">
            <img class="news-content-block-img" src="/frontend/img/news-block-img-1.png" alt="">
            <form action="news-2.html">
                <button type="submit" class="news-content-block-button">Подробнее</button>
            </form>
        </div>
        <div class="news-content-block-right">
            <h3 class="news-content-block-title">Распродажа уже идет!!!</h3>
            <p class="news-content-block-text-1">23.02.19</p>
            <p class="news-content-block-text-2">Осталось 4 грандиозных дня, что бы придти на распродажу A-Center на Тутаевском шоссе, 8 и успеть осуществить свою мечту! <br> Вы будете приятно удивлены не только максимальным выгодам, но и подаркам! Пришло то вре...</p>

        </div>
    </div>
    <div class="news-content-block wow fadeInUp">
        <div class="news-content-block-left">
            <img class="news-content-block-img" src="/frontend/img/news-block-img-1.png" alt="">
            <form action="news-2.html">
                <button type="submit" class="news-content-block-button">Подробнее</button>
            </form>
        </div>
        <div class="news-content-block-right">
            <h3 class="news-content-block-title">Распродажа уже идет!!!</h3>
            <p class="news-content-block-text-1">23.02.19</p>
            <p class="news-content-block-text-2">Осталось 4 грандиозных дня, что бы придти на распродажу A-Center на Тутаевском шоссе, 8 и успеть осуществить свою мечту! <br> Вы будете приятно удивлены не только максимальным выгодам, но и подаркам! Пришло то вре...</p>

        </div>
    </div>
    <div class="news-content-block wow fadeInUp">
        <div class="news-content-block-left">
            <img class="news-content-block-img" src="/frontend/img/news-block-img-1.png" alt="">
            <form action="news-2.html">
                <button type="submit" class="news-content-block-button">Подробнее</button>
            </form>
        </div>
        <div class="news-content-block-right">
            <h3 class="news-content-block-title">Распродажа уже идет!!!</h3>
            <p class="news-content-block-text-1">23.02.19</p>
            <p class="news-content-block-text-2">Осталось 4 грандиозных дня, что бы придти на распродажу A-Center на Тутаевском шоссе, 8 и успеть осуществить свою мечту! <br> Вы будете приятно удивлены не только максимальным выгодам, но и подаркам! Пришло то вре...</p>

        </div>
    </div>
    <div class="news-content-block wow fadeInUp">
        <div class="news-content-block-left">
            <img class="news-content-block-img" src="/frontend/img/news-block-img-1.png" alt="">
            <form action="news-2.html">
                <button type="submit" class="news-content-block-button">Подробнее</button>
            </form>
        </div>
        <div class="news-content-block-right">
            <h3 class="news-content-block-title">Распродажа уже идет!!!</h3>
            <p class="news-content-block-text-1">23.02.19</p>
            <p class="news-content-block-text-2">Осталось 4 грандиозных дня, что бы придти на распродажу A-Center на Тутаевском шоссе, 8 и успеть осуществить свою мечту! <br> Вы будете приятно удивлены не только максимальным выгодам, но и подаркам! Пришло то вре...</p>

        </div>
    </div>
    <div class="news-content-block wow fadeInUp">
        <div class="news-content-block-left">
            <img class="news-content-block-img" src="/frontend/img/news-block-img-1.png" alt="">
            <form action="news-2.html">
                <button type="submit" class="news-content-block-button">Подробнее</button>
            </form>
        </div>
        <div class="news-content-block-right">
            <h3 class="news-content-block-title">Распродажа уже идет!!!</h3>
            <p class="news-content-block-text-1">23.02.19</p>
            <p class="news-content-block-text-2">Осталось 4 грандиозных дня, что бы придти на распродажу A-Center на Тутаевском шоссе, 8 и успеть осуществить свою мечту! <br> Вы будете приятно удивлены не только максимальным выгодам, но и подаркам! Пришло то вре...</p>

        </div>
    </div> -->
</div>
<div id="J_loading" style="display:none;"><img src="/frontend/img/loader.gif" alt=""></div>