<div class="resident-address-texts footer-address-texts wow fadeInUp">
    <div class="resident-address-texts-adds footer-address-texts-adds">
        <h5><?= $dictionary['adres']?></h5>
        <p><?= $about['adres']?></p>
    </div>
    <div class="resident-address-texts-tel footer-address-texts-tel">
        <h5><?= $dictionary['phone']?></h5>
        <p><a href="tel:<?=$about['phone']?>"><?=$about['phone']?></a></p>
    </div>
    <div class="resident-address-texts-email footer-address-texts-email">
        <h5><?= $dictionary['working_time']?></h5>
        <p><?= $dictionary['clock']?></p>
    </div>
    <div class="resident-address-texts-soc footer-address-texts-soc">
        <h5><?= $dictionary['social']?></h5>
        <a class="footer-address-texts-soc-google" href="<?= $about['google']?>"></a>
        <a class="footer-address-texts-soc-insta" href="<?= $about['instagram']?>"></a>
        <a class="footer-address-texts-soc-facebook" href="<?= $about['facebook']?>"></a>
        <a class="footer-address-texts-soc-twitter" href="<?= $about['twitter']?>"></a>
    </div>
</div>

<div class="resident-content-block-1 wow fadeInUp">
    <img class="resident-content-block-1-img" src="<?= $shop_items['logo']?>" alt="">
    <div class="resident-content-block-1-text">
        <?= $shop_items['text']?>
    </div>
</div>

<div class="resident-content-block-2 wow fadeInUp">
    <img class="resident-content-block-2-img" src="<?= $shop_items['photo1']?>" alt="">
    <?= $shop_items['text_top']?>
</div>

<div class="resident-content-block-3 wow fadeInUp">
    <img class="resident-content-block-3-img" src="<?= $shop_items['photo2']?>" alt="">
    <?= $shop_items['text_bottom']?>
</div>