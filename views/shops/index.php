<?php
use yii\helpers\Url;
?>
<div id="mag-content" class="mag-content">
    <?php foreach ($items as $item): ?>
        <div class="mag-content-block wow fadeInUp">
            <div class="mag-content-block-left">

                <a href="<?= Url::to(['/shops/resident','id' => $item['id']]) ?>"><img class="mag-content-block-img mag-content-block-img-1"
                                             src="<?= $item['logo'] ?>" alt=""></a>
            </div>
            <div class="mag-content-block-right">
                <h3 class="mag-content-block-title"><?= $item['name'] ?></h3>
                <p class="mag-content-block-text"><?= $item['title'] ?></p>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<div id="J_loading" style="display:none;"><img src="/frontend/img/loader.gif" alt=""></div>