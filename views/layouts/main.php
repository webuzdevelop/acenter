<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\models\About;
use app\models\Dictionary;
use yii\helpers\Html;
use app\assets\FrontAsset;
use yii\helpers\Url;
use app\models\Menus;
use app\models\Sliders;

FrontAsset::register($this);
$this->title = 'A-center';
$this->registerLinkTag(['rel' => 'shortcut icon', 'type' => 'image/x-icon', 'href' => '/images/new-ico.png']);
$controller = Yii::$app->controller->id;
$session = Yii::$app->session;
$lang = $session['language'] == null ? 'ru' : $session['language'];
/** menu bar*/
$name = 'name_' . $lang;
$main = Menus::find()->where(['and', ['key' => 'Main'], ['visible' => 0]])->one();
$about_company = Menus::find()->where(['and', ['key' => 'About company'], ['visible' => 0]])->one();
$services = Menus::find()->where(['and', ['key' => 'Services'], ['visible' => 0]])->one();
$shops = Menus::find()->where(['and', ['key' => 'Shops'], ['visible' => 0]])->one();
$news = Menus::find()->where(['and', ['key' => 'News'], ['visible' => 0]])->one();
$contacts = Menus::find()->where(['and', ['key' => 'Contacts'], ['visible' => 0]])->one();
$navigation = Menus::find()->where(['and', ['key' => 'Navigation'], ['visible' => 0]])->one();
$menu = [
    'main' => $main != null ? $main->$name : false,
    'about_company' => $about_company != null ? $about_company->$name : false,
    'services' => $services != null ? $services->$name : false,
    'shops' => $shops != null ? $shops->$name : false,
    'news' => $news != null ? $news->$name : false,
    'contacts' => $contacts != null ? $contacts->$name : false,
    'navigation' => $navigation != null ? $navigation->$name : false,
];
/** menu bar*/
/**menu title*/
$title_body = 'title_'.$lang;
$about_company = Menus::find()->where(['and', ['key' => 'About company'], ['visible' => 0]])->one();
$services = Menus::find()->where(['and', ['key' => 'Services'], ['visible' => 0]])->one();
$shops = Menus::find()->where(['and', ['key' => 'Shops'], ['visible' => 0]])->one();
$news = Menus::find()->where(['and', ['key' => 'News'], ['visible' => 0]])->one();
$contacts = Menus::find()->where(['and', ['key' => 'Contacts'], ['visible' => 0]])->one();
$navigation = Menus::find()->where(['and', ['key' => 'Navigation'], ['visible' => 0]])->one();

$menu_title = [
    'about_company_title' => $about_company != null ? $about_company->$title_body : false,
    'services_title' => $services != null ? $services->$title_body : false,
    'shops_title' => $shops != null ? $shops->$title_body : false,
    'news_title' => $news != null ? $news->$title_body : false,
    'contacts_title' => $contacts != null ? $contacts->$title_body : false,
    'navigation_title' => $navigation != null ? $navigation->$title_body : false,
/*
    'about_company_title' => Menus::find()->where(['and', ['key' => 'About company'], ['visible' => 0]])->one()->$title_body,
    'services_title' => Menus::find()->where(['and', ['key' => 'Services'], ['visible' => 0]])->one()->$title_body,
    'shops_title' => Menus::find()->where(['and', ['key' => 'Shops'], ['visible' => 0]])->one()->$title_body,
    'news_title' => Menus::find()->where(['and', ['key' => 'News'], ['visible' => 0]])->one()->$title_body,
    'contacts_title' => Menus::find()->where(['and', ['key' => 'Contacts'], ['visible' => 0]])->one()->$title_body,
    'navigation_title' => Menus::find()->where(['and', ['key' => 'Navigation'], ['visible' => 0]])->one()->$title_body*/
];
/**menu title*/
/** Dictionary*/

$dictionary = [];
switch ($lang) {
    case 'ru':
        $dictionary = [
            'about' => Dictionary::findOne(['key' => 'about_company'])->rus,
            'vacancy' => Dictionary::findOne(['key' => 'jobs'])->rus,
            'partner' => Dictionary::findOne(['key' => 'to_partner'])->rus,
            'title' => Dictionary::findOne(['key' => 'company_review'])->rus,
            'main' => Dictionary::findOne(['key' => 'main'])->rus,
            'read_more' => Dictionary::findOne(['key' => 'read_more'])->rus,
            'adres' => Dictionary::findOne(['key' => 'address'])->rus,
            'email' => Dictionary::findOne(['key' => 'email'])->rus,
            'phone' => Dictionary::findOne(['key' => 'phone'])->rus,
            'social' => Dictionary::findOne(['key' => 'social'])->rus,
            'company_review' => Dictionary::findOne(['key' => 'company_review'])->rus,
            'our_shops' => Dictionary::findOne(['key' => 'our_shops'])->rus,
            'promotions' => Dictionary::findOne(['key' => 'promotions'])->rus,
            'site_developed' => Dictionary::findOne(['key' => 'site_developed'])->rus,
            'promotions' => Dictionary::findOne(['key' => 'promotions'])->rus,
        ];
        break;
    case 'uz':
        $dictionary = [
            'about' => Dictionary::findOne(['key' => 'about_company'])->uzb,
            'vacancy' => Dictionary::findOne(['key' => 'jobs'])->uzb,
            'partner' => Dictionary::findOne(['key' => 'to_partner'])->uzb,
            'title' => Dictionary::findOne(['key' => 'company_review'])->uzb,
            'main' => Dictionary::findOne(['key' => 'main'])->uzb,
            'read_more' => Dictionary::findOne(['key' => 'read_more'])->uzb,
            'adres' => Dictionary::findOne(['key' => 'address'])->uzb,
            'email' => Dictionary::findOne(['key' => 'email'])->uzb,
            'phone' => Dictionary::findOne(['key' => 'phone'])->uzb,
            'social' => Dictionary::findOne(['key' => 'social'])->uzb,
            'company_review' => Dictionary::findOne(['key' => 'company_review'])->uzb,
            'our_shops' => Dictionary::findOne(['key' => 'our_shops'])->uzb,
            'promotions' => Dictionary::findOne(['key' => 'promotions'])->uzb,
            'site_developed' => Dictionary::findOne(['key' => 'site_developed'])->uzb,
            'promotions' => Dictionary::findOne(['key' => 'promotions'])->uzb,
        ];
        break;
    case 'en':
        $dictionary = [
            'about' => Dictionary::findOne(['key' => 'about_company'])->eng,
            'vacancy' => Dictionary::findOne(['key' => 'jobs'])->eng,
            'partner' => Dictionary::findOne(['key' => 'to_partner'])->eng,
            'title' => Dictionary::findOne(['key' => 'company_review'])->eng,
            'main' => Dictionary::findOne(['key' => 'main'])->eng,
            'read_more' => Dictionary::findOne(['key' => 'read_more'])->eng,
            'adres' => Dictionary::findOne(['key' => 'address'])->eng,
            'email' => Dictionary::findOne(['key' => 'email'])->eng,
            'phone' => Dictionary::findOne(['key' => 'phone'])->eng,
            'social' => Dictionary::findOne(['key' => 'social'])->eng,
            'company_review' => Dictionary::findOne(['key' => 'company_review'])->eng,
            'our_shops' => Dictionary::findOne(['key' => 'our_shops'])->eng,
            'promotions' => Dictionary::findOne(['key' => 'promotions'])->eng,
            'site_developed' => Dictionary::findOne(['key' => 'site_developed'])->eng,
            'promotions' => Dictionary::findOne(['key' => 'promotions'])->eng,
        ];
        break;
}
/** Dictionary*/
/** main slider*/
$allSliders = Sliders::find()->where(['type' => 'main'])->all();
$slider_items = [];
foreach ($allSliders as $item) {
    $title = 'title_' . $lang;
    $text = 'text_' . $lang;
    if($item->$title == '') $title = 'title_ru';
    if($item->$text == '') $text = 'text_ru';
    $photo = ($item->photo != null) ? '/uploads/sliders/' . $item->photo : '/images/no-image.jpg';
    $slider_items[] = [
        'title' => $item->$title,
        'text' => $item->$text,
        'photo' => $photo,
        'link' => $item->link
    ];
}
/** main slider*/

/** footer addresses*/
$about = About::findOne(1);
$adres = 'address_' . $lang;
$footer_text = 'footer_text_' . $lang;
$items = [
    'adres' => $about->$adres,
    'email' => $about->email,
    'phone' => $about->phone,
    'google' => $about->google,
    'facebook' => $about->facebook,
    'instagram' => $about->instagram,
    'twitter' => $about->twitter,
    'footer_text' => $about->$footer_text
];
/** footer addresses*/
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600|Roboto:300,400,500&display=swap"
          rel="stylesheet">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?= $this->render('header', ['controller' => $controller,
    'lang' => $lang,
    'menu' => $menu,
    'dictionary' => $dictionary,
    'slider_items' => $slider_items,
    'menu_title' => $menu_title]) ?>

<?= $content ?>

<?= $this->render('footer', ['controller' => $controller,
    'lang' => $lang,
    'menu' => $menu,
    'dictionary' => $dictionary,
    'items' => $items]) ?>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
