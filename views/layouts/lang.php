<?php

use app\models\Languages;

$langList = Languages::find()->where(['accept' => 1])->all();
?>        
<select id="mainSelect" onchange="window.location.href = '/'+$(this).val() + '/<?=$controller?>';"
    class="js-example-basic-single"
    name="floor">

	<?php foreach ($langList as $langItem): ?>
	<option value="<?=$langItem->title?>" <?= $lang == $langItem->title ? 'selected=""' : '' ?> ><?=strtoupper($langItem->title)?></option>
	<?php endforeach; ?>
</select>