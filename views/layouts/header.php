<?php

use app\models\Dictionary;

?>
<div class="<?= ($controller == 'site') ? 'main-header' : 'header header-2' ?>">
    <div id="navbar"
         class="<?= ($controller == 'site') ? 'header-top wow fadeInUp' : 'header-top header-2-top wow fadeInUp' ?>">
       <!--  <a href="/site"><img id="logo"
                             class="<?php // ($controller == 'site') ? 'header-top-logo' : 'header-top-logo header-2-top-logo' ?>"
                             src="" alt=""></a> -->
        <a href="/site">
            <img id="logo" class="<?= ($controller == 'site') ? 'header-top-logo' : 'header-top-logo header-2-top-logo' ?>" src="" alt="">
        </a>
        <ul class="<?= ($controller == 'site') ? 'header-top-ul' : 'header-top-ul header-2-top-ul' ?>">
            <?php if($menu['main'] != false):?>
            <li class="<?= ($controller == 'site') ? 'header-top-li' : 'header-top-li header-2-top-li' ?>">
                <a class="header-top-li-a" href="<?= Yii::$app->homeUrl ?>"><?= $menu['main'] ?></a></li>
            <?php endif;?>
            <?php if($menu['about_company'] != false):?>
            <li class="<?= ($controller == 'site') ? 'header-top-li' : 'header-top-li header-2-top-li' ?>">
                <a class="header-top-li-a" href="/<?= $lang ?>/about"><?= $menu['about_company'] ?></a></li>
            <?php endif;?>
            <?php if($menu['services'] != false):?>
            <li class="<?= ($controller == 'site') ? 'header-top-li' : 'header-top-li header-2-top-li' ?>">
                <a class="header-top-li-a" href="/<?= $lang ?>/service"><?= $menu['services'] ?></a></li>
            <?php endif;?>
            <?php if($menu['shops'] != false):?>
            <li class="<?= ($controller == 'site') ? 'header-top-li' : 'header-top-li header-2-top-li' ?>">
                <a class="header-top-li-a" href="/<?= $lang ?>/shops"><?= $menu['shops'] ?></a></li>
            <?php endif;?>
            <?php if($menu['news'] != false):?>
            <li class="<?= ($controller == 'site') ? 'header-top-li' : 'header-top-li header-2-top-li' ?>">
                <a class="header-top-li-a" href="/<?= $lang ?>/news"><?= $menu['news'] ?></a></li>
            <?php endif;?>
            <?php if($menu['contacts'] != false):?>
            <li class="<?= ($controller == 'site') ? 'header-top-li' : 'header-top-li header-2-top-li' ?>">
                <a class="header-top-li-a" href="/<?= $lang ?>/contacts"><?= $menu['contacts'] ?></a></li>
            <?php endif;?>
        </ul>
        <?= $this->render('lang', [
            'controller' => $controller,
            'lang' => $lang
        ]) ?>
        <?php if($menu['navigation'] != false):?>
            <button id="button" class="<?= ($controller == 'site') ? 'header-top-button' : 'header-top-button header-2-top-button' ?>">
                <a href="/navigation"><?= $menu['navigation'] ?></a>
            </button>
        <?php endif;?>
        <div class="<?= ($controller == 'site') ? 'hamburger-menu' : 'hamburger-menu hamburger-menu-2' ?>">
            <input id="menu__toggle" type="checkbox"/>
            <label class="menu__btn" for="menu__toggle">
                <span></span>
            </label>
            <ul class="menu__box">
                <select id="select" class="hamburger-menu-select" name="floor"
                        onchange="window.location.href = '/'+$(this).val();">
                    <option value="ru" <?= $lang == 'ru' ? 'selected=""' : '' ?> >RU</option>
                    <option value="en" <?= $lang == 'en' ? 'selected=""' : '' ?> >ENG</option>
                    <option value="uz" <?= $lang == 'uz' ? 'selected=""' : '' ?> >UZ</option>
                </select>
                <?php if($menu['main'] != false){?><li><a class="menu__item" href="/site"><?= $menu['main'] ?></a></li><?php }?>
                <?php if($menu['about_company'] != false){?><li><a class="menu__item" href="/about"><?= $menu['about_company'] ?></a></li><?php }?>
                <?php if($menu['services'] != false){?><li><a class="menu__item" href="/service"><?= $menu['services'] ?></a></li><?php }?>
                <?php if($menu['shops'] != false){?><li><a class="menu__item" href="/shops"><?= $menu['shops'] ?></a></li><?php }?>
                <?php if($menu['news'] != false){?><li><a class="menu__item" href="/news"><?= $menu['news'] ?></a></li><?php }?>
                <?php if($menu['contacts'] != false){?><li><a class="menu__item" href="/contacts"><?= $menu['contacts'] ?></a></li><?php }?>
                <?php if($menu['navigation'] != false){?>
                <button id="button" class="hamburger-menu-button">
                    <a href="/<?= $lang ?>/navigation"><?= $menu['navigation'] ?></a>
                </button>
                <?php }?>
            </ul>
        </div>
    </div>
    <?php if ($controller == 'site'): ?>
        <div class="header-slider owl-carousel owl-theme owl-loaded">
            <?php foreach ($slider_items as $item): ?>
                <div class="header-slider-item">
                    <img class="header-slider-item-img" src="<?= $item['photo'] ?>" alt="">
                    <div class="owl-item-text-1 wow fadeInUp"><?= $item['title'] ?></div>
                    <div class="owl-item-text-2 wow fadeInUp">
                        <?= $item['text'] ?>
                    </div>
                    <button class="header-slider-item-button wow fadeInUp"><a href="<?= $item['link'] ?>"
                                                                              target="_blank"><?= $dictionary['read_more'] ?></a>
                    </button>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <?php if ($controller == 'about'): ?>
        <h3 class="header-2-title wow fadeInUp"><?= $dictionary['about'] ?></h3>
        <p class="header-2-text wow fadeInUp"><?= $menu_title['about_company_title'] ?></p>
        <div class="header-bottom wow fadeInUp">
            <div class="header-bottom-left">
                <p><span class="<?= (Yii::$app->controller->action->id == 'index') ? 'header-bottom-left-ent' : '' ?>"><a
                                href="/<?= $lang ?>/about"><?= $dictionary['about'] ?></a></span></p>
                <p><span class="<?= (Yii::$app->controller->action->id == 'vacancy') ? 'header-bottom-left-ent' : '' ?>"><a href="/<?= $lang ?>/about/vacancy"><?= $dictionary['vacancy'] ?></a></span></p>
                <p><span class="<?= (Yii::$app->controller->action->id == 'partner') ? 'header-bottom-left-ent' : '' ?>"><a href="/<?= $lang ?>/about/partner"><?= $dictionary['partner'] ?></a></span></p>
            </div>
            <?php $session = Yii::$app->session;?>
            <div class="header-bottom-right">
                <p><span class="header-bottom-right-ent"><a href="<?= Yii::$app->homeUrl ?>"><?= $dictionary['main'] ?></a></span>
                   <?php if($session['breadcrumb']!= null):?>
                    <a href="#">/ <?= $session['breadcrumb']?></a>
                <?php endif;?>
                </p>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($controller == 'service'): ?>
        <h3 class="header-2-title wow fadeInUp"><?= $menu['services'] ?></h3>
        <p class="header-2-text wow fadeInUp"><?= $menu_title['services_title'] ?></p>
        <div class="header-bottom wow fadeInUp">
            <div class="header-bottom-left">
            </div>
            <div class="header-bottom-right">
                <p><span class="header-bottom-right-ent"><a
                                href="/<?= $lang ?>/site"><?= $dictionary['main'] ?></a></span> <a
                            href="/<?= $lang ?>/service">/
                        <?= $menu['services'] ?></a></p>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($controller == 'shops'): ?>
        <h3 class="header-2-title wow fadeInUp"><?= $dictionary['our_shops'] ?></h3>
        <p class="header-2-text wow fadeInUp"><?= $menu_title['shops_title'] ?></p>
        <div class="header-bottom wow fadeInUp">
            <div class="header-bottom-left">

            </div>
            <div class="header-bottom-right">
                <?php $session = Yii::$app->session; ?>
                <p><span class="header-bottom-right-ent"><a
                                href="/<?= $lang ?>/site"><?= $dictionary['main'] ?></a></span>
                    <a href="/<?= $lang ?>/shops">/<?= $dictionary['our_shops'] ?></a>
                    <?php if (Yii::$app->controller->action->id == 'resident'): ?>
                    <a href="#">/<?= $session['resident'] ?></a></p>
            <?php endif; ?>
                </p>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($controller == 'news'): ?>
        <h3 class="header-2-title  wow fadeInUp"><?= $menu['news'] ?></h3>
        <p class="header-2-text  wow fadeInUp"><?= $menu_title['news_title'] ?></p>
        <div class="header-bottom  wow fadeInUp">
            <div class="header-bottom-left">
                <p><span class="<?= (Yii::$app->controller->action->id == 'index')? 'header-bottom-left-ent' : ''?>"><a href="/<?= $lang ?>/news"><?= $menu['news'] ?></a></span></p>
                <p><span class="<?= (Yii::$app->controller->action->id == 'stock')? 'header-bottom-left-ent' : ''?>"><a href="/<?= $lang ?>/news/stock"><?= $dictionary['promotions'] ?></a></span></p>
            </div>
            <?php $session = Yii::$app->session ?>
            <div class="header-bottom-right">
                <p><span class="header-bottom-right-ent"><a href="/<?= $lang ?>/site"><?= $menu['main'] ?></a></span>
                    <?php if(Yii::$app->controller->action->id == 'index'):?>
                    <a href="/<?= $lang ?>/news">/ <?= $menu['news'] ?></a>
                    <?php endif;?>
                    <?php if(Yii::$app->controller->action->id == 'stock'):?>
                    <a href="/<?= $lang ?>/news/stock">/ <?= $dictionary['promotions'] ?></a>
                    <?php endif;?>
                    <?php if (Yii::$app->controller->action->id == 'view'): ?>
                        <a href="#">/ <?= $session['new'] ?></a>
                    <?php endif; ?>
                </p>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($controller == 'contacts'): ?>
        <h3 class="header-2-title wow fadeInUp"><?= $menu['contacts'] ?></h3>
        <p class="header-2-text wow fadeInUp"><?= $menu_title['contacts_title'] ?></p>
        <div class="header-bottom wow fadeInUp">
            <div class="header-bottom-left">

            </div>
            <div class="header-bottom-right">
                <p><span class="header-bottom-right-ent"><a href="/<?= $lang ?>/site"><?= $menu['main'] ?></a></span> <a
                            href="/<?= $lang ?>/contacts">/ <?= $menu['contacts'] ?></a></p>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($controller == 'navigation'): ?>
        <h3 class="header-2-title wow fadeInUp"><?= $menu['navigation'] ?></h3>
        <p class="header-2-text wow fadeInUp"><?= $menu_title['navigation_title'] ?></p>
        <div class="header-bottom wow fadeInUp">
            <div class="header-bottom-left">

            </div>
            <div class="header-bottom-right">
                <p><span class="header-bottom-right-ent"><a href="/<?= $lang ?>/site"><?= $menu['main'] ?></a></span> <a
                            href="/<?= $lang ?>/navigation">/ <?= $menu['navigation'] ?></a></p>
            </div>
        </div>
    <?php endif; ?>
</div>