<div class="footer">
    <ul class="footer-ul wow fadeInUp">
        <?php if($menu['main'] != false){?><li class="footer-li"><a href="/<?= $lang ?>/site"><?=$menu['main']?></a></li><?php }?>
        <?php if($menu['about_company'] != false){?><li class="footer-li"><a href="/<?= $lang ?>/about"><?=$menu['about_company']?></a></li><?php }?>
        <?php if($menu['services'] != false){?><li class="footer-li"><a href="/<?= $lang ?>/service"><?=$menu['services']?></a></li><?php }?>
        <?php if($menu['shops'] != false){?><li class="footer-li"><a href="/<?= $lang ?>/shops"><?=$menu['shops']?></a></li><?php }?>
        <?php if($menu['news'] != false){?><li class="footer-li"><a href="/<?= $lang ?>/news"><?=$menu['news']?></a></li><?php }?>
        <?php if($menu['contacts'] != false){?><li class="footer-li"><a href="/<?= $lang ?>/contacts"><?=$menu['contacts']?></a></li><?php }?>
    </ul>
    <div class="footer-address-texts wow fadeInUp">
        <div class="footer-address-texts-adds">
            <h5><?= $dictionary['adres']?></h5>
            <p><?= $items['adres']?></p>
        </div>
        <div class="footer-address-texts-tel">
            <h5><?= $dictionary['phone']?></h5>
            <p><a href="tel: <?= $items['phone']?>"><?= $items['phone']?></a></p>
        </div>
        <div class="footer-address-texts-email">
            <h5><?= $dictionary['email']?></h5>
            <p><a href="mailto:<?= $items['email']?>"><?= $items['email']?></a>
            </p>
        </div>
        <div class="footer-address-texts-soc">
            <h5><?= $dictionary['social']?></h5>
            <a class="footer-address-texts-soc-google" href="<?= $items['google']?>"></a>
            <a class="footer-address-texts-soc-insta" href="<?= $items['instagram']?>"></a>
            <a class="footer-address-texts-soc-facebook" href="<?= $items['facebook']?>"></a>
            <a class="footer-address-texts-soc-twitter" href="<?= $items['twitter']?>"></a>
        </div>
    </div>
    <p class="footer-text-1 wow fadeInUp"><?= $items['footer_text']?></p>
    <p class="footer-text-2 wow fadeInUp"><?= $dictionary['site_developed']?>: <a href="http://www.deepx.uz">DeepX</a></p>

</div>