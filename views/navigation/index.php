<?php


use yii\widgets\ActiveForm;

?>

<div class="navigation-floor wow fadeInUp">
    <div class="navigation-floor-block-1">
        <p class="navigation-floor-block-1-text">Авто бизнес центр</p>
        <select  onchange="window.location.href = '/<?=$lang?>' + '/<?=$controller?>?floor='+$(this).val();"
            class="navigation-js-example-basic-single js-example-basic-single"
            name="floor">
            <?php foreach ($floors as $item): ?>
                <option value="<?=$item->name?>" <?= $floor == $item->name ? 'selected=""' : '' ?> ><?=$item->name?></option>
            <?php endforeach; ?>
        </select>

        <div class="navigation-floor-block-1-img">
            <img src="/uploads/floors/<?=$firstFloor->photo?>" alt="<?=$firstFloor->name?>">
        </div>
    </div>

    <div class="navigation-floor-block-2">
        <form class="navigation-floor-block-2-form" action="" autocomplete="off">
            <input class="navigation-floor-block-2-input" name="search" type="text" value="<?=$search?>" placeholder="Поиск">
            <input name="floor" type="hidden" value="<?=$floor?>">
        </form>
        <div class="accordion nav-accordion">
            <ul>
                <?php foreach ($shops as $shop) : ?>
                <li>
                    <a href="/<?=$lang?>/resident?id=<?=$shop->id?>">
                        <h2 class="navigation-floor-block-2-text">
                            <?=$shop->name?>
                            <!-- <span class="navigation-floor-block-2-span"><?php //$shop->floors->name?></span> -->
                        </h2> 
                    </a>
                </li>
            <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>

<div class="navigation-slider-block wow fadeInUp">
    <div class="navigation-slider owl-carousel owl-theme owl-loaded">
        <?php foreach ($sliders as $slider):?>
        <div>
            <img src="<?= $slider['photo']?>" alt="nav-sl-img">
        </div>
        <?php endforeach;?>
    </div>
</div>