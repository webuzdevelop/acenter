<?php

use katzz0\yandexmaps\Canvas as YandexMaps;
use katzz0\yandexmaps\Map;
use katzz0\yandexmaps\objects\Placemark;
use katzz0\yandexmaps\Point;
use kartik\form\ActiveForm;
?>
<div class="address">
    <div class="address-texts wow fadeInUp">
        <div class="address-texts-adds">
            <h5><?= $dictionary['adres']?></h5>
            <p><?= $items['adres']?></p>
        </div>
        <div class="address-texts-email">
            <h5><?= $dictionary['email']?></h5>
            <p><a href="mailto:<?= $items['email']?>"><?= $items['email']?></a> </p>
        </div>
        <div class="address-texts-tel">
            <h5><?= $dictionary['phone']?></h5>
            <p><a href="tel:<?= $items['email']?>"><?= $items['email']?></a></p>
        </div>
        <div class="address-texts-soc">
            <h5><?= $dictionary['social']?></h5>
            <a class="footer-address-texts-soc-google" href="<?= $items['google']?>"></a>
            <a class="footer-address-texts-soc-insta" href="<?= $items['instagram']?>"></a>
            <a class="footer-address-texts-soc-facebook" href="<?= $items['instagram']?>"></a>
            <a class="footer-address-texts-soc-twitter" href="<?= $items['instagram']?>"></a>
        </div>
    </div>
    <div id="mymap" class="">
<!--        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aad16a76603ab07a8055002b529a9a62260855472b563d1d6cf61f5a46666472e&amp;width=1920&amp;height=240&amp;lang=ru_RU&amp;scroll=true"></script>-->
        <?= YandexMaps::widget([
            'htmlOptions' => [
                'style' => 'height: 240px;',
            ],
            'map' => new Map('yandex_map', [
                'center' => [$items['coordinate_x'], $items['coordinate_y']],
                'zoom' => 12,
                'controls' => [Map::CONTROL_ZOOM],
                'behaviors' => [Map::BEHAVIOR_DRAG],
                'type' => "yandex#map",
            ],
                [
                    'objects' => [
                        new Placemark(new Point($items['coordinate_x'], $items['coordinate_y']), [], [
                            'draggable' => false,
                            'preset' => 'islands#dotIcon',
                            'iconColor' => 'red'
                        ]
                        )
                    ]
                ])
        ]) ?>
    </div>
</div>

<h3 class="contacts-feedback-title wow fadeInUp"><?= $dictionary['feedback']?></h3>

<div class="contacts-feedback">
    <form class="contacts-feedback-form" method="get" action="/contacts/feedback">
        <div class="contacts-feedback-form-1">
            <fieldset class="contacts-feedback-form-field contacts-feedback-form-field-1 wow fadeInUp">
                <input class="contacts-feedback-form-input contacts-feedback-form-input-1" id="name" name="name" type="name" placeholder="<?= $dictionary['fio']?>"/>
            </fieldset>
            <fieldset class="contacts-feedback-form-field contacts-feedback-form-field-2 wow fadeInUp">
                <label class="contacts-feedback-form-label" for=""></label>
                <input class="contacts-feedback-form-input contacts-feedback-form-input-2" id="email" name="email" type="email" placeholder="<?= $dictionary['email']?>"/>
            </fieldset>
            <fieldset class="contacts-feedback-form-field contacts-feedback-form-field-3 wow fadeInUp">
                <label class="contacts-feedback-form-label" for=""></label>
                <input class="contacts-feedback-form-input contacts-feedback-form-input-4" id="number" name="phone" type="text" placeholder="+998"/>
            </fieldset>
        </div>
        <div class="contacts-feedback-form-2">
            <fieldset class="contacts-feedback-form-field form-field-4 wow fadeInUp">
                <label class="contacts-feedback-form-label" for=""></label>
                <textarea class="contacts-feedback-form-textarea" placeholder="<?= $dictionary['message']?>" id="textarea" name="message"></textarea>
            </fieldset>
        </div>
        <div class="contacts-feedback-form-3 wow fadeInUp">
            <div style="width: 158px; transform: scale(0.5); transform-origin: 0;" class="g-recaptcha" data-sitekey="6LfA3qgUAAAAAFbbMNiAgbbVeX5QSI5BniudS_jO"></div>
            <fieldset class="contacts-feedback-form-field contacts-feedback-form-field-5">
                <button class="contacts-feedback-form-button" type="submit"><?= $dictionary['send']?></button>
            </fieldset>
        </div>
    </form>

</div>