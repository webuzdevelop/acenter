<div id="service-content" class="service-content">
    <?php foreach ($items as $item):?>
    <div class="service-content-block wow fadeInUp">
        <div class="service-content-block-left">
            <img class="service-content-block-img" src="<?= $item['photo']?>" alt="">
        </div>
        <div class="service-content-block-right">
            <h3 class="service-content-block-title"><?= $item['name']?></h3>
            <p class="service-content-block-text"><?= $item['price']?>.</p>
            <?= $item['text']?>
        </div>
    </div>
    <?php endforeach;?>
</div>
<div id="J_loading" style="display:none;"><img src="/frontend/img/loader.gif" alt=""></div>