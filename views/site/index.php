<div class="content">
    <div class="content-block-1">
        <div class="content-block-1-left">
            <h3 class="content-block-1-title wow fadeInUp"><?= $titles['our_center']?></h3>
            <img class="content-block-1-img wow fadeInUp" src="<?= $about['logo']?>" alt="content-logo">
            <p class="content-block-1-text-1 wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;"><?= $about['text']?></p>
            <p class="content-block-1-text-2 wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;"><?= $about['about_text']?></p>
        </div>
        <!-- <div class="content-block-1-left">
            <h3 class="content-block-1-title wow fadeInUp"><?php // $titles['our_center']?></h3>
            <img class="content-block-1-img wow fadeInUp" src="<?php // $about['logo']?>" alt="content-logo">
            <p class="content-block-1-text-2 wow fadeInUp"><?php // $about['about_text']?></p>
        </div> -->
        <div class="content-block-1-right">
            <img class="content-block-1-right-img" src="<?= $about['foto']?>" alt="index-img-1">
        </div>
    </div>
</div>
<div class="main-slider-block">
    <div class="main-slider-block-bg">
        <img class="wow fadeInUp" style="width: 640px;height: 523px;" src="/images/office-desks.jpg" alt="">
    </div>
    <h3 class="main-slider-block-title wow fadeInUp"><?= $titles['about_company']?></h3>
    <p class="main-slider-block-text wow fadeInUp"><?= $titles['company_review']?></p>
    <div class="main-slider owl-carousel owl-theme owl-loaded wow fadeInUp">
        <?php foreach ($items as $item):?>
        <div class="main-slider-item">
            <img class="main-slider-item-img" src="<?= $item['avatar']?>" alt="main-slider-item-img">
            <h3 class="main-slider-item-title"><?= $item['fio']?></h3>
            <p class="main-slider-item-text-1"><?= $item['permission']?></p>
            <p class="main-slider-item-text-2"><?= $item['text']?> <p>
        </div>
        <?php endforeach;?>
    </div>
</div>
