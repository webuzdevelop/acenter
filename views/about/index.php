
<div class="about-text-block">
    <p class="about-text-block-1 wow fadeInUp"><?= $about_company?></p>
</div>

<div class="navigation-slider-block wow fadeInUp">
    <div class="navigation-slider owl-carousel owl-theme owl-loaded">
        <?php foreach($sliders as $item):?>
        <!-- <div>
            <img src="<?php // $item['photo']?>" alt="nav-sl-img">
        </div> -->
        <div>
            <iframe height="447px" width="760px" allowfullscreen="true" src="<?=$item['link']?>"></iframe>
        </div>
        <?php endforeach;?>
    </div>
</div>

<div class="about-command-slider owl-carousel owl-theme owl-loaded wow fadeInUp">
    <?php foreach ($Commands as $item):?>
    <div class="about-command-slider-block">
        <div class="about-command-slider-block-left">
            <img class="about-command-slider-block-img" src="<?= $item['photo']?>" alt="">
        </div>
        <div class="about-command-slider-block-right">
            <h3 class="about-command-slider-block-title"><?= $item['fio']?></h3>
            <p class="about-command-slider-block-text-1"><?= $item['permission']?></p>
            <p class="about-command-slider-block-text-2"><?= $item['description']?></p>
            <p class="about-command-slider-block-text-3"><span class="about-command-slider-block-text-3-blue"><?= $title['email']?></span> <br> <?= $item['email']?></p>
            <p class="about-command-slider-block-text-3"><span class="about-command-slider-block-text-3-blue"><?= $title['phone']?></span> <br> <?= $item['phone']?></p>
        </div>
    </div>
    <?php endforeach;?>
</div>

<img class="about-content-logo wow fadeInUp" style="width: 160px; height: 84px;" src="../images/new-logo.png" alt="">