<h3 class="vacancy-content-title"><?= $title ?></h3>
<div class="accordion">
    <ul>
        <?php foreach ($items as $item): ?>
            <li class="wow fadeInUp">
                <input type="checkbox" checked>
                <i></i>
                <h2 class="title_block"><?= $item['name'] ?></h2>
                <div class="msg">
                <?= $item['text']?>
                </div >
            </li>
        <?php endforeach; ?>
    </ul>
</div>