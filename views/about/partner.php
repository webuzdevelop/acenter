<?php

use yii\helpers\Url;

?>
<h3 class="vacancy-content-title wow fadeInUp"><?= $title ?></h3>
<p class="partner-content-title wow fadeInUp"></p>
<div class="accordion">
    <ul>
        <?php foreach ($items as $item): ?>
            <li class="wow fadeInUp">
                <input type="checkbox" checked>
                <i></i>
                <h2 class="title_block"><?= $item['name'] ?></h2>
                <div class="partner-msg msg">
                    <div class="partner-slider owl-carousel owl-theme owl-loaded">
                        <?php for ($i = 0; $i < count($item['photos']); $i++): ?>
                            <div>
                                <img src="<?= Url::to($item['photos'][$i]) ?>" alt="part-sl-img">
                            </div>
                        <?php endfor; ?>
                    </div>
                    <div>
                        <?= $item['text'] ?>
                    </div>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>