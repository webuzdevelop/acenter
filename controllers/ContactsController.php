<?php

namespace app\controllers;

use app\models\About;
use app\models\Dictionary;
use Yii;
use app\models\Feedback;
use app\models\FeedbackSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

class ContactsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'feedback' => ['get'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $session = Yii::$app->session;
        $lang = $session['language'] == null ? 'ru' : $session['language'];
        $dictionary = [];
        switch ($lang) {
            case 'ru':
                {
                    $dictionary = [
                        'adres' => Dictionary::findOne(['key' => 'address'])->rus,
                    'email' => Dictionary::findOne(['key' => 'email'])->rus,
                    'phone' => Dictionary::findOne(['key' => 'phone'])->rus,
                    'social' => Dictionary::findOne(['key' => 'social'])->rus,
                    'feedback' => Dictionary::findOne(['key' => 'feedback'])->rus,
                    'fio' => Dictionary::findOne(['key' => 'fio'])->rus,
                    'message' => Dictionary::findOne(['key' => 'message'])->rus,
                    'send' => Dictionary::findOne(['key' => 'send'])->rus,
                    ];
                }
                break;
            case 'uz':
                {
                    $dictionary = [
                        'adres' => Dictionary::findOne(['key' => 'address'])->uzb,
                    'email' => Dictionary::findOne(['key' => 'email'])->uzb,
                    'phone' => Dictionary::findOne(['key' => 'phone'])->uzb,
                    'social' => Dictionary::findOne(['key' => 'social'])->uzb,
                    'feedback' => Dictionary::findOne(['key' => 'feedback'])->uzb,
                    'fio' => Dictionary::findOne(['key' => 'fio'])->uzb,
                    'message' => Dictionary::findOne(['key' => 'message'])->uzb,
                    'send' => Dictionary::findOne(['key' => 'send'])->uzb,
                    ];
                }
                break;
            case 'en':
                {
                    $dictionary = [
                        'adres' => Dictionary::findOne(['key' => 'address'])->eng,
                    'email' => Dictionary::findOne(['key' => 'email'])->eng,
                    'phone' => Dictionary::findOne(['key' => 'phone'])->eng,
                    'social' => Dictionary::findOne(['key' => 'social'])->eng,
                    'feedback' => Dictionary::findOne(['key' => 'feedback'])->eng,
                    'fio' => Dictionary::findOne(['key' => 'fio'])->eng,
                    'message' => Dictionary::findOne(['key' => 'message'])->eng,
                    'send' => Dictionary::findOne(['key' => 'send'])->eng,
                    ];
                }
                break;
        }
        $about = About::findOne(1);
        $adres = 'address_'.$lang;
        if($about->$adres == '') $adres = 'address_ru';
        $items =[
            'adres' => $about->$adres,
            'email' => $about->email,
            'phone' => $about->phone,
            'google' => $about->google,
            'facebook' => $about->facebook,
            'instagram' => $about->instagram,
            'twitter' => $about->twitter,
            'coordinate_x' => $about->coordinate_x,
            'coordinate_y' => $about->coordinate_y,
        ];
        return $this->render('index', [
            'items' => $items,
            'dictionary' => $dictionary
        ]);
    }

    public function actionFeedback()
    {
        $model = new Feedback();
        $model->name = Yii::$app->request->get('name');
        $model->email = Yii::$app->request->get('email');
        $model->phone = Yii::$app->request->get('phone');
        $model->message = Yii::$app->request->get('message');
        $model->date_cr = date('Y-m-d H:i:s');
        $model->save();
        return $this->redirect('index');
    }
}
