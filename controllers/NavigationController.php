<?php

namespace app\controllers;

use app\models\Sliders;
use Yii;
use app\models\Feedback;
use app\models\FeedbackSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Shops;
use app\models\Floors;

class NavigationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex($search = null, $floor = null)
    {
        $session = Yii::$app->session;
        $controller = Yii::$app->controller->id;
        $lang = $session['language'] == null ? 'ru' : $session['language'];

        $firstFloor = Floors::find()->where(['name' => $floor])->one();
        if($firstFloor == null) {
            $firstFloor = Floors::find()->one();
            if($search == null) $shops = Shops::find()->all();
            else $shops = Shops::find()->where(['like', 'name', $search])->all();
        }
        else{
            if($search == null) $shops = Shops::find()->where(['floor_id' => $firstFloor->id])->all();
            else $shops = Shops::find()->where(['like', 'name', $search])->andWhere(['floor_id' => $firstFloor->id])->all();
        }

        $floors = Floors::find()->all();

        $allSliders = Sliders::find()->where(['type' => 'navigation'])->all();
        $sliders = [];
        foreach ($allSliders as $item) {
            $photo = $item->photo != null ? '/uploads/sliders/' . $item->photo : '/images/no-image.jpg';
            $sliders [] = [
                'photo' => $photo
            ];
        }
        return $this->render('index', [
            'lang' => $lang,
            'shops' => $shops,
            'floor' => $floor,
            'search' => $search,
            'floors' => $floors,
            'controller' => $controller,
            'firstFloor' => $firstFloor,
            'sliders' => $sliders,
        ]);
    }

}
