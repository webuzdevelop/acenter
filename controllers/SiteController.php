<?php

namespace app\controllers;

use app\models\About;
use app\models\Dictionary;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Reviews;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $allReviews = Reviews::find()->all();
        $allAbout = About::findOne(1);
        $session = Yii::$app->session;
        $lang = $session['language'] == null ? 'ru' : $session['language'];
        $about_text = 'about_text_' . $lang;
        if($allAbout->$about_text == '') $about_text = 'about_text_ru';
        $titles = [];
        switch ($lang) {
            case 'ru':
                {
                    $text = $allAbout->text_ru;
                    $titles = [
                        'our_center' => Dictionary::findOne(['key' => 'our center'])->rus,
                        'about_company' => Dictionary::findOne(['key' => 'reviews about company'])->rus,
                        'company_review' => Dictionary::findOne(['key' => 'company_review'])->rus
                    ];
                };
                break;
            case 'uz':
                {
                    $text = $allAbout->text_uz;
                    $titles = [
                        'our_center' => Dictionary::findOne(['key' => 'our center'])->uzb,
                        'about_company' => Dictionary::findOne(['key' => 'reviews about company'])->uzb,
                        'company_review' => Dictionary::findOne(['key' => 'company_review'])->uzb
                    ];
                };
                break;
            case 'en':
                {
                    $text = $allAbout->text_en;
                    $titles = [
                        'our_center' => Dictionary::findOne(['key' => 'our center'])->eng,
                        'about_company' => Dictionary::findOne(['key' => 'reviews about company'])->eng,
                        'company_review' => Dictionary::findOne(['key' => 'company_review'])->eng
                    ];
                }
                break;
        }

        $about = [
            'logo' => $allAbout->logo != null ? '/images/' . $allAbout->logo : '/images/no-image.jpg',
            'foto' => $allAbout->image_about_company != null ? '/images/' . $allAbout->image_about_company : '/images/no-image.jpg',
            'about_text' => $allAbout->$about_text,
            'text' => $text,
        ];
        $items = [];
        foreach ($allReviews as $value) {
            $permission = 'permission_' . $lang;
            $title = 'title_' . $lang;
            $text = 'text_' . $lang;
            if($value->$permission == '') $permission = 'permission_ru';
            if($value->$title == '') $title = 'title_ru';
            if($value->$text == '') $text = 'text_ru';
            $avatar = $value->avatar != null ? '/uploads/review/' . $value->avatar : '/images/no-image.jpg';
            $items [] = [
                'id' => $value->id,
                'avatar' => $avatar,
                'fio' => $value->fio,
                'permission' => $value->$permission,
                'title' => $value->$title,
                'text' => $value->$text,
            ];
        }
        return $this->render('index', ['items' => $items, 'lang' => $lang, 'about' => $about , 'titles' => $titles]);
    }

}
