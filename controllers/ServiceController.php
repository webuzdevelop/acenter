<?php

namespace app\controllers;

use app\models\Dictionary;
use Yii;
use app\models\Service;
use app\models\ServiceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * ServiceController implements the CRUD actions for Service model.
 */
class ServiceController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Service models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $lang = $session['language'] == null ? 'ru' : $session['language'];
        $allService = Service::find()->all();
        $title = '';
        switch ($lang) {
            case 'ru':
                $title = Dictionary::findOne(['key' => 'sevice'])->rus;
                break;
            case 'uz':
                $title = Dictionary::findOne(['key' => 'sevice'])->uzb;
                break;
            case 'en':
                $title = Dictionary::findOne(['key' => 'sevice'])->eng;
                break;
        }
        $items = [];
        foreach ($allService as $item) {
            $photo = $item->photo != null ? '/uploads/service/' . $item->photo : '/images/no-image.jpg';
            $name = 'name_'.$lang;
            $text = 'text_'.$lang;
            if($item->$name == '') $name = 'name_ru';
            if($item->$text == '') $text = 'text_ru';
            $items[] = [
              'photo' => $photo,
              'name' => $item->$name,
              'text' => $item->$text,
              'price' => $item->price
            ];
        }
        return $this->render('index', ['title' => $title, 'items' => $items]);
    }


}
