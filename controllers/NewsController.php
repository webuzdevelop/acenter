<?php

namespace app\controllers;

use app\models\Dictionary;
use Yii;
use app\models\News;
use app\models\NewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

class NewsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $allNews = News::find()->where(['type' => 1])->all();
        $session = Yii::$app->session;
        $lang = $session['language'] == null ? 'ru' : $session['language'];
        $dictionary = '';
        switch ($lang) {
            case 'ru':
                {
                    $dictionary = Dictionary::findOne(['key' => 'read_more'])->rus;
                }
                break;
            case 'uz':
                {
                    $dictionary = Dictionary::findOne(['key' => 'read_more'])->uzb;
                }
                break;
            case 'en':
                {
                    $dictionary = Dictionary::findOne(['key' => 'read_more'])->eng;
                }
                break;
        }
        $items = [];
        foreach ($allNews as $value) {
            $title = 'title_' . $lang;
            $text = 'text_' . $lang;
            if($value->$title == '') $title = 'title_ru';
            if($value->$text == '') $text = 'text_ru';
            $fone = $value->fone != null ? '/uploads/news/' . $value->fone : '/images/no-image.jpg';
            $items [] = [
                'id' => $value->id,
                'fone' => $fone,
                'date' => $value->date != null ? date('d.m.Y', strtotime($value->date) ) : '',
                'title' => $value->$title,
                'text' => $value->$text,
            ];
        }
        return $this->render('index', [
            'items' => $items,
            'lang' => $lang,
            'dictionary' => $dictionary
        ]);
    }

    public function actionStock()
    {
        $allNews = News::find()->where(['type' => 2])->all();
        $session = Yii::$app->session;
        $lang = $session['language'] == null ? 'ru' : $session['language'];
        $dictionary = '';
        switch ($lang) {
            case ru:
                {
                    $dictionary = Dictionary::findOne(['key' => 'read_more'])->rus;
                }
                break;
            case uz:
                {
                    $dictionary = Dictionary::findOne(['key' => 'read_more'])->uzb;
                }
                break;
            case en:
                {
                    $dictionary = Dictionary::findOne(['key' => 'read_more'])->eng;
                }
                break;
        }
        $items = [];
        foreach ($allNews as $value) {
            $title = 'title_' . $lang;
            $text = 'text_' . $lang;
            if($value->$title == '') $title = 'title_ru';
            if($value->$text == '') $text = 'text_ru';
            $fone = $value->fone != null ? '/uploads/news/' . $value->fone : '/images/no-image.jpg';
            $items [] = [
                'id' => $value->id,
                'fone' => $fone,
                'date' => $value->date != null ? date('d.m.Y', strtotime($value->date) ) : '',
                'title' => $value->$title,
                'text' => $value->$text,
            ];
        }
        return $this->render('index', [
            'items' => $items,
            'lang' => $lang,
            'dictionary' => $dictionary,
        ]);
    }

    public function actionView($id)
    {
        $news = News::findOne($id);
        $session = Yii::$app->session;
        $lang = $session['language'] == null ? 'ru' : $session['language'];
        $title = 'title_' . $lang;
        $text = 'text_' . $lang;
        if($news->$title == '') $title = 'title_ru';
        if($news->$text == '') $text = 'text_ru';
        $session['new'] = $news->$title;
        $description = 'description_' . $lang;
        if($news->$description == '') $description = 'description_ru';
        $fone = $news->fone != null ? '/uploads/news/' . $news->fone : '/images/no-image.jpg';
        $photo = $news->photo != null ? '/uploads/news/' . $news->photo : '/images/no-image.jpg';
        $news = [
            'id' => $news->id,
            'fone' => $fone,
            'description' => $news->$description,
            'photo' => $photo,
            'date' => $news->date != null ? date('d.m.Y', strtotime($news->date) ) : '',
            'title' => $news->$title,
            'text' => $news->$text,
        ];
        return $this->render('view', [
            'news' => $news,
            'lang' => $lang,
        ]);
    }
}
