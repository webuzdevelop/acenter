<?php

namespace app\controllers;

use app\models\About;
use app\models\Dictionary;
use Yii;
use app\models\Shops;
use app\models\ShopsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

class ShopsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Shops models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $lang = $session['language'] == null ? 'ru' : $session['language'];
        $allShops = Shops::find()->all();
        $items = [];
        foreach ($allShops as $item) {
            $title = 'title_'.$lang;
            $text = 'text_'.$lang;
            if($item->$title == '') $title = 'title_ru';
            if($item->$text == '') $text = 'text_ru';
            $logo = $item->logo != null ? '/uploads/shops/' . $item->logo : '/images/no-image.jpg';
            $items[] = [
                'id' => $item->id,
                'logo' => $logo,
                'name' => $item->name,
                'title' => $item->$title,
                'text' => $item->$text,
            ];
        }
        return $this->render('index', ['items' => $items]);
    }

    public function actionResident($id)
    {

        $session = Yii::$app->session;
        $lang = $session['language'] == null ? 'ru' : $session['language'];
        $shop = Shops::findOne(['id' => $id]);
        $allAbout = About::findOne(1);
        $shop_items = [];
        $logo = $shop->logo != null ? '/uploads/shops/' . $shop->logo : '/images/no-image.jpg';
        $photo1 = $shop->photo1 != null ? '/uploads/shops/' . $shop->photo1 : '/images/no-image.jpg';
        $photo2 = $shop->photo2 != null ? '/uploads/shops/' . $shop->photo2 : '/images/no-image.jpg';
        $text = 'text_'.$lang;
        if($shop->$text == '') $text = 'text_ru';
        $text_top = 'text_top_'.$lang;
        if($shop->$text_top == '') $text_top = 'text_top_ru';
        $text_bottom = 'text_bottom_'.$lang;
        if($shop->$text_bottom == '') $text_bottom = 'text_bottom_ru';
        $shop_items = [
            'logo' => $logo,
            'photo1' => $photo1,
            'photo2' => $photo2,
            'text' => $shop->$text,
            'text_top' => $shop->$text_top,
            'text_bottom' => $shop->$text_bottom,
        ];
        $about = [];
        $adres = 'address_'.$lang;
        if($allAbout->$adres == '') $adres = 'address_ru';
        $about = [
            'adres' => $allAbout->$adres,
            'phone' => $allAbout->phone,
            'email' => $allAbout->email,
            'instagram' => $allAbout->instagram,
            'facebook' => $allAbout->facebook,
            'twitter' => $allAbout->twitter,
            'google' => $allAbout->google
        ];
        $dictionary = [];
        switch ($lang) {
            case 'ru':
                {
                    $dictionary = [
                        'adres' => Dictionary::findOne(['key' => 'address'])->rus,
                        'email' => Dictionary::findOne(['key' => 'email'])->rus,
                        'phone' => Dictionary::findOne(['key' => 'phone'])->rus,
                        'social' => Dictionary::findOne(['key' => 'social'])->rus,
                        'clock' => Dictionary::findOne(['key' => 'clock'])->rus,
                        'working_time' => Dictionary::findOne(['key' => 'working_time'])->rus,
                    ];
                }
                break;
            case 'uz':
                {
                    $dictionary = [
                        'adres' => Dictionary::findOne(['key' => 'address'])->uzb,
                        'email' => Dictionary::findOne(['key' => 'email'])->uzb,
                        'phone' => Dictionary::findOne(['key' => 'phone'])->uzb,
                        'social' => Dictionary::findOne(['key' => 'social'])->uzb,
                        'clock' => Dictionary::findOne(['key' => 'clock'])->uzb,
                        'working_time' => Dictionary::findOne(['key' => 'working_time'])->uzb,
                    ];
                }
                break;
            case 'en':
                {
                    $dictionary = [
                        'adres' => Dictionary::findOne(['key' => 'address'])->eng,
                        'email' => Dictionary::findOne(['key' => 'email'])->eng,
                        'phone' => Dictionary::findOne(['key' => 'phone'])->eng,
                        'social' => Dictionary::findOne(['key' => 'social'])->eng,
                        'clock' => Dictionary::findOne(['key' => 'clock'])->eng,
                        'working_time' => Dictionary::findOne(['key' => 'working_time'])->eng,
                    ];
                }
                break;
        }
        return $this->render('resident', ['shop_items' => $shop_items, 'dictionary' => $dictionary, 'shop' => $shop, 'about' => $about]);
    }

}
