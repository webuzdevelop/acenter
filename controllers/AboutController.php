<?php

namespace app\controllers;

use app\models\Commands;
use app\models\Dictionary;
use app\models\Partners;
use app\models\Sliders;
use app\models\Vacancy;
use Yii;
use app\models\About;
use app\models\AboutSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

class AboutController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all About models.
     * @return mixed
     */
    public function actionIndex()
    {
        $about = About::findOne(1);
        $allCommands = Commands::find()->all();
        $session = Yii::$app->session;
        $lang = $session['language'] == null ? 'ru' : $session['language'];
        $title = [];
        switch ($lang) {
            case 'ru':
                {
                    $title = [
                        'email' => Dictionary::findOne(['key' => 'email'])->rus,
                        'phone' => Dictionary::findOne(['key' => 'phone'])->rus
                    ];
                    $session['breadcrumb'] = Dictionary::findOne(['key' => 'about_company'])->rus;
                }
                break;
            case 'uz':
                {
                    $title = [
                        'email' => Dictionary::findOne(['key' => 'email'])->uzb,
                        'phone' => Dictionary::findOne(['key' => 'phone'])->uzb
                    ];
                    $session['breadcrumb'] = Dictionary::findOne(['key' => 'about_company'])->uzb;
                }
                break;
            case 'en':
                {
                    $title = [
                        'email' => Dictionary::findOne(['key' => 'email'])->eng,
                        'phone' => Dictionary::findOne(['key' => 'phone'])->eng
                    ];
                    $session['breadcrumb'] = Dictionary::findOne(['key' => 'about_company'])->eng;
                }
                break;
        }

        $text = 'partners_text_' . $lang;
        $about_company = $about->$text;
        $Commands;
        foreach ($allCommands as $item) {
            $description = 'description_' . $lang;
            if($item->$description == '') $description = 'description_ru';
            $Commands[] = [
                'fio' => $item->fio,
                'permission' => $item->permission,
                'email' => $item->email,
                'photo' => $item->photo != null ? '/uploads/commands/' . $item->photo : '/images/no-image.jpg',
                'description' => $item->$description,
                'phone' => $item->phone,
            ];
        }
        $allSliders = Sliders::find()->where(['type' => 'about'])->all();
        $sliders = [];
        foreach ($allSliders as $item) {
            $photo = $item->photo != null ? '/uploads/sliders/' . $item->photo : '/images/no-image.jpg';
            $sliders [] = [
                'photo' => $photo,
                'link' => $item->link,
            ];
        }

        return $this->render('index', [
            'about_company' => $about_company,
            'Commands' => $Commands,
            'title' => $title,
            'sliders' => $sliders
        ]);
    }

    public function actionVacancy()
    {
        $allVacancy = Vacancy::find()->all();
        $session = Yii::$app->session;
        $lang = $session['language'] == null ? 'ru' : $session['language'];
        $title = '';
        switch ($lang) {
            case 'ru':
                {
                    $title = Dictionary::findOne(['key' => 'jobs'])->rus;
                    $session['breadcrumb'] = Dictionary::findOne(['key' => 'jobs'])->rus;
                }
                break;
            case 'uz':
                {
                    $session['breadcrumb'] = Dictionary::findOne(['key' => 'jobs'])->uzb;
                    $title = Dictionary::findOne(['key' => 'jobs'])->uzb;
                }
                break;
            case 'en':
                {
                    $session['breadcrumb'] = Dictionary::findOne(['key' => 'jobs'])->eng;
                    $title = Dictionary::findOne(['key' => 'jobs'])->eng;
                }
                break;
        }
        $items = [];
        foreach ($allVacancy as $item) {
            $name = 'name_' . $lang;
            if($item->$name == '') $name = 'name_ru';
            $text = 'text_' . $lang;
            if($item->$text == '') $text = 'text_ru';
            $items [] = [
                'name' => $item->$name,
                'text' => $item->$text,
            ];
        }
        return $this->render('vacancy', ['items' => $items, 'title' => $title]);
    }

    public function actionPartner()
    {
        $session = Yii::$app->session;
        $lang = $session['language'] == null ? 'ru' : $session['language'];
        $title = '';
        switch ($lang) {
            case 'ru':
                {
                    $session['breadcrumb'] = Dictionary::findOne(['key' => 'to_partner'])->rus;
                    $title = Dictionary::findOne(['key' => 'to_partner'])->rus;
                }
                break;
            case 'uz':
                {
                    $session['breadcrumb'] = Dictionary::findOne(['key' => 'to_partner'])->uzb;
                    $title = Dictionary::findOne(['key' => 'to_partner'])->uzb;
                }
                break;
            case 'en':
                {
                    $session['breadcrumb'] = Dictionary::findOne(['key' => 'to_partner'])->eng;
                    $title = Dictionary::findOne(['key' => 'to_partner'])->eng;
                }
                break;
        }
        $allPartners = Partners::find()->all();
        $items = [];
        foreach ($allPartners as $item) {
            $name = 'name_' . $lang;
            if($item->$name == '') $name = 'name_ru';
            $text = 'text_' . $lang;
            if($item->$text == '') $text = 'text_ru';
            $photos = explode("**", $item->photos);
            $result = [];
            for ($i = 0; $i < count($photos); $i++) {
                if($photos[$i] != null) $result [] = '/uploads/partners/' . $item->id . '/' . $photos[$i];
                /*$photos[$i] = $photos[$i] != null ? '/uploads/partners/' . $item->id . '/' . $photos[$i] : '/images/no-image.jpg';*/
            }
            $items [] = [
                'photos' => $result,
                'name' => $item->$name,
                'text' => $item->$text,
            ];

        }

        return $this->render('partner', ['items' => $items, 'title' => $title]);
    }
}
