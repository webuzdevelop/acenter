<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m180601_154502_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('ФИО'),//required
            'login' => $this->string(255)->unique()->comment('Логин/Е-mail'),//required
            'password' => $this->string(255)->comment('Пароль'),//required
            'telephone' => $this->string(255)->comment('Телефон'),//required
            'role_id' => $this->string(255)->comment('Должность'),
            'status' => $this->string(255)->comment('Статус'),
            'auth_key' => $this->string(255)->comment('Пароль'),//required
            'data_cr' => $this->date()->comment('Дата регистрации'),
        ]);

        $this->insert('users',array(
            'name' => 'Иванов Иван Иванович',
            'login' => 'admin',
            'password' => md5('admin'),
            'telephone' => '+998 90 900 00 00',
            'role_id' => 'administrator', //администратор
            'status' => 'working',
            'auth_key' => 'admin',
            'data_cr' => date('Y-m-d'),
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
