<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%menus}}`.
 */
class m190707_132641_create_menus_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('menus', [
            'id' => $this->primaryKey(),
            'name_uz' => $this->string(255)->comment('Nomi'),
            'name_ru' => $this->string(255)->comment('Наименование'),
            'name_en' => $this->string(255)->comment('Name'),
            'visible' => $this->boolean()->comment('Скрыть'),
            'key' => $this->string(255)->comment('Ключ'),
            'title_uz' => $this->string()->comment('Title'),
            'title_ru' => $this->string()->comment('Sarlavha'),
            'title_en' => $this->string()->comment('Заглавие')
        ]);

        $this->insert('menus',array(
            'name_uz' => 'Asosiy',
            'name_ru' => 'Главная',
            'name_en' => 'Main',
            'visible' => false,
            'key' => 'Main',
            'title_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz professional uchrashuv bilan uchrashasiz',
            'title_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча',
            'title_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting',
        ));

        $this->insert('menus',array(
            'name_uz' => 'Kompaniya haqida',
            'name_ru' => 'О компании',
            'name_en' => 'About company',
            'visible' => false,
            'key' => 'About company',
            'title_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz professional uchrashuv bilan uchrashasiz',
            'title_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча',
            'title_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting'

        ));

        $this->insert('menus',array(
            'name_uz' => 'Xizmatlar',
            'name_ru' => 'Услуги',
            'name_en' => 'Services',
            'visible' => false,
            'key' => 'Services',
            'title_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz professional uchrashuv bilan uchrashasiz',
            'title_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча',
            'title_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting'

        ));

        $this->insert('menus',array(
            'name_uz' => 'Do\'konlar',
            'name_ru' => 'Магазины',
            'name_en' => 'Shops',
            'visible' => false,
            'key' => 'Shops',
            'title_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz professional uchrashuv bilan uchrashasiz',
            'title_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча',
            'title_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting'

        ));

        $this->insert('menus',array(
            'name_uz' => 'Yangiliklar',
            'name_ru' => 'Новости',
            'name_en' => 'News',
            'visible' => false,
            'key' => 'News',
            'title_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz professional uchrashuv bilan uchrashasiz',
            'title_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча',
            'title_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting'

        ));

        $this->insert('menus',array(
            'name_uz' => 'Biz bilan bog\'lanish',
            'name_ru' => 'Контакты',
            'name_en' => 'Contacts',
            'visible' => false,
            'key' => 'Contacts',
            'title_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz professional uchrashuv bilan uchrashasiz',
            'title_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча',
            'title_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting'

        ));

        $this->insert('menus',array(
            'name_uz' => 'Navigatsiya',
            'name_ru' => 'Навигация',
            'name_en' => 'Navigation',
            'visible' => false,
            'key' => 'Navigation',
            'title_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz professional uchrashuv bilan uchrashasiz',
            'title_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча',
            'title_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting'

        ));

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('menus');
    }
}
