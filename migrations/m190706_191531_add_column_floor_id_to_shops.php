<?php

use yii\db\Migration;
use app\models\Shops;

/**
 * Class m190706_191531_add_column_floor_id_to_shops
 */
class m190706_191531_add_column_floor_id_to_shops extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('shops','floor_id',$this->integer()->comment('Магазин id'));
        $shops = Shops::find()->all();
        foreach ($shops as $shop) {
            $shop->floor_id = rand (1,5);
            $shop->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn('shops','floor_id');
        return false;
    }

}
