<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%feedback}}`.
 */
class m190704_063719_create_feedback_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('feedback', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('ФИО или Название компании'),
            'email' => $this->string(255)->comment('Е-mail'),
            'phone' => $this->string(255)->comment('Телефон'),
            'message' => $this->text()->comment('Сообщение'),
            'date_cr' => $this->dateTime()->comment('Дата создание')
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('feedback');
    }
}
