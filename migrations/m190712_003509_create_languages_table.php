<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%languages}}`.
 */
class m190712_003509_create_languages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('{{%languages}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(50),
            'name' => $this->string(50),
            'accept' => $this->boolean()
        ]);

        $this->insert('languages', [
            'title' => 'ru',
            'name' => 'Русский',
            'accept' => true,
        ]);

        $this->insert('languages', [
            'title' => 'uz',
            'name' => 'O\'zbekcha',
            'accept' => true,
        ]);

        $this->insert('languages', [
            'title' => 'en',
            'name' => 'English',
            'accept' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('{{%languages}}');
    }
}
