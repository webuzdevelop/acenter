<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%service}}`.
 */
class m190704_202310_create_service_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('service', [
            'id' => $this->primaryKey(),
            'photo' => $this->string(255)->comment('Фото'),
            'price' => $this->float()->comment('Цена')->defaultValue(0.0),
            'name_uz' => $this->string(255)->comment('Nomi'),
            'name_ru' => $this->string(255)->comment('Наименование'),
            'name_en' => $this->string(255)->comment('Name'),
            'text_uz' => $this->text()->comment('Punktlar'),
            'text_ru' => $this->text()->comment('Предметы'),
            'text_en' => $this->text()->comment('Items'),
        ]);

        $this->insert('service',array(
            'photo' => 'service_img1.png',
            'price' => '34325',
            'name_uz' => 'Starter',
            'name_ru' => 'Стартер',
            'name_en' => 'Starter',
            'text_uz' => '<ul class="service-content-block-ul">
                   <li class="service-content-block-li service-content-block-li-blue">Ishlay boshlayotgan jamoalar uchun asosiy HZ versiyamiz..</li>
                   <li class="service-content-block-li service-content-block-li-blue">Cheksiz vazifalar, loyihalar va suhbatlar</li>
                   <li class="service-content-block-li service-content-block-li-blue">15 nafar jamoa a\'zolari</li>
                   <li class="service-content-block-li">Asosiy panellar</li>
                   <li class="service-content-block-li">Asosiy qidirish</li>
               </ul>',
            'text_ru' => '<ul class="service-content-block-ul">
                   <li class="service-content-block-li service-content-block-li-blue">Наша базовая версия ХЗ для команд, которые только начинают работать.</li>
                   <li class="service-content-block-li service-content-block-li-blue">Неограниченные задачи, проекты и разговоры</li>
                   <li class="service-content-block-li service-content-block-li-blue">До 15 членов команды</li>
                   <li class="service-content-block-li">Основные панели</li>
                   <li class="service-content-block-li">Базовый поиск</li>
               </ul>',
            'text_en' => '<ul class="service-content-block-ul">
                   <li class="service-content-block-li service-content-block-li-blue">Our basic HZ version for teams that are just starting to work..</li>
                   <li class="service-content-block-li service-content-block-li-blue">Unlimited tasks, projects and conversations</li>
                   <li class="service-content-block-li service-content-block-li-blue">Up to 15 team members</li>
                   <li class="service-content-block-li">Main panels</li>
                   <li class="service-content-block-li">Basic search</li>
               </ul>',
        ));

        $this->insert('service',array(
            'photo' => 'service_img2.png',
            'price' => '34325',
            'name_uz' => 'Starter',
            'name_ru' => 'Стартер',
            'name_en' => 'Starter',
            'text_uz' => '<ul class="service-content-block-ul">
                   <li class="service-content-block-li service-content-block-li-blue">Ishlay boshlayotgan jamoalar uchun asosiy HZ versiyamiz..</li>
                   <li class="service-content-block-li service-content-block-li-blue">Cheksiz vazifalar, loyihalar va suhbatlar</li>
                   <li class="service-content-block-li service-content-block-li-blue">15 nafar jamoa a\'zolari</li>
                   <li class="service-content-block-li">Asosiy panellar</li>
                   <li class="service-content-block-li">Asosiy qidirish</li>
               </ul>',
            'text_ru' => '<ul class="service-content-block-ul">
                   <li class="service-content-block-li service-content-block-li-blue">Наша базовая версия ХЗ для команд, которые только начинают работать.</li>
                   <li class="service-content-block-li service-content-block-li-blue">Неограниченные задачи, проекты и разговоры</li>
                   <li class="service-content-block-li service-content-block-li-blue">До 15 членов команды</li>
                   <li class="service-content-block-li">Основные панели</li>
                   <li class="service-content-block-li">Базовый поиск</li>
               </ul>',
            'text_en' => '<ul class="service-content-block-ul">
                   <li class="service-content-block-li service-content-block-li-blue">Our basic HZ version for teams that are just starting to work..</li>
                   <li class="service-content-block-li service-content-block-li-blue">Unlimited tasks, projects and conversations</li>
                   <li class="service-content-block-li service-content-block-li-blue">Up to 15 team members</li>
                   <li class="service-content-block-li">Main panels</li>
                   <li class="service-content-block-li">Basic search</li>
               </ul>',
        ));

        $this->insert('service',array(
            'photo' => 'service_img3.png',
            'price' => '34325',
            'name_uz' => 'Starter',
            'name_ru' => 'Стартер',
            'name_en' => 'Starter',
            'text_uz' => '<ul class="service-content-block-ul">
                   <li class="service-content-block-li service-content-block-li-blue">Ishlay boshlayotgan jamoalar uchun asosiy HZ versiyamiz..</li>
                   <li class="service-content-block-li service-content-block-li-blue">Cheksiz vazifalar, loyihalar va suhbatlar</li>
                   <li class="service-content-block-li service-content-block-li-blue">15 nafar jamoa a\'zolari</li>
                   <li class="service-content-block-li">Asosiy panellar</li>
                   <li class="service-content-block-li">Asosiy qidirish</li>
               </ul>',
            'text_ru' => '<ul class="service-content-block-ul">
                   <li class="service-content-block-li service-content-block-li-blue">Наша базовая версия ХЗ для команд, которые только начинают работать.</li>
                   <li class="service-content-block-li service-content-block-li-blue">Неограниченные задачи, проекты и разговоры</li>
                   <li class="service-content-block-li service-content-block-li-blue">До 15 членов команды</li>
                   <li class="service-content-block-li">Основные панели</li>
                   <li class="service-content-block-li">Базовый поиск</li>
               </ul>',
            'text_en' => '<ul class="service-content-block-ul">
                   <li class="service-content-block-li service-content-block-li-blue">Our basic HZ version for teams that are just starting to work..</li>
                   <li class="service-content-block-li service-content-block-li-blue">Unlimited tasks, projects and conversations</li>
                   <li class="service-content-block-li service-content-block-li-blue">Up to 15 team members</li>
                   <li class="service-content-block-li">Main panels</li>
                   <li class="service-content-block-li">Basic search</li>
               </ul>',
        ));

        $this->insert('service',array(
            'photo' => 'service_img4.png',
            'price' => '34325',
            'name_uz' => 'Starter',
            'name_ru' => 'Стартер',
            'name_en' => 'Starter',
            'text_uz' => '<ul class="service-content-block-ul">
                   <li class="service-content-block-li service-content-block-li-blue">Ishlay boshlayotgan jamoalar uchun asosiy HZ versiyamiz..</li>
                   <li class="service-content-block-li service-content-block-li-blue">Cheksiz vazifalar, loyihalar va suhbatlar</li>
                   <li class="service-content-block-li service-content-block-li-blue">15 nafar jamoa a\'zolari</li>
                   <li class="service-content-block-li">Asosiy panellar</li>
                   <li class="service-content-block-li">Asosiy qidirish</li>
               </ul>',
            'text_ru' => '<ul class="service-content-block-ul">
                   <li class="service-content-block-li service-content-block-li-blue">Наша базовая версия ХЗ для команд, которые только начинают работать.</li>
                   <li class="service-content-block-li service-content-block-li-blue">Неограниченные задачи, проекты и разговоры</li>
                   <li class="service-content-block-li service-content-block-li-blue">До 15 членов команды</li>
                   <li class="service-content-block-li">Основные панели</li>
                   <li class="service-content-block-li">Базовый поиск</li>
               </ul>',
            'text_en' => '<ul class="service-content-block-ul">
                   <li class="service-content-block-li service-content-block-li-blue">Our basic HZ version for teams that are just starting to work..</li>
                   <li class="service-content-block-li service-content-block-li-blue">Unlimited tasks, projects and conversations</li>
                   <li class="service-content-block-li service-content-block-li-blue">Up to 15 team members</li>
                   <li class="service-content-block-li">Main panels</li>
                   <li class="service-content-block-li">Basic search</li>
               </ul>',
        ));

        $this->insert('service',array(
            'photo' => 'service_img5.png',
            'price' => '34325',
            'name_uz' => 'Starter',
            'name_ru' => 'Стартер',
            'name_en' => 'Starter',
            'text_uz' => '<ul class="service-content-block-ul">
                   <li class="service-content-block-li service-content-block-li-blue">Ishlay boshlayotgan jamoalar uchun asosiy HZ versiyamiz..</li>
                   <li class="service-content-block-li service-content-block-li-blue">Cheksiz vazifalar, loyihalar va suhbatlar</li>
                   <li class="service-content-block-li service-content-block-li-blue">15 nafar jamoa a\'zolari</li>
                   <li class="service-content-block-li">Asosiy panellar</li>
                   <li class="service-content-block-li">Asosiy qidirish</li>
               </ul>',
            'text_ru' => '<ul class="service-content-block-ul">
                   <li class="service-content-block-li service-content-block-li-blue">Наша базовая версия ХЗ для команд, которые только начинают работать.</li>
                   <li class="service-content-block-li service-content-block-li-blue">Неограниченные задачи, проекты и разговоры</li>
                   <li class="service-content-block-li service-content-block-li-blue">До 15 членов команды</li>
                   <li class="service-content-block-li">Основные панели</li>
                   <li class="service-content-block-li">Базовый поиск</li>
               </ul>',
            'text_en' => '<ul class="service-content-block-ul">
                   <li class="service-content-block-li service-content-block-li-blue">Our basic HZ version for teams that are just starting to work..</li>
                   <li class="service-content-block-li service-content-block-li-blue">Unlimited tasks, projects and conversations</li>
                   <li class="service-content-block-li service-content-block-li-blue">Up to 15 team members</li>
                   <li class="service-content-block-li">Main panels</li>
                   <li class="service-content-block-li">Basic search</li>
               </ul>',
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('service');
    }
}
