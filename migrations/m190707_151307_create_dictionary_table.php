<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%dictionary}}`.
 */
class m190707_151307_create_dictionary_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('dictionary', [
            'id' => $this->primaryKey(),
            'key' => $this->string(255)->comment('Ключ'),
            'uzb' => $this->text()->comment('O\'zbekcha'),
            'rus' => $this->text()->comment('Русский'),
            'eng' => $this->text()->comment('English'),
        ]);

        $this->insert('dictionary', array(
            'key' => 'company_review',
            'uzb' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz professional uchrashuv bilan uchrashasiz',
            'rus' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча',
            'eng' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting',
        ));

        $this->insert('dictionary', array(
            'key' => 'address',
            'uzb' => 'Manzil',
            'rus' => 'Адрес',
            'eng' => 'Address',
        ));

        $this->insert('dictionary', array(
            'key' => 'phone',
            'uzb' => 'Telefon',
            'rus' => 'Телефон',
            'eng' => 'Phone',
        ));

        $this->insert('dictionary', array(
            'key' => 'email',
            'uzb' => 'Email',
            'rus' => 'Email',
            'eng' => 'Email',
        ));

        $this->insert('dictionary', array(
            'key' => 'social',
            'uzb' => 'Ijtimoiy tarmoqlar',
            'rus' => 'Соцсети',
            'eng' => 'Social network',
        ));

        $this->insert('dictionary', array(
            'key' => 'site_developed',
            'uzb' => 'Sayt ishlab chiqildi',
            'rus' => 'Сайт разработан',
            'eng' => 'Site developed',
        ));

        $this->insert('dictionary', array(
            'key' => 'jobs',
            'uzb' => 'Ishlar',
            'rus' => 'Вакансии',
            'eng' => 'Jobs',
        ));

        $this->insert('dictionary', array(
            'key' => 'to_partner',
            'uzb' => 'Hamkorlarga',
            'rus' => 'Партнёрам',
            'eng' => 'To partners',
        ));

        $this->insert('dictionary', array(
            'key' => 'our_shops',
            'uzb' => 'Bizning do\'konlarimiz',
            'rus' => 'Наши магазины',
            'eng' => 'Our shops',
        ));

        $this->insert('dictionary', array(
            'key' => 'service',
            'uzb' => 'Xizmatlar',
            'rus' => 'Услуга',
            'eng' => 'Service',
        ));

        $this->insert('dictionary', array(
            'key' => 'about_company',
            'uzb' => 'Kompaniya haqida',
            'rus' => 'О компании',
            'eng' => 'About Company',
        ));

        $this->insert('dictionary', array(
            'key' => 'news',
            'uzb' => 'Xabarlar',
            'rus' => 'Новости',
            'eng' => 'News',
        ));

        $this->insert('dictionary', array(
            'key' => 'promotions',
            'uzb' => 'Aksiyalar',
            'rus' => 'Акции',
            'eng' => 'Promotions',
        ));

        $this->insert('dictionary', array(
            'key' => 'contacts',
            'uzb' => 'Kontakt',
            'rus' => 'Контакты',
            'eng' => 'Contacts',
        ));

        $this->insert('dictionary', array(
            'key' => 'feedback',
            'uzb' => 'Qayta aloqa',
            'rus' => 'Обратная связь',
            'eng' => 'Feedback',
        ));

        $this->insert('dictionary', array(
            'key' => 'our center',
            'uzb' => 'Bizning markaz',
            'rus' => 'Наш центр',
            'eng' => 'Our center',
        ));

        $this->insert('dictionary', array(
            'key' => 'reviews about company',
            'uzb' => 'Sharhlar <br> Kompaniya haqida',
            'rus' => 'Отзывы <br> о компании',
            'eng' => 'Reviews <br> about company',
        ));

        $this->insert('dictionary', array(
            'key' => 'read_more',
            'uzb' => 'Davomi',
            'rus' => 'Подробнее',
            'eng' => 'Read more',
        ));

        $this->insert('dictionary', array(
            'key' => 'main',
            'uzb' => 'Asosiy',
            'rus' => 'Главная',
            'eng' => 'Main',
        ));

        $this->insert('dictionary', array(
            'key' => 'fio',
            'uzb' => 'To\'liq ism yoki kompaniya nomi',
            'rus' => 'Ф.И.О. или Название компании',
            'eng' => 'Full name or company name',
        ));

        $this->insert('dictionary', array(
            'key' => 'message',
            'uzb' => 'Xabar',
            'rus' => 'Сообщение',
            'eng' => 'Message',
        ));

        $this->insert('dictionary', array(
            'key' => 'send',
            'uzb' => 'Jo\'natish',
            'rus' => 'Отравить',
            'eng' => 'Send',
        ));

        $this->insert('dictionary', array(
            'key' => 'clock',
            'uzb' => '10:00 dan 23:00 gacha',
            'rus' => 'c 10:00 до 23:00',
            'eng' => '10:00 to 23:00',
        ));

        $this->insert('dictionary', array(
            'key' => 'working_time',
            'uzb' => 'Ish vaqti',
            'rus' => 'Часы работы',
            'eng' => 'Opening hours',
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('dictionary');
    }
}
