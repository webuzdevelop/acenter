<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sliders}}`.
 */
class m190704_123845_create_sliders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('sliders', [
            'id' => $this->primaryKey(),
            'type' => $this->string(50)->comment('Тип')->defaultValue('main'),
            'title_uz' => $this->string(255)->comment('Sarlavha'),
            'title_ru' => $this->string(255)->comment('Заголовок'),
            'title_en' => $this->string(255)->comment('Title'),
            'text_uz' => $this->text()->comment('Matn'),
            'text_ru' => $this->text()->comment('Текст'),
            'text_en' => $this->text()->comment('Text'),
            'link' => $this->string(255)->comment('Ссылка'),
            'photo' => $this->string(255)->comment('Фото'),
        ]);

        $this->insert('sliders', array(
            'title_uz' => 'Qulay shartlar Sizning biznesingiz uchun',
            'title_ru' => 'Комфортные условия для вашего бизнеса',
            'title_en' => 'Comfortable conditions for your business',
            'text_uz' => 'Sizni tanlagan A-Markaziga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'text_ru' => 'Мы рады приветствовать вас в офисе A-Center по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'text_en' => 'We welcome you to the A-Center office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'link' => 'http://acenter.deepx.uz',
            'photo' => 'slider_img1.png'
        ));

        $this->insert('sliders', array(
            'title_uz' => 'Qulay shartlar Sizning biznesingiz uchun',
            'title_ru' => 'Комфортные условия для вашего бизнеса',
            'title_en' => 'Comfortable conditions for your business',
            'text_uz' => 'Sizni tanlagan A-Markaziga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'text_ru' => 'Мы рады приветствовать вас в офисе A-Center по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'text_en' => 'We welcome you to the A-Center office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'link' => 'http://acenter.deepx.uz',
            'photo' => 'slider_img2.png'
        ));

        $this->insert('sliders', array(
            'title_uz' => 'Qulay shartlar Sizning biznesingiz uchun',
            'title_ru' => 'Комфортные условия для вашего бизнеса',
            'title_en' => 'Comfortable conditions for your business',
            'text_uz' => 'Sizni tanlagan A-Markaziga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'text_ru' => 'Мы рады приветствовать вас в офисе A-Center по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'text_en' => 'We welcome you to the A-Center office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'link' => 'http://acenter.deepx.uz',
            'photo' => 'slider_img3.png'
        ));

        $this->insert('sliders', array(
            'title_uz' => 'Qulay shartlar Sizning biznesingiz uchun',
            'title_ru' => 'Комфортные условия для вашего бизнеса',
            'title_en' => 'Comfortable conditions for your business',
            'text_uz' => 'Sizni tanlagan A-Markaziga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'text_ru' => 'Мы рады приветствовать вас в офисе A-Center по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'text_en' => 'We welcome you to the A-Center office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'link' => 'http://acenter.deepx.uz',
            'photo' => 'slider_img4.png'
        ));

        $this->insert('sliders', array(
            'type' => 'about',
            'title_uz' => 'Qulay shartlar Sizning biznesingiz uchun',
            'title_ru' => 'Комфортные условия для вашего бизнеса',
            'title_en' => 'Comfortable conditions for your business',
            'text_uz' => 'Sizni tanlagan A-Markaziga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'text_ru' => 'Мы рады приветствовать вас в офисе A-Center по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'text_en' => 'We welcome you to the A-Center office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'link' => 'https://momento360.com/e/u/a9b53aa8f8b0403ba7a4e18243aabc66',
            'photo' => 'about-slider-img1.png'
        ));

        $this->insert('sliders', array(
            'type' => 'about',
            'title_uz' => 'Qulay shartlar Sizning biznesingiz uchun',
            'title_ru' => 'Комфортные условия для вашего бизнеса',
            'title_en' => 'Comfortable conditions for your business',
            'text_uz' => 'Sizni tanlagan A-Markaziga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'text_ru' => 'Мы рады приветствовать вас в офисе A-Center по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'text_en' => 'We welcome you to the A-Center office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'link' => 'https://momento360.com/e/u/a9b53aa8f8b0403ba7a4e18243aabc66',
            'photo' => 'about-slider-img2.png'
        ));

        $this->insert('sliders', array(
            'type' => 'about',
            'title_uz' => 'Qulay shartlar Sizning biznesingiz uchun',
            'title_ru' => 'Комфортные условия для вашего бизнеса',
            'title_en' => 'Comfortable conditions for your business',
            'text_uz' => 'Sizni tanlagan A-Markaziga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'text_ru' => 'Мы рады приветствовать вас в офисе A-Center по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'text_en' => 'We welcome you to the A-Center office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'link' => 'https://momento360.com/e/u/a9b53aa8f8b0403ba7a4e18243aabc66',
            'photo' => 'about-slider-img3.png'
        ));

        $this->insert('sliders', array(
            'type' => 'navigation',
            'title_uz' => 'Qulay shartlar Sizning biznesingiz uchun',
            'title_ru' => 'Комфортные условия для вашего бизнеса',
            'title_en' => 'Comfortable conditions for your business',
            'text_uz' => 'Sizni tanlagan A-Markaziga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'text_ru' => 'Мы рады приветствовать вас в офисе A-Center по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'text_en' => 'We welcome you to the A-Center office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'link' => 'http://acenter.deepx.uz',
            'photo' => 'navigation-slider-img1.png'
        ));

        $this->insert('sliders', array(
            'type' => 'navigation',
            'title_uz' => 'Qulay shartlar Sizning biznesingiz uchun',
            'title_ru' => 'Комфортные условия для вашего бизнеса',
            'title_en' => 'Comfortable conditions for your business',
            'text_uz' => 'Sizni tanlagan A-Markaziga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'text_ru' => 'Мы рады приветствовать вас в офисе A-Center по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'text_en' => 'We welcome you to the A-Center office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'link' => 'http://acenter.deepx.uz',
            'photo' => 'navigation-slider-img2.png'
        ));

        $this->insert('sliders', array(
            'type' => 'navigation',
            'title_uz' => 'Qulay shartlar Sizning biznesingiz uchun',
            'title_ru' => 'Комфортные условия для вашего бизнеса',
            'title_en' => 'Comfortable conditions for your business',
            'text_uz' => 'Sizni tanlagan A-Markaziga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'text_ru' => 'Мы рады приветствовать вас в офисе A-Center по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'text_en' => 'We welcome you to the A-Center office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'link' => 'http://acenter.deepx.uz',
            'photo' => 'navigation-slider-img3.png'
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('sliders');
    }
}
