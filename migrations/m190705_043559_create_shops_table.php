<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%shops}}`.
 */
class m190705_043559_create_shops_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('shops', [
            'id' => $this->primaryKey(),
            'logo' => $this->string(255)->comment('Логотип'),
            'name' => $this->string(255)->comment('Наименование'),
            'title_uz' => $this->string(500)->comment('Sarlavha'),
            'title_ru' => $this->string(500)->comment('Заголовок'),
            'title_en' => $this->string(500)->comment('Title'),
            'text_uz' => $this->text()->comment('Matn'),
            'text_ru' => $this->text()->comment('Текст'),
            'text_en' => $this->text()->comment('Text'),
            'photo1' => $this->string(255),
            'photo2' => $this->string(255),
            'text_top_uz' => $this->text(),
            'text_top_ru' => $this->text(),
            'text_top_en' => $this->text(),
            'text_bottom_uz' => $this->text(),
            'text_bottom_ru' => $this->text(),
            'text_bottom_en' => $this->text(),
        ]);

        $this->insert('shops', array(
            'logo' => 'shops_img1.png',
            'name' => 'Logo',
            'title_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'title_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'title_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'text_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'text_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'text_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'photo1' => 'resident_top1.png',
            'photo2' => 'resident_bootom1.png',
            'text_top_uz' => '<div class="resident-content-block-2-title">
            A-Center — sifati <br> va chidamlilik.
       </div> <div class="resident-content-block-2-text">
            <p>LIMAX xalqaro xolding kompaniyasi yuqori sifatli paypoq va paypoq mahsulotlarini ishlab chiqaruvchi kompaniya hisoblanadi. LIMAX 17 yildan buyon yangi mahsulotlarni tadqiq etish va ishlab chiqarish bilan shug\'ullanadi. LIMAX mahsulotlari keng va sifatli assortimentlari uchun tan olinadi, mahsulot ishlab chiqarishdagi eng yangi texnologiyalardan foydalangan holda ekologik xom ashyolardan tayyorlanadi. Biz erkaklar, ayollar, bolalar paypoqlari, choyshablar, taytlar, erkaklar, ayollar ichki kiyimlari va kiyimlari keng doirasini taklif etamiz.</p>
       </div>',
            'text_top_ru' => '<div class="resident-content-block-2-title">
            A-Center — качество <br> и долговечность.
       </div> <div class="resident-content-block-2-text">
            <p>Международная холдинговая компания LIMAX является производителем носочно-чулочной продукции высокого качества. LIMAX на протяжении 17 лет занимается научными исследованиями и разработкой новых видов продукции. Изделия LIMAX является узнаваемыми своим широким и качественным ассортиментом, продукция изготавливается из экологически чистого сырья с Использованием новейших технологий при производстве. Мы предлагаем широкий выбор мужских, женских, детских носков, чулков, колготок, мужского, женского нижнего белья и одежды.</p>
       </div>',
            'text_top_en' => '<div class="resident-content-block-2-title">
            A-Center — quality <br> and durability.
       </div> <div class="resident-content-block-2-text">
            <p>The international holding company LIMAX is a manufacturer of high quality hosiery and hosiery products. LIMAX has been engaged in research and development of new products for 17 years. LIMAX products are recognizable for their wide and high-quality assortment, products are made from environmentally friendly raw materials using the latest technologies in production. We offer a wide selection of men\'s, women\'s, children\'s socks, stockings, tights, men\'s, women\'s underwear and clothing.</p>
       </div>',
            'text_bottom_uz' => '<div class="resident-content-block-3-title">
            Mehmonlar
       </div> <div class="resident-content-block-3-text">
            <p>LIMAX xalqaro xolding kompaniyasi yuqori sifatli paypoq va paypoq mahsulotlarini ishlab chiqaruvchi kompaniya hisoblanadi. LIMAX 17 yildan buyon yangi mahsulotlarni tadqiq etish va ishlab chiqarish bilan shug\'ullanadi. LIMAX mahsulotlari keng va sifatli assortimentlari uchun tan olinadi, mahsulot ishlab chiqarishdagi eng yangi texnologiyalardan foydalangan holda ekologik xom ashyolardan tayyorlanadi. Biz erkaklar, ayollar, bolalar paypoqlari, choyshablar, taytlar, erkaklar, ayollar ichki kiyimlari va kiyimlari keng doirasini taklif etamiz.</p>
       </div>',
            'text_bottom_ru' => '<div class="resident-content-block-3-title">
            Посетителем
       </div> <div class="resident-content-block-3-text">
            <p>Международная холдинговая компания LIMAX является производителем носочно-чулочной продукции высокого качества. LIMAX на протяжении 17 лет занимается научными исследованиями и разработкой новых видов продукции. Изделия LIMAX является узнаваемыми своим широким и качественным ассортиментом, продукция изготавливается из экологически чистого сырья с Использованием новейших технологий при производстве. Мы предлагаем широкий выбор мужских, женских, детских носков, чулков, колготок, мужского, женского нижнего белья и одежды.</p>
       </div>\'',
            'text_bottom_en' => '<div class="resident-content-block-3-title">
            The visitor
       </div> <div class="resident-content-block-3-text">
            <p>The international holding company LIMAX is a manufacturer of high quality hosiery and hosiery products. LIMAX has been engaged in research and development of new products for 17 years. LIMAX products are recognizable for their wide and high-quality assortment, products are made from environmentally friendly raw materials using the latest technologies in production. We offer a wide selection of men\'s, women\'s, children\'s socks, stockings, tights, men\'s, women\'s underwear and clothing.</p>
       </div>\''
        ));

        $this->insert('shops', array(
            'logo' => 'shops_img2.png',
            'name' => 'Amazon.in',
            'title_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'title_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'title_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'text_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'text_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'text_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'photo1' => 'resident_top2.png',
            'photo2' => 'resident_bootom2.png',
            'text_top_uz' => '<div class="resident-content-block-2-title">
            A-Center — sifati <br> va chidamlilik.
       </div> <div class="resident-content-block-2-text">
            <p>LIMAX xalqaro xolding kompaniyasi yuqori sifatli paypoq va paypoq mahsulotlarini ishlab chiqaruvchi kompaniya hisoblanadi. LIMAX 17 yildan buyon yangi mahsulotlarni tadqiq etish va ishlab chiqarish bilan shug\'ullanadi. LIMAX mahsulotlari keng va sifatli assortimentlari uchun tan olinadi, mahsulot ishlab chiqarishdagi eng yangi texnologiyalardan foydalangan holda ekologik xom ashyolardan tayyorlanadi. Biz erkaklar, ayollar, bolalar paypoqlari, choyshablar, taytlar, erkaklar, ayollar ichki kiyimlari va kiyimlari keng doirasini taklif etamiz.</p>
       </div>',
            'text_top_ru' => '<div class="resident-content-block-2-title">
            A-Center — качество <br> и долговечность.
       </div> <div class="resident-content-block-2-text">
            <p>Международная холдинговая компания LIMAX является производителем носочно-чулочной продукции высокого качества. LIMAX на протяжении 17 лет занимается научными исследованиями и разработкой новых видов продукции. Изделия LIMAX является узнаваемыми своим широким и качественным ассортиментом, продукция изготавливается из экологически чистого сырья с Использованием новейших технологий при производстве. Мы предлагаем широкий выбор мужских, женских, детских носков, чулков, колготок, мужского, женского нижнего белья и одежды.</p>
       </div>',
            'text_top_en' => '<div class="resident-content-block-2-title">
            A-Center — quality <br> and durability.
       </div> <div class="resident-content-block-2-text">
            <p>The international holding company LIMAX is a manufacturer of high quality hosiery and hosiery products. LIMAX has been engaged in research and development of new products for 17 years. LIMAX products are recognizable for their wide and high-quality assortment, products are made from environmentally friendly raw materials using the latest technologies in production. We offer a wide selection of men\'s, women\'s, children\'s socks, stockings, tights, men\'s, women\'s underwear and clothing.</p>
       </div>',
            'text_bottom_uz' => '<div class="resident-content-block-3-title">
            Mehmonlar
       </div> <div class="resident-content-block-3-text">
            <p>LIMAX xalqaro xolding kompaniyasi yuqori sifatli paypoq va paypoq mahsulotlarini ishlab chiqaruvchi kompaniya hisoblanadi. LIMAX 17 yildan buyon yangi mahsulotlarni tadqiq etish va ishlab chiqarish bilan shug\'ullanadi. LIMAX mahsulotlari keng va sifatli assortimentlari uchun tan olinadi, mahsulot ishlab chiqarishdagi eng yangi texnologiyalardan foydalangan holda ekologik xom ashyolardan tayyorlanadi. Biz erkaklar, ayollar, bolalar paypoqlari, choyshablar, taytlar, erkaklar, ayollar ichki kiyimlari va kiyimlari keng doirasini taklif etamiz.</p>
       </div>',
            'text_bottom_ru' => '<div class="resident-content-block-3-title">
            Посетителем
       </div> <div class="resident-content-block-3-text">
            <p>Международная холдинговая компания LIMAX является производителем носочно-чулочной продукции высокого качества. LIMAX на протяжении 17 лет занимается научными исследованиями и разработкой новых видов продукции. Изделия LIMAX является узнаваемыми своим широким и качественным ассортиментом, продукция изготавливается из экологически чистого сырья с Использованием новейших технологий при производстве. Мы предлагаем широкий выбор мужских, женских, детских носков, чулков, колготок, мужского, женского нижнего белья и одежды.</p>
       </div>\'',
            'text_bottom_en' => '<div class="resident-content-block-3-title">
            The visitor
       </div> <div class="resident-content-block-3-text">
            <p>The international holding company LIMAX is a manufacturer of high quality hosiery and hosiery products. LIMAX has been engaged in research and development of new products for 17 years. LIMAX products are recognizable for their wide and high-quality assortment, products are made from environmentally friendly raw materials using the latest technologies in production. We offer a wide selection of men\'s, women\'s, children\'s socks, stockings, tights, men\'s, women\'s underwear and clothing.</p>
       </div>\''
        ));

        $this->insert('shops', array(
            'logo' => 'shops_img3.png',
            'name' => 'Unity',
            'title_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'title_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'title_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'text_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'text_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'text_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'photo1' => 'resident_top3.png',
            'photo2' => 'resident_bootom3.png',
            'text_top_uz' => '<div class="resident-content-block-2-title">
            A-Center — sifati <br> va chidamlilik.
       </div> <div class="resident-content-block-2-text">
            <p>LIMAX xalqaro xolding kompaniyasi yuqori sifatli paypoq va paypoq mahsulotlarini ishlab chiqaruvchi kompaniya hisoblanadi. LIMAX 17 yildan buyon yangi mahsulotlarni tadqiq etish va ishlab chiqarish bilan shug\'ullanadi. LIMAX mahsulotlari keng va sifatli assortimentlari uchun tan olinadi, mahsulot ishlab chiqarishdagi eng yangi texnologiyalardan foydalangan holda ekologik xom ashyolardan tayyorlanadi. Biz erkaklar, ayollar, bolalar paypoqlari, choyshablar, taytlar, erkaklar, ayollar ichki kiyimlari va kiyimlari keng doirasini taklif etamiz.</p>
       </div>',
            'text_top_ru' => '<div class="resident-content-block-2-title">
            A-Center — качество <br> и долговечность.
       </div> <div class="resident-content-block-2-text">
            <p>Международная холдинговая компания LIMAX является производителем носочно-чулочной продукции высокого качества. LIMAX на протяжении 17 лет занимается научными исследованиями и разработкой новых видов продукции. Изделия LIMAX является узнаваемыми своим широким и качественным ассортиментом, продукция изготавливается из экологически чистого сырья с Использованием новейших технологий при производстве. Мы предлагаем широкий выбор мужских, женских, детских носков, чулков, колготок, мужского, женского нижнего белья и одежды.</p>
       </div>',
            'text_top_en' => '<div class="resident-content-block-2-title">
            A-Center — quality <br> and durability.
       </div> <div class="resident-content-block-2-text">
            <p>The international holding company LIMAX is a manufacturer of high quality hosiery and hosiery products. LIMAX has been engaged in research and development of new products for 17 years. LIMAX products are recognizable for their wide and high-quality assortment, products are made from environmentally friendly raw materials using the latest technologies in production. We offer a wide selection of men\'s, women\'s, children\'s socks, stockings, tights, men\'s, women\'s underwear and clothing.</p>
       </div>',
            'text_bottom_uz' => '<div class="resident-content-block-3-title">
            Mehmonlar
       </div> <div class="resident-content-block-3-text">
            <p>LIMAX xalqaro xolding kompaniyasi yuqori sifatli paypoq va paypoq mahsulotlarini ishlab chiqaruvchi kompaniya hisoblanadi. LIMAX 17 yildan buyon yangi mahsulotlarni tadqiq etish va ishlab chiqarish bilan shug\'ullanadi. LIMAX mahsulotlari keng va sifatli assortimentlari uchun tan olinadi, mahsulot ishlab chiqarishdagi eng yangi texnologiyalardan foydalangan holda ekologik xom ashyolardan tayyorlanadi. Biz erkaklar, ayollar, bolalar paypoqlari, choyshablar, taytlar, erkaklar, ayollar ichki kiyimlari va kiyimlari keng doirasini taklif etamiz.</p>
       </div>',
            'text_bottom_ru' => '<div class="resident-content-block-3-title">
            Посетителем
       </div> <div class="resident-content-block-3-text">
            <p>Международная холдинговая компания LIMAX является производителем носочно-чулочной продукции высокого качества. LIMAX на протяжении 17 лет занимается научными исследованиями и разработкой новых видов продукции. Изделия LIMAX является узнаваемыми своим широким и качественным ассортиментом, продукция изготавливается из экологически чистого сырья с Использованием новейших технологий при производстве. Мы предлагаем широкий выбор мужских, женских, детских носков, чулков, колготок, мужского, женского нижнего белья и одежды.</p>
       </div>\'',
            'text_bottom_en' => '<div class="resident-content-block-3-title">
            The visitor
       </div> <div class="resident-content-block-3-text">
            <p>The international holding company LIMAX is a manufacturer of high quality hosiery and hosiery products. LIMAX has been engaged in research and development of new products for 17 years. LIMAX products are recognizable for their wide and high-quality assortment, products are made from environmentally friendly raw materials using the latest technologies in production. We offer a wide selection of men\'s, women\'s, children\'s socks, stockings, tights, men\'s, women\'s underwear and clothing.</p>
       </div>\''
        ));

        $this->insert('shops', array(
            'logo' => 'shops_img4.png',
            'name' => 'Vistaprint',
            'title_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'title_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'title_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'text_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'text_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'text_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'photo1' => 'resident_top4.png',
            'photo2' => 'resident_bootom4.png',
            'text_top_uz' => '<div class="resident-content-block-2-title">
            A-Center — sifati <br> va chidamlilik.
       </div> <div class="resident-content-block-2-text">
            <p>LIMAX xalqaro xolding kompaniyasi yuqori sifatli paypoq va paypoq mahsulotlarini ishlab chiqaruvchi kompaniya hisoblanadi. LIMAX 17 yildan buyon yangi mahsulotlarni tadqiq etish va ishlab chiqarish bilan shug\'ullanadi. LIMAX mahsulotlari keng va sifatli assortimentlari uchun tan olinadi, mahsulot ishlab chiqarishdagi eng yangi texnologiyalardan foydalangan holda ekologik xom ashyolardan tayyorlanadi. Biz erkaklar, ayollar, bolalar paypoqlari, choyshablar, taytlar, erkaklar, ayollar ichki kiyimlari va kiyimlari keng doirasini taklif etamiz.</p>
       </div>',
            'text_top_ru' => '<div class="resident-content-block-2-title">
            A-Center — качество <br> и долговечность.
       </div> <div class="resident-content-block-2-text">
            <p>Международная холдинговая компания LIMAX является производителем носочно-чулочной продукции высокого качества. LIMAX на протяжении 17 лет занимается научными исследованиями и разработкой новых видов продукции. Изделия LIMAX является узнаваемыми своим широким и качественным ассортиментом, продукция изготавливается из экологически чистого сырья с Использованием новейших технологий при производстве. Мы предлагаем широкий выбор мужских, женских, детских носков, чулков, колготок, мужского, женского нижнего белья и одежды.</p>
       </div>',
            'text_top_en' => '<div class="resident-content-block-2-title">
            A-Center — quality <br> and durability.
       </div> <div class="resident-content-block-2-text">
            <p>The international holding company LIMAX is a manufacturer of high quality hosiery and hosiery products. LIMAX has been engaged in research and development of new products for 17 years. LIMAX products are recognizable for their wide and high-quality assortment, products are made from environmentally friendly raw materials using the latest technologies in production. We offer a wide selection of men\'s, women\'s, children\'s socks, stockings, tights, men\'s, women\'s underwear and clothing.</p>
       </div>',
            'text_bottom_uz' => '<div class="resident-content-block-3-title">
            Mehmonlar
       </div> <div class="resident-content-block-3-text">
            <p>LIMAX xalqaro xolding kompaniyasi yuqori sifatli paypoq va paypoq mahsulotlarini ishlab chiqaruvchi kompaniya hisoblanadi. LIMAX 17 yildan buyon yangi mahsulotlarni tadqiq etish va ishlab chiqarish bilan shug\'ullanadi. LIMAX mahsulotlari keng va sifatli assortimentlari uchun tan olinadi, mahsulot ishlab chiqarishdagi eng yangi texnologiyalardan foydalangan holda ekologik xom ashyolardan tayyorlanadi. Biz erkaklar, ayollar, bolalar paypoqlari, choyshablar, taytlar, erkaklar, ayollar ichki kiyimlari va kiyimlari keng doirasini taklif etamiz.</p>
       </div>',
            'text_bottom_ru' => '<div class="resident-content-block-3-title">
            Посетителем
       </div> <div class="resident-content-block-3-text">
            <p>Международная холдинговая компания LIMAX является производителем носочно-чулочной продукции высокого качества. LIMAX на протяжении 17 лет занимается научными исследованиями и разработкой новых видов продукции. Изделия LIMAX является узнаваемыми своим широким и качественным ассортиментом, продукция изготавливается из экологически чистого сырья с Использованием новейших технологий при производстве. Мы предлагаем широкий выбор мужских, женских, детских носков, чулков, колготок, мужского, женского нижнего белья и одежды.</p>
       </div>\'',
            'text_bottom_en' => '<div class="resident-content-block-3-title">
            The visitor
       </div> <div class="resident-content-block-3-text">
            <p>The international holding company LIMAX is a manufacturer of high quality hosiery and hosiery products. LIMAX has been engaged in research and development of new products for 17 years. LIMAX products are recognizable for their wide and high-quality assortment, products are made from environmentally friendly raw materials using the latest technologies in production. We offer a wide selection of men\'s, women\'s, children\'s socks, stockings, tights, men\'s, women\'s underwear and clothing.</p>
       </div>\''
        ));

        $this->insert('shops', array(
            'logo' => 'shops_img5.png',
            'name' => 'Ikea',
            'title_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'title_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'title_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'text_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'text_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'text_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'photo1' => 'resident_top5.png',
            'photo2' => 'resident_bootom5.png',
            'text_top_uz' => '<div class="resident-content-block-2-title">
            A-Center — sifati <br> va chidamlilik.
       </div> <div class="resident-content-block-2-text">
            <p>LIMAX xalqaro xolding kompaniyasi yuqori sifatli paypoq va paypoq mahsulotlarini ishlab chiqaruvchi kompaniya hisoblanadi. LIMAX 17 yildan buyon yangi mahsulotlarni tadqiq etish va ishlab chiqarish bilan shug\'ullanadi. LIMAX mahsulotlari keng va sifatli assortimentlari uchun tan olinadi, mahsulot ishlab chiqarishdagi eng yangi texnologiyalardan foydalangan holda ekologik xom ashyolardan tayyorlanadi. Biz erkaklar, ayollar, bolalar paypoqlari, choyshablar, taytlar, erkaklar, ayollar ichki kiyimlari va kiyimlari keng doirasini taklif etamiz.</p>
       </div>',
            'text_top_ru' => '<div class="resident-content-block-2-title">
            A-Center — качество <br> и долговечность.
       </div> <div class="resident-content-block-2-text">
            <p>Международная холдинговая компания LIMAX является производителем носочно-чулочной продукции высокого качества. LIMAX на протяжении 17 лет занимается научными исследованиями и разработкой новых видов продукции. Изделия LIMAX является узнаваемыми своим широким и качественным ассортиментом, продукция изготавливается из экологически чистого сырья с Использованием новейших технологий при производстве. Мы предлагаем широкий выбор мужских, женских, детских носков, чулков, колготок, мужского, женского нижнего белья и одежды.</p>
       </div>',
            'text_top_en' => '<div class="resident-content-block-2-title">
            A-Center — quality <br> and durability.
       </div> <div class="resident-content-block-2-text">
            <p>The international holding company LIMAX is a manufacturer of high quality hosiery and hosiery products. LIMAX has been engaged in research and development of new products for 17 years. LIMAX products are recognizable for their wide and high-quality assortment, products are made from environmentally friendly raw materials using the latest technologies in production. We offer a wide selection of men\'s, women\'s, children\'s socks, stockings, tights, men\'s, women\'s underwear and clothing.</p>
       </div>',
            'text_bottom_uz' => '<div class="resident-content-block-3-title">
            Mehmonlar
       </div> <div class="resident-content-block-3-text">
            <p>LIMAX xalqaro xolding kompaniyasi yuqori sifatli paypoq va paypoq mahsulotlarini ishlab chiqaruvchi kompaniya hisoblanadi. LIMAX 17 yildan buyon yangi mahsulotlarni tadqiq etish va ishlab chiqarish bilan shug\'ullanadi. LIMAX mahsulotlari keng va sifatli assortimentlari uchun tan olinadi, mahsulot ishlab chiqarishdagi eng yangi texnologiyalardan foydalangan holda ekologik xom ashyolardan tayyorlanadi. Biz erkaklar, ayollar, bolalar paypoqlari, choyshablar, taytlar, erkaklar, ayollar ichki kiyimlari va kiyimlari keng doirasini taklif etamiz.</p>
       </div>',
            'text_bottom_ru' => '<div class="resident-content-block-3-title">
            Посетителем
       </div> <div class="resident-content-block-3-text">
            <p>Международная холдинговая компания LIMAX является производителем носочно-чулочной продукции высокого качества. LIMAX на протяжении 17 лет занимается научными исследованиями и разработкой новых видов продукции. Изделия LIMAX является узнаваемыми своим широким и качественным ассортиментом, продукция изготавливается из экологически чистого сырья с Использованием новейших технологий при производстве. Мы предлагаем широкий выбор мужских, женских, детских носков, чулков, колготок, мужского, женского нижнего белья и одежды.</p>
       </div>\'',
            'text_bottom_en' => '<div class="resident-content-block-3-title">
            The visitor
       </div> <div class="resident-content-block-3-text">
            <p>The international holding company LIMAX is a manufacturer of high quality hosiery and hosiery products. LIMAX has been engaged in research and development of new products for 17 years. LIMAX products are recognizable for their wide and high-quality assortment, products are made from environmentally friendly raw materials using the latest technologies in production. We offer a wide selection of men\'s, women\'s, children\'s socks, stockings, tights, men\'s, women\'s underwear and clothing.</p>
       </div>\''
        ));

        $this->insert('shops', array(
            'logo' => 'shops_img6.png',
            'name' => 'Linked.in',
            'title_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'title_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'title_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'text_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'text_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'text_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'photo1' => 'resident_top6.png',
            'photo2' => 'resident_bootom6.png',
            'text_top_uz' => '<div class="resident-content-block-2-title">
            A-Center — sifati <br> va chidamlilik.
       </div> <div class="resident-content-block-2-text">
            <p>LIMAX xalqaro xolding kompaniyasi yuqori sifatli paypoq va paypoq mahsulotlarini ishlab chiqaruvchi kompaniya hisoblanadi. LIMAX 17 yildan buyon yangi mahsulotlarni tadqiq etish va ishlab chiqarish bilan shug\'ullanadi. LIMAX mahsulotlari keng va sifatli assortimentlari uchun tan olinadi, mahsulot ishlab chiqarishdagi eng yangi texnologiyalardan foydalangan holda ekologik xom ashyolardan tayyorlanadi. Biz erkaklar, ayollar, bolalar paypoqlari, choyshablar, taytlar, erkaklar, ayollar ichki kiyimlari va kiyimlari keng doirasini taklif etamiz.</p>
       </div>',
            'text_top_ru' => '<div class="resident-content-block-2-title">
            A-Center — качество <br> и долговечность.
       </div> <div class="resident-content-block-2-text">
            <p>Международная холдинговая компания LIMAX является производителем носочно-чулочной продукции высокого качества. LIMAX на протяжении 17 лет занимается научными исследованиями и разработкой новых видов продукции. Изделия LIMAX является узнаваемыми своим широким и качественным ассортиментом, продукция изготавливается из экологически чистого сырья с Использованием новейших технологий при производстве. Мы предлагаем широкий выбор мужских, женских, детских носков, чулков, колготок, мужского, женского нижнего белья и одежды.</p>
       </div>',
            'text_top_en' => '<div class="resident-content-block-2-title">
            A-Center — quality <br> and durability.
       </div> <div class="resident-content-block-2-text">
            <p>The international holding company LIMAX is a manufacturer of high quality hosiery and hosiery products. LIMAX has been engaged in research and development of new products for 17 years. LIMAX products are recognizable for their wide and high-quality assortment, products are made from environmentally friendly raw materials using the latest technologies in production. We offer a wide selection of men\'s, women\'s, children\'s socks, stockings, tights, men\'s, women\'s underwear and clothing.</p>
       </div>',
            'text_bottom_uz' => '<div class="resident-content-block-3-title">
            Mehmonlar
       </div> <div class="resident-content-block-3-text">
            <p>LIMAX xalqaro xolding kompaniyasi yuqori sifatli paypoq va paypoq mahsulotlarini ishlab chiqaruvchi kompaniya hisoblanadi. LIMAX 17 yildan buyon yangi mahsulotlarni tadqiq etish va ishlab chiqarish bilan shug\'ullanadi. LIMAX mahsulotlari keng va sifatli assortimentlari uchun tan olinadi, mahsulot ishlab chiqarishdagi eng yangi texnologiyalardan foydalangan holda ekologik xom ashyolardan tayyorlanadi. Biz erkaklar, ayollar, bolalar paypoqlari, choyshablar, taytlar, erkaklar, ayollar ichki kiyimlari va kiyimlari keng doirasini taklif etamiz.</p>
       </div>',
            'text_bottom_ru' => '<div class="resident-content-block-3-title">
            Посетителем
       </div> <div class="resident-content-block-3-text">
            <p>Международная холдинговая компания LIMAX является производителем носочно-чулочной продукции высокого качества. LIMAX на протяжении 17 лет занимается научными исследованиями и разработкой новых видов продукции. Изделия LIMAX является узнаваемыми своим широким и качественным ассортиментом, продукция изготавливается из экологически чистого сырья с Использованием новейших технологий при производстве. Мы предлагаем широкий выбор мужских, женских, детских носков, чулков, колготок, мужского, женского нижнего белья и одежды.</p>
       </div>\'',
            'text_bottom_en' => '<div class="resident-content-block-3-title">
            The visitor
       </div> <div class="resident-content-block-3-text">
            <p>The international holding company LIMAX is a manufacturer of high quality hosiery and hosiery products. LIMAX has been engaged in research and development of new products for 17 years. LIMAX products are recognizable for their wide and high-quality assortment, products are made from environmentally friendly raw materials using the latest technologies in production. We offer a wide selection of men\'s, women\'s, children\'s socks, stockings, tights, men\'s, women\'s underwear and clothing.</p>
       </div>\''
        ));

        $this->insert('shops', array(
            'logo' => 'shops_img7.png',
            'name' => 'EtuLite',
            'title_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'title_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'title_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'text_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'text_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'text_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'photo1' => 'resident_top7.png',
            'photo2' => 'resident_bootom7.png',
            'text_top_uz' => '<div class="resident-content-block-2-title">
            A-Center — sifati <br> va chidamlilik.
       </div> <div class="resident-content-block-2-text">
            <p>LIMAX xalqaro xolding kompaniyasi yuqori sifatli paypoq va paypoq mahsulotlarini ishlab chiqaruvchi kompaniya hisoblanadi. LIMAX 17 yildan buyon yangi mahsulotlarni tadqiq etish va ishlab chiqarish bilan shug\'ullanadi. LIMAX mahsulotlari keng va sifatli assortimentlari uchun tan olinadi, mahsulot ishlab chiqarishdagi eng yangi texnologiyalardan foydalangan holda ekologik xom ashyolardan tayyorlanadi. Biz erkaklar, ayollar, bolalar paypoqlari, choyshablar, taytlar, erkaklar, ayollar ichki kiyimlari va kiyimlari keng doirasini taklif etamiz.</p>
       </div>',
            'text_top_ru' => '<div class="resident-content-block-2-title">
            A-Center — качество <br> и долговечность.
       </div> <div class="resident-content-block-2-text">
            <p>Международная холдинговая компания LIMAX является производителем носочно-чулочной продукции высокого качества. LIMAX на протяжении 17 лет занимается научными исследованиями и разработкой новых видов продукции. Изделия LIMAX является узнаваемыми своим широким и качественным ассортиментом, продукция изготавливается из экологически чистого сырья с Использованием новейших технологий при производстве. Мы предлагаем широкий выбор мужских, женских, детских носков, чулков, колготок, мужского, женского нижнего белья и одежды.</p>
       </div>',
            'text_top_en' => '<div class="resident-content-block-2-title">
            A-Center — quality <br> and durability.
       </div> <div class="resident-content-block-2-text">
            <p>The international holding company LIMAX is a manufacturer of high quality hosiery and hosiery products. LIMAX has been engaged in research and development of new products for 17 years. LIMAX products are recognizable for their wide and high-quality assortment, products are made from environmentally friendly raw materials using the latest technologies in production. We offer a wide selection of men\'s, women\'s, children\'s socks, stockings, tights, men\'s, women\'s underwear and clothing.</p>
       </div>',
            'text_bottom_uz' => '<div class="resident-content-block-3-title">
            Mehmonlar
       </div> <div class="resident-content-block-3-text">
            <p>LIMAX xalqaro xolding kompaniyasi yuqori sifatli paypoq va paypoq mahsulotlarini ishlab chiqaruvchi kompaniya hisoblanadi. LIMAX 17 yildan buyon yangi mahsulotlarni tadqiq etish va ishlab chiqarish bilan shug\'ullanadi. LIMAX mahsulotlari keng va sifatli assortimentlari uchun tan olinadi, mahsulot ishlab chiqarishdagi eng yangi texnologiyalardan foydalangan holda ekologik xom ashyolardan tayyorlanadi. Biz erkaklar, ayollar, bolalar paypoqlari, choyshablar, taytlar, erkaklar, ayollar ichki kiyimlari va kiyimlari keng doirasini taklif etamiz.</p>
       </div>',
            'text_bottom_ru' => '<div class="resident-content-block-3-title">
            Посетителем
       </div> <div class="resident-content-block-3-text">
            <p>Международная холдинговая компания LIMAX является производителем носочно-чулочной продукции высокого качества. LIMAX на протяжении 17 лет занимается научными исследованиями и разработкой новых видов продукции. Изделия LIMAX является узнаваемыми своим широким и качественным ассортиментом, продукция изготавливается из экологически чистого сырья с Использованием новейших технологий при производстве. Мы предлагаем широкий выбор мужских, женских, детских носков, чулков, колготок, мужского, женского нижнего белья и одежды.</p>
       </div>\'',
            'text_bottom_en' => '<div class="resident-content-block-3-title">
            The visitor
       </div> <div class="resident-content-block-3-text">
            <p>The international holding company LIMAX is a manufacturer of high quality hosiery and hosiery products. LIMAX has been engaged in research and development of new products for 17 years. LIMAX products are recognizable for their wide and high-quality assortment, products are made from environmentally friendly raw materials using the latest technologies in production. We offer a wide selection of men\'s, women\'s, children\'s socks, stockings, tights, men\'s, women\'s underwear and clothing.</p>
       </div>\''
        ));

        $this->insert('shops', array(
            'logo' => 'shops_img8.png',
            'name' => 'Coca-Cola',
            'title_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'title_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'title_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'text_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'text_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'text_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'photo1' => 'resident_top8.png',
            'photo2' => 'resident_bootom8.png',
            'text_top_uz' => '<div class="resident-content-block-2-title">
            A-Center — sifati <br> va chidamlilik.
       </div> <div class="resident-content-block-2-text">
            <p>LIMAX xalqaro xolding kompaniyasi yuqori sifatli paypoq va paypoq mahsulotlarini ishlab chiqaruvchi kompaniya hisoblanadi. LIMAX 17 yildan buyon yangi mahsulotlarni tadqiq etish va ishlab chiqarish bilan shug\'ullanadi. LIMAX mahsulotlari keng va sifatli assortimentlari uchun tan olinadi, mahsulot ishlab chiqarishdagi eng yangi texnologiyalardan foydalangan holda ekologik xom ashyolardan tayyorlanadi. Biz erkaklar, ayollar, bolalar paypoqlari, choyshablar, taytlar, erkaklar, ayollar ichki kiyimlari va kiyimlari keng doirasini taklif etamiz.</p>
       </div>',
            'text_top_ru' => '<div class="resident-content-block-2-title">
            A-Center — качество <br> и долговечность.
       </div> <div class="resident-content-block-2-text">
            <p>Международная холдинговая компания LIMAX является производителем носочно-чулочной продукции высокого качества. LIMAX на протяжении 17 лет занимается научными исследованиями и разработкой новых видов продукции. Изделия LIMAX является узнаваемыми своим широким и качественным ассортиментом, продукция изготавливается из экологически чистого сырья с Использованием новейших технологий при производстве. Мы предлагаем широкий выбор мужских, женских, детских носков, чулков, колготок, мужского, женского нижнего белья и одежды.</p>
       </div>',
            'text_top_en' => '<div class="resident-content-block-2-title">
            A-Center — quality <br> and durability.
       </div> <div class="resident-content-block-2-text">
            <p>The international holding company LIMAX is a manufacturer of high quality hosiery and hosiery products. LIMAX has been engaged in research and development of new products for 17 years. LIMAX products are recognizable for their wide and high-quality assortment, products are made from environmentally friendly raw materials using the latest technologies in production. We offer a wide selection of men\'s, women\'s, children\'s socks, stockings, tights, men\'s, women\'s underwear and clothing.</p>
       </div>',
            'text_bottom_uz' => '<div class="resident-content-block-3-title">
            Mehmonlar
       </div> <div class="resident-content-block-3-text">
            <p>LIMAX xalqaro xolding kompaniyasi yuqori sifatli paypoq va paypoq mahsulotlarini ishlab chiqaruvchi kompaniya hisoblanadi. LIMAX 17 yildan buyon yangi mahsulotlarni tadqiq etish va ishlab chiqarish bilan shug\'ullanadi. LIMAX mahsulotlari keng va sifatli assortimentlari uchun tan olinadi, mahsulot ishlab chiqarishdagi eng yangi texnologiyalardan foydalangan holda ekologik xom ashyolardan tayyorlanadi. Biz erkaklar, ayollar, bolalar paypoqlari, choyshablar, taytlar, erkaklar, ayollar ichki kiyimlari va kiyimlari keng doirasini taklif etamiz.</p>
       </div>',
            'text_bottom_ru' => '<div class="resident-content-block-3-title">
            Посетителем
       </div> <div class="resident-content-block-3-text">
            <p>Международная холдинговая компания LIMAX является производителем носочно-чулочной продукции высокого качества. LIMAX на протяжении 17 лет занимается научными исследованиями и разработкой новых видов продукции. Изделия LIMAX является узнаваемыми своим широким и качественным ассортиментом, продукция изготавливается из экологически чистого сырья с Использованием новейших технологий при производстве. Мы предлагаем широкий выбор мужских, женских, детских носков, чулков, колготок, мужского, женского нижнего белья и одежды.</p>
       </div>\'',
            'text_bottom_en' => '<div class="resident-content-block-3-title">
            The visitor
       </div> <div class="resident-content-block-3-text">
            <p>The international holding company LIMAX is a manufacturer of high quality hosiery and hosiery products. LIMAX has been engaged in research and development of new products for 17 years. LIMAX products are recognizable for their wide and high-quality assortment, products are made from environmentally friendly raw materials using the latest technologies in production. We offer a wide selection of men\'s, women\'s, children\'s socks, stockings, tights, men\'s, women\'s underwear and clothing.</p>
       </div>\''
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('shops');
    }
}
