<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%floors}}`.
 */
class m190706_190959_create_floors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('{{%floors}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
            'photo' => $this->string(255)->comment('Фотограция')
        ]);

        $this->insert('floors', array(
            'name' => 'Этаж:1',
            'photo' => '1.png',
        ));

        $this->insert('floors', array(
            'name' => 'Этаж:2',
            'photo' => '2.png',
        ));

        $this->insert('floors', array(
            'name' => 'Этаж:3',
            'photo' => '3.png',
        ));

        $this->insert('floors', array(
            'name' => 'Этаж:4',
            'photo' => '4.png',
        ));

        $this->insert('floors', array(
            'name' => 'Этаж:5',
            'photo' => '5.png',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('{{%floors}}');
    }
}
