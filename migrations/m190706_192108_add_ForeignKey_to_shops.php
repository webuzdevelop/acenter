<?php

use yii\db\Migration;

/**
 * Class m190706_192108_add_ForeignKey_to_shops
 */
class m190706_192108_add_ForeignKey_to_shops extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addForeignKey('fk-shops-floor_id', 'shops', 'floor_id', 'floors' ,'id');
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropForeignKey('fk-shops-floor_id', 'shops');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190706_192108_add_ForeignKey_to_shops cannot be reverted.\n";

        return false;
    }
    */
}
