<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%news}}`.
 */
class m190704_135738_create_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'type' => $this->integer()->comment('Тип'),
            'fone' => $this->string(255)->comment('Фон'),
            'photo' => $this->string(255)->comment('Фото'),
            'date' => $this->date()->comment('Дата'),
            'title_uz' => $this->string(300)->comment('Заголовок'),
            'title_ru' => $this->string(300)->comment('Sarlavha'),
            'title_en' => $this->string(300)->comment('Title'),
            'text_uz' => $this->text()->comment('Matn'),
            'text_ru' => $this->text()->comment('Текст'),
            'text_en' => $this->text()->comment('Text'),
            'description_uz' => $this->text()->comment('Ma\'lumot'),
            'description_ru' => $this->text()->comment('Описание'),
            'description_en' => $this->text()->comment('Description'),
        ]);

        $this->insert('news', array(
            'type' => 1,
            'fone' => 'news_fon1.png',
            'photo' => 'news_img1.png',
            'date' => date('Y-m-d'),
            'title_uz' => 'Sotuvlar allaqachon kelmoqda!',
            'title_ru' => 'Распродажа уже идет!!!',
            'title_en' => 'Sale is already coming !!!',
            'text_uz' => 'Tutaevskoye magistralida 8-sonli A-Markazga tashrif buyurishingiz va orzularingizni ro\'yobga chiqarish uchun vaqtingiz bor.
Siz nafaqat maksimal foyda, balki sovg\'alarni ham hayratda qoldirasiz! Bu vaqt keldi ...',
            'text_ru' => 'Осталось 4 грандиозных дня, что бы придти на распродажу A-Center на Тутаевском шоссе, 8 и успеть осуществить свою мечту! 
Вы будете приятно удивлены не только максимальным выгодам, но и подаркам! Пришло то вре...',
            'text_en' => 'There are 4 grand days left to come to the A-Center sale on Tutaevskoye Highway, 8 and have time to fulfill your dream!
You will be pleasantly surprised not only the maximum benefits, but also gifts! That time has come ...',
            'description_uz' => 'Xaridor: Xalqaro Hamkorlik Markazi, manfaatdor kompaniyalarni xizmatlarni taqdim etish uchun tanlovli tanlovda ishtirok etishga taklif qiladi

Toshkent-Yunus-Obod tumani, Amir Temur shoh ko\'chasi, 107A uyda joylashgan "Toshkent-Plaza" Savatga markazi binosida ichki bezatish ishlarini olib tashlash.

Tanlovda qatnashish uchun firma shartnoma shartlari mazmuniga ega bo\'lib, unda firma tomonidan yozilgan firma raqami, ro\'yxatdan o\'tgan sanasi va imzosi ko\'rsatilgan holda firma blankida (tashkilotning tafsilotlari bilan) kotirovkalarni taqdim etishlari shart.

Tovar taklifi yopiq konvertlarda yopishtirilgan joylarda, Toshkent shahri, Yunus-Obod tumani, Amir Temur shoh ko\'chasi 107-B manzilida muhrlanadi.

Tanlovda taqdim etilayotgan xizmatlarning raqobatbardosh, raqobatbardosh narxiga va to\'liqligiga o\'xshash ish tajribasi hisobga olinadi.',
            'description_ru' => 'Заказчик: АО «Xalqaro Hamkorlik Markazi», приглашает заинтересованные компании принять участие в конкурсном отборе на оказание услуг 

выполнение работ по демонтажу внутренней отделки в здании Торгового Центра «Ташкент- Плаза», расположенном в Юнус Абадском районе города Ташкента пр. Амира Тимура,107В. 

Для участия в отборе, компания должна представить коммерческое предложение на фирменном бланке (с реквизитами организации) с содержанием условий договора, с указанием номера, даты регистрации и подписью руководителя, заверенной печатью организации. 

Коммерческое предложение принимается в запечатанных конвертах, опечатанные на местах склейки по адресу: г. Ташкент Юнус Абадский р-н пр. Амира Тимура 107-В. 

При отборе учитываются опыт работы, аналогичный конкурсному, конкурентная цена и полнота предлагаемых услуг. 
',
            'description_en' => 'Customer: Xalqaro Hamkorlik Markazi, as, invites interested companies to take part in competitive selection for the provision of services

dismantling of interior decoration in the building of the Shopping Center "Tashkent-Plaza", located in Yunus Abad district of Tashkent, Amir Timur Avenue, 107B.

To participate in the selection, the company must submit a quotation on a letterhead (with the details of the organization) with the contents of the terms of the contract, indicating the number, date of registration and the signature of the head, stamped by the organization.

The commercial offer is accepted in sealed envelopes, sealed at the places of gluing at the address: Tashkent, Yunus Abad District, Amir Timur Ave. 107-B.

The selection takes into account work experience, similar to the competitive, competitive price and completeness of the services offered.'
        ));

        $this->insert('news', array(
            'type' => 1,
            'fone' => 'news_fon2.png',
            'photo' => 'news_img2.png',
            'date' => date('Y-m-d'),
            'title_uz' => 'Sotuvlar allaqachon kelmoqda!',
            'title_ru' => 'Распродажа уже идет!!!',
            'title_en' => 'Sale is already coming !!!',
            'text_uz' => 'Tutaevskoye magistralida 8-sonli A-Markazga tashrif buyurishingiz va orzularingizni ro\'yobga chiqarish uchun vaqtingiz bor.
Siz nafaqat maksimal foyda, balki sovg\'alarni ham hayratda qoldirasiz! Bu vaqt keldi ...',
            'text_ru' => 'Осталось 4 грандиозных дня, что бы придти на распродажу A-Center на Тутаевском шоссе, 8 и успеть осуществить свою мечту! 
Вы будете приятно удивлены не только максимальным выгодам, но и подаркам! Пришло то вре...',
            'text_en' => 'There are 4 grand days left to come to the A-Center sale on Tutaevskoye Highway, 8 and have time to fulfill your dream!
You will be pleasantly surprised not only the maximum benefits, but also gifts! That time has come ...',
            'description_uz' => 'Xaridor: Xalqaro Hamkorlik Markazi, manfaatdor kompaniyalarni xizmatlarni taqdim etish uchun tanlovli tanlovda ishtirok etishga taklif qiladi

Toshkent-Yunus-Obod tumani, Amir Temur shoh ko\'chasi, 107A uyda joylashgan "Toshkent-Plaza" Savatga markazi binosida ichki bezatish ishlarini olib tashlash.

Tanlovda qatnashish uchun firma shartnoma shartlari mazmuniga ega bo\'lib, unda firma tomonidan yozilgan firma raqami, ro\'yxatdan o\'tgan sanasi va imzosi ko\'rsatilgan holda firma blankida (tashkilotning tafsilotlari bilan) kotirovkalarni taqdim etishlari shart.

Tovar taklifi yopiq konvertlarda yopishtirilgan joylarda, Toshkent shahri, Yunus-Obod tumani, Amir Temur shoh ko\'chasi 107-B manzilida muhrlanadi.

Tanlovda taqdim etilayotgan xizmatlarning raqobatbardosh, raqobatbardosh narxiga va to\'liqligiga o\'xshash ish tajribasi hisobga olinadi.',
            'description_ru' => 'Заказчик: АО «Xalqaro Hamkorlik Markazi», приглашает заинтересованные компании принять участие в конкурсном отборе на оказание услуг 

выполнение работ по демонтажу внутренней отделки в здании Торгового Центра «Ташкент- Плаза», расположенном в Юнус Абадском районе города Ташкента пр. Амира Тимура,107В. 

Для участия в отборе, компания должна представить коммерческое предложение на фирменном бланке (с реквизитами организации) с содержанием условий договора, с указанием номера, даты регистрации и подписью руководителя, заверенной печатью организации. 

Коммерческое предложение принимается в запечатанных конвертах, опечатанные на местах склейки по адресу: г. Ташкент Юнус Абадский р-н пр. Амира Тимура 107-В. 

При отборе учитываются опыт работы, аналогичный конкурсному, конкурентная цена и полнота предлагаемых услуг. 
',
            'description_en' => 'Customer: Xalqaro Hamkorlik Markazi, as, invites interested companies to take part in competitive selection for the provision of services

dismantling of interior decoration in the building of the Shopping Center "Tashkent-Plaza", located in Yunus Abad district of Tashkent, Amir Timur Avenue, 107B.

To participate in the selection, the company must submit a quotation on a letterhead (with the details of the organization) with the contents of the terms of the contract, indicating the number, date of registration and the signature of the head, stamped by the organization.

The commercial offer is accepted in sealed envelopes, sealed at the places of gluing at the address: Tashkent, Yunus Abad District, Amir Timur Ave. 107-B.

The selection takes into account work experience, similar to the competitive, competitive price and completeness of the services offered.'
        ));

        $this->insert('news',array(
            'type' => 1,
            'fone' => 'news_fon3.png',
            'photo' => 'news_img3.png',
            'date' => date('Y-m-d'),
            'title_uz' => 'Sotuvlar allaqachon kelmoqda!',
            'title_ru' => 'Распродажа уже идет!!!',
            'title_en' => 'Sale is already coming !!!',
            'text_uz' => 'Tutaevskoye magistralida 8-sonli A-Markazga tashrif buyurishingiz va orzularingizni ro\'yobga chiqarish uchun vaqtingiz bor.
Siz nafaqat maksimal foyda, balki sovg\'alarni ham hayratda qoldirasiz! Bu vaqt keldi ...',
            'text_ru' => 'Осталось 4 грандиозных дня, что бы придти на распродажу A-Center на Тутаевском шоссе, 8 и успеть осуществить свою мечту! 
Вы будете приятно удивлены не только максимальным выгодам, но и подаркам! Пришло то вре...',
            'text_en' => 'There are 4 grand days left to come to the A-Center sale on Tutaevskoye Highway, 8 and have time to fulfill your dream!
You will be pleasantly surprised not only the maximum benefits, but also gifts! That time has come ...',
            'description_uz' => 'Xaridor: Xalqaro Hamkorlik Markazi, manfaatdor kompaniyalarni xizmatlarni taqdim etish uchun tanlovli tanlovda ishtirok etishga taklif qiladi

Toshkent-Yunus-Obod tumani, Amir Temur shoh ko\'chasi, 107A uyda joylashgan "Toshkent-Plaza" Savatga markazi binosida ichki bezatish ishlarini olib tashlash.

Tanlovda qatnashish uchun firma shartnoma shartlari mazmuniga ega bo\'lib, unda firma tomonidan yozilgan firma raqami, ro\'yxatdan o\'tgan sanasi va imzosi ko\'rsatilgan holda firma blankida (tashkilotning tafsilotlari bilan) kotirovkalarni taqdim etishlari shart.

Tovar taklifi yopiq konvertlarda yopishtirilgan joylarda, Toshkent shahri, Yunus-Obod tumani, Amir Temur shoh ko\'chasi 107-B manzilida muhrlanadi.

Tanlovda taqdim etilayotgan xizmatlarning raqobatbardosh, raqobatbardosh narxiga va to\'liqligiga o\'xshash ish tajribasi hisobga olinadi.',
            'description_ru' => 'Заказчик: АО «Xalqaro Hamkorlik Markazi», приглашает заинтересованные компании принять участие в конкурсном отборе на оказание услуг 

выполнение работ по демонтажу внутренней отделки в здании Торгового Центра «Ташкент- Плаза», расположенном в Юнус Абадском районе города Ташкента пр. Амира Тимура,107В. 

Для участия в отборе, компания должна представить коммерческое предложение на фирменном бланке (с реквизитами организации) с содержанием условий договора, с указанием номера, даты регистрации и подписью руководителя, заверенной печатью организации. 

Коммерческое предложение принимается в запечатанных конвертах, опечатанные на местах склейки по адресу: г. Ташкент Юнус Абадский р-н пр. Амира Тимура 107-В. 

При отборе учитываются опыт работы, аналогичный конкурсному, конкурентная цена и полнота предлагаемых услуг. 
',
            'description_en' => 'Customer: Xalqaro Hamkorlik Markazi, as, invites interested companies to take part in competitive selection for the provision of services

dismantling of interior decoration in the building of the Shopping Center "Tashkent-Plaza", located in Yunus Abad district of Tashkent, Amir Timur Avenue, 107B.

To participate in the selection, the company must submit a quotation on a letterhead (with the details of the organization) with the contents of the terms of the contract, indicating the number, date of registration and the signature of the head, stamped by the organization.

The commercial offer is accepted in sealed envelopes, sealed at the places of gluing at the address: Tashkent, Yunus Abad District, Amir Timur Ave. 107-B.

The selection takes into account work experience, similar to the competitive, competitive price and completeness of the services offered.'
        ));

        $this->insert('news',array(
            'type' => 1,
            'fone' => 'news_fon4.png',
            'photo' => 'news_img4.png',
            'date' => date('Y-m-d'),
            'title_uz' => 'Sotuvlar allaqachon kelmoqda!',
            'title_ru' => 'Распродажа уже идет!!!',
            'title_en' => 'Sale is already coming !!!',
            'text_uz' => 'Tutaevskoye magistralida 8-sonli A-Markazga tashrif buyurishingiz va orzularingizni ro\'yobga chiqarish uchun vaqtingiz bor.
Siz nafaqat maksimal foyda, balki sovg\'alarni ham hayratda qoldirasiz! Bu vaqt keldi ...',
            'text_ru' => 'Осталось 4 грандиозных дня, что бы придти на распродажу A-Center на Тутаевском шоссе, 8 и успеть осуществить свою мечту! 
Вы будете приятно удивлены не только максимальным выгодам, но и подаркам! Пришло то вре...',
            'text_en' => 'There are 4 grand days left to come to the A-Center sale on Tutaevskoye Highway, 8 and have time to fulfill your dream!
You will be pleasantly surprised not only the maximum benefits, but also gifts! That time has come ...',
            'description_uz' => 'Xaridor: Xalqaro Hamkorlik Markazi, manfaatdor kompaniyalarni xizmatlarni taqdim etish uchun tanlovli tanlovda ishtirok etishga taklif qiladi

Toshkent-Yunus-Obod tumani, Amir Temur shoh ko\'chasi, 107A uyda joylashgan "Toshkent-Plaza" Savatga markazi binosida ichki bezatish ishlarini olib tashlash.

Tanlovda qatnashish uchun firma shartnoma shartlari mazmuniga ega bo\'lib, unda firma tomonidan yozilgan firma raqami, ro\'yxatdan o\'tgan sanasi va imzosi ko\'rsatilgan holda firma blankida (tashkilotning tafsilotlari bilan) kotirovkalarni taqdim etishlari shart.

Tovar taklifi yopiq konvertlarda yopishtirilgan joylarda, Toshkent shahri, Yunus-Obod tumani, Amir Temur shoh ko\'chasi 107-B manzilida muhrlanadi.

Tanlovda taqdim etilayotgan xizmatlarning raqobatbardosh, raqobatbardosh narxiga va to\'liqligiga o\'xshash ish tajribasi hisobga olinadi.',
            'description_ru' => 'Заказчик: АО «Xalqaro Hamkorlik Markazi», приглашает заинтересованные компании принять участие в конкурсном отборе на оказание услуг 

выполнение работ по демонтажу внутренней отделки в здании Торгового Центра «Ташкент- Плаза», расположенном в Юнус Абадском районе города Ташкента пр. Амира Тимура,107В. 

Для участия в отборе, компания должна представить коммерческое предложение на фирменном бланке (с реквизитами организации) с содержанием условий договора, с указанием номера, даты регистрации и подписью руководителя, заверенной печатью организации. 

Коммерческое предложение принимается в запечатанных конвертах, опечатанные на местах склейки по адресу: г. Ташкент Юнус Абадский р-н пр. Амира Тимура 107-В. 

При отборе учитываются опыт работы, аналогичный конкурсному, конкурентная цена и полнота предлагаемых услуг. 
',
            'description_en' => 'Customer: Xalqaro Hamkorlik Markazi, as, invites interested companies to take part in competitive selection for the provision of services

dismantling of interior decoration in the building of the Shopping Center "Tashkent-Plaza", located in Yunus Abad district of Tashkent, Amir Timur Avenue, 107B.

To participate in the selection, the company must submit a quotation on a letterhead (with the details of the organization) with the contents of the terms of the contract, indicating the number, date of registration and the signature of the head, stamped by the organization.

The commercial offer is accepted in sealed envelopes, sealed at the places of gluing at the address: Tashkent, Yunus Abad District, Amir Timur Ave. 107-B.

The selection takes into account work experience, similar to the competitive, competitive price and completeness of the services offered.'
        ));

        $this->insert('news',array(
            'type' => 2,
            'fone' => 'promotion_fon1.png',
            'photo' => 'promotion_img1.png',
            'date' => date('Y-m-d'),
            'title_uz' => 'Sotuvlar allaqachon kelmoqda!',
            'title_ru' => 'Распродажа уже идет!!!',
            'title_en' => 'Sale is already coming !!!',
            'text_uz' => 'Tutaevskoye magistralida 8-sonli A-Markazga tashrif buyurishingiz va orzularingizni ro\'yobga chiqarish uchun vaqtingiz bor.
Siz nafaqat maksimal foyda, balki sovg\'alarni ham hayratda qoldirasiz! Bu vaqt keldi ...',
            'text_ru' => 'Осталось 4 грандиозных дня, что бы придти на распродажу A-Center на Тутаевском шоссе, 8 и успеть осуществить свою мечту! 
Вы будете приятно удивлены не только максимальным выгодам, но и подаркам! Пришло то вре...',
            'text_en' => 'There are 4 grand days left to come to the A-Center sale on Tutaevskoye Highway, 8 and have time to fulfill your dream!
You will be pleasantly surprised not only the maximum benefits, but also gifts! That time has come ...',
            'description_uz' => 'Xaridor: Xalqaro Hamkorlik Markazi, manfaatdor kompaniyalarni xizmatlarni taqdim etish uchun tanlovli tanlovda ishtirok etishga taklif qiladi

Toshkent-Yunus-Obod tumani, Amir Temur shoh ko\'chasi, 107A uyda joylashgan "Toshkent-Plaza" Savatga markazi binosida ichki bezatish ishlarini olib tashlash.

Tanlovda qatnashish uchun firma shartnoma shartlari mazmuniga ega bo\'lib, unda firma tomonidan yozilgan firma raqami, ro\'yxatdan o\'tgan sanasi va imzosi ko\'rsatilgan holda firma blankida (tashkilotning tafsilotlari bilan) kotirovkalarni taqdim etishlari shart.

Tovar taklifi yopiq konvertlarda yopishtirilgan joylarda, Toshkent shahri, Yunus-Obod tumani, Amir Temur shoh ko\'chasi 107-B manzilida muhrlanadi.

Tanlovda taqdim etilayotgan xizmatlarning raqobatbardosh, raqobatbardosh narxiga va to\'liqligiga o\'xshash ish tajribasi hisobga olinadi.',
            'description_ru' => 'Заказчик: АО «Xalqaro Hamkorlik Markazi», приглашает заинтересованные компании принять участие в конкурсном отборе на оказание услуг 

выполнение работ по демонтажу внутренней отделки в здании Торгового Центра «Ташкент- Плаза», расположенном в Юнус Абадском районе города Ташкента пр. Амира Тимура,107В. 

Для участия в отборе, компания должна представить коммерческое предложение на фирменном бланке (с реквизитами организации) с содержанием условий договора, с указанием номера, даты регистрации и подписью руководителя, заверенной печатью организации. 

Коммерческое предложение принимается в запечатанных конвертах, опечатанные на местах склейки по адресу: г. Ташкент Юнус Абадский р-н пр. Амира Тимура 107-В. 

При отборе учитываются опыт работы, аналогичный конкурсному, конкурентная цена и полнота предлагаемых услуг. 
',
            'description_en' => 'Customer: Xalqaro Hamkorlik Markazi, as, invites interested companies to take part in competitive selection for the provision of services

dismantling of interior decoration in the building of the Shopping Center "Tashkent-Plaza", located in Yunus Abad district of Tashkent, Amir Timur Avenue, 107B.

To participate in the selection, the company must submit a quotation on a letterhead (with the details of the organization) with the contents of the terms of the contract, indicating the number, date of registration and the signature of the head, stamped by the organization.

The commercial offer is accepted in sealed envelopes, sealed at the places of gluing at the address: Tashkent, Yunus Abad District, Amir Timur Ave. 107-B.

The selection takes into account work experience, similar to the competitive, competitive price and completeness of the services offered.'
        ));


        $this->insert('news',array(
            'type' => 2,
            'fone' => 'promotion_fon2.png',
            'photo' => 'promotion_img2.png',
            'date' => date('Y-m-d'),
            'title_uz' => 'Sotuvlar allaqachon kelmoqda!',
            'title_ru' => 'Распродажа уже идет!!!',
            'title_en' => 'Sale is already coming !!!',
            'text_uz' => 'Tutaevskoye magistralida 8-sonli A-Markazga tashrif buyurishingiz va orzularingizni ro\'yobga chiqarish uchun vaqtingiz bor.
Siz nafaqat maksimal foyda, balki sovg\'alarni ham hayratda qoldirasiz! Bu vaqt keldi ...',
            'text_ru' => 'Осталось 4 грандиозных дня, что бы придти на распродажу A-Center на Тутаевском шоссе, 8 и успеть осуществить свою мечту! 
Вы будете приятно удивлены не только максимальным выгодам, но и подаркам! Пришло то вре...',
            'text_en' => 'There are 4 grand days left to come to the A-Center sale on Tutaevskoye Highway, 8 and have time to fulfill your dream!
You will be pleasantly surprised not only the maximum benefits, but also gifts! That time has come ...',
            'description_uz' => 'Xaridor: Xalqaro Hamkorlik Markazi, manfaatdor kompaniyalarni xizmatlarni taqdim etish uchun tanlovli tanlovda ishtirok etishga taklif qiladi

Toshkent-Yunus-Obod tumani, Amir Temur shoh ko\'chasi, 107A uyda joylashgan "Toshkent-Plaza" Savatga markazi binosida ichki bezatish ishlarini olib tashlash.

Tanlovda qatnashish uchun firma shartnoma shartlari mazmuniga ega bo\'lib, unda firma tomonidan yozilgan firma raqami, ro\'yxatdan o\'tgan sanasi va imzosi ko\'rsatilgan holda firma blankida (tashkilotning tafsilotlari bilan) kotirovkalarni taqdim etishlari shart.

Tovar taklifi yopiq konvertlarda yopishtirilgan joylarda, Toshkent shahri, Yunus-Obod tumani, Amir Temur shoh ko\'chasi 107-B manzilida muhrlanadi.

Tanlovda taqdim etilayotgan xizmatlarning raqobatbardosh, raqobatbardosh narxiga va to\'liqligiga o\'xshash ish tajribasi hisobga olinadi.',
            'description_ru' => 'Заказчик: АО «Xalqaro Hamkorlik Markazi», приглашает заинтересованные компании принять участие в конкурсном отборе на оказание услуг 

выполнение работ по демонтажу внутренней отделки в здании Торгового Центра «Ташкент- Плаза», расположенном в Юнус Абадском районе города Ташкента пр. Амира Тимура,107В. 

Для участия в отборе, компания должна представить коммерческое предложение на фирменном бланке (с реквизитами организации) с содержанием условий договора, с указанием номера, даты регистрации и подписью руководителя, заверенной печатью организации. 

Коммерческое предложение принимается в запечатанных конвертах, опечатанные на местах склейки по адресу: г. Ташкент Юнус Абадский р-н пр. Амира Тимура 107-В. 

При отборе учитываются опыт работы, аналогичный конкурсному, конкурентная цена и полнота предлагаемых услуг. 
',
            'description_en' => 'Customer: Xalqaro Hamkorlik Markazi, as, invites interested companies to take part in competitive selection for the provision of services

dismantling of interior decoration in the building of the Shopping Center "Tashkent-Plaza", located in Yunus Abad district of Tashkent, Amir Timur Avenue, 107B.

To participate in the selection, the company must submit a quotation on a letterhead (with the details of the organization) with the contents of the terms of the contract, indicating the number, date of registration and the signature of the head, stamped by the organization.

The commercial offer is accepted in sealed envelopes, sealed at the places of gluing at the address: Tashkent, Yunus Abad District, Amir Timur Ave. 107-B.

The selection takes into account work experience, similar to the competitive, competitive price and completeness of the services offered.'
        ));

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news');
    }
}
