<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%vacancy}}`.
 */
class m190703_182419_create_vacancy_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('vacancy', [
            'id' => $this->primaryKey(),
            'name_uz' => $this->string(255)->comment('Nomi'),
            'name_ru' => $this->string(255)->comment('Наименование'),
            'name_en' => $this->string(255)->comment('Name'),
            'text_uz' => $this->text()->comment('Matn'),
            'text_ru' => $this->text()->comment('Текст'),
            'text_en' => $this->text()->comment('Text'),
        ]);

        $this->insert('vacancy', array(
            'name_uz' => 'Biznes markazi boshqaruvchisi',
            'name_ru' => 'Администратор Бизнес-центра',
            'name_en' => 'Business Center Administrator',
            'text_uz' => '
                <h5>Vazifalari:</h5>
                <ul>
                    <li>Biznes-markaz mijozlari va mehmonlarini qabul qilish va tabriklash;</li>
                    <li>Buxgalteriya tizimida mehmon kartalarini ro\'yxatdan o\'tkazish va ro\'yxatdan o\'tkazish;</li>
                    <li>Qabul maydoni va mijozlar hududining holatini kuzatish.</li>
                </ul>
                <h5>Talablar:</h5>
                <ul>
                    <li>Kiyinish kodiga tayoq berishga tayyor; <br> <br></li>
                </ul>
                <h5>Shartlar:</h5>
                <ul>
                    <li>Rossiya Federatsiyasi Mehnat Kodeksi doirasida rasmiy ravishda ishga joylashish;</li>
                    <li>Ish vaqti 2/2 с 08.00-20.00;</li>
                    <li>Ish joyi: Avtozavodskaya metro stantsiyasi (metrogacha 3 daqiqa);</li>
                    <li>Barqaror, qat\'iy maosh .</li>
                </ul >
           ',
            'text_ru' => '
                <h5>Обязанности:</h5>
                <ul>
                    <li>Прием и приветствие клиентов и посетителей Бизнес-центра;</li>
                    <li>Оформление и регистрация карточек гостевых пропусков в учетной системе;</li>
                    <li>Контроль состояния зоны ресепшен и клиентской зоны.</li>
                </ul>
                <h5>Требования:</h5>
                <ul>
                    <li>Готовность придерживаться дресс-кода; <br> <br></li>
                </ul>
                <h5>Условия:</h5>
                <ul>
                    <li>Официальное трудоустройство по ТК РФ;</li>
                    <li>График работы 2/2 с 08.00-20.00;</li>
                    <li>Место работы на выбор: м.Автозаводская (3 мин. от метро);</li>
                    <li>Стабильный, фиксированный оклад.</li>
                </ul>
         ',
            'text_en' => '
                <h5>Duties:</h5>
                <ul>
                    <li>Reception and greetings of customers and visitors of the Business Center;</li>
                    <li>Registration and registration of guest pass cards in the accounting system;</li>
                    <li>Monitoring the state of the reception area and the customer area.</li>
                </ul>
                <h5>Requirements:</h5>
                <ul>
                    <li>Willing to stick to the dress code; <br> <br></li>
                </ul>
                <h5>Условия:</h5>
                <ul>
                    <li>Official employment under the Labor Code of the Russian Federation;</li>
                    <li>Schedule 2/2 from 08.00-20.00;</li>
                    <li>Place of work to choose from: metro station Avtozavodskaya (3 minutes from the metro);</li>
                    <li>Stable, fixed salary.</li>
                </ul>
           '
        ));

        $this->insert('vacancy', array(
            'name_uz' => 'Biznes markazidagi kotib',
            'name_ru' => 'Секретарь в бизнес-центре',
            'name_en' => 'Secretary in the business center',
            'text_uz' => '
                <h5>Vazifalari:</h5>
                <ul>
                    <li>Biznes-markaz mijozlari va mehmonlarini qabul qilish va tabriklash;</li>
                    <li>Buxgalteriya tizimida mehmon kartalarini ro\'yxatdan o\'tkazish va ro\'yxatdan o\'tkazish;</li>
                    <li>Qabul maydoni va mijozlar hududining holatini kuzatish.</li>
                </ul>
                <h5>Talablar:</h5>
                <ul>
                    <li>Kiyinish kodiga tayoq berishga tayyor; <br> <br></li>
                </ul>
                <h5>Shartlar:</h5>
                <ul>
                    <li>Rossiya Federatsiyasi Mehnat Kodeksi doirasida rasmiy ravishda ishga joylashish;</li>
                    <li>Ish vaqti 2/2 с 08.00-20.00;</li>
                    <li>Ish joyi: Avtozavodskaya metro stantsiyasi (metrogacha 3 daqiqa);</li>
                    <li>Barqaror, qat\'iy maosh .</li>
                </ul >
            ',
            'text_ru' => '
                <h5>Обязанности:</h5>
                <ul>
                    <li>Прием и приветствие клиентов и посетителей Бизнес-центра;</li>
                    <li>Оформление и регистрация карточек гостевых пропусков в учетной системе;</li>
                    <li>Контроль состояния зоны ресепшен и клиентской зоны.</li>
                </ul>
                <h5>Требования:</h5>
                <ul>
                    <li>Готовность придерживаться дресс-кода; <br> <br></li>
                </ul>
                <h5>Условия:</h5>
                <ul>
                    <li>Официальное трудоустройство по ТК РФ;</li>
                    <li>График работы 2/2 с 08.00-20.00;</li>
                    <li>Место работы на выбор: м.Автозаводская (3 мин. от метро);</li>
                    <li>Стабильный, фиксированный оклад.</li>
                </ul>
          ',
            'text_en' => '<h5>Duties:</h5>
                <ul>
                    <li>Reception and greetings of customers and visitors of the Business Center;</li>
                    <li>Registration and registration of guest pass cards in the accounting system;</li>
                    <li>Monitoring the state of the reception area and the customer area.</li>
                </ul>
                <h5>Requirements:</h5>
                <ul>
                    <li>Willing to stick to the dress code; <br> <br></li>
                </ul>
                <h5>Условия:</h5>
                <ul>
                    <li>Official employment under the Labor Code of the Russian Federation;</li>
                    <li>Schedule 2/2 from 08.00-20.00;</li>
                    <li>Place of work to choose from: metro station Avtozavodskaya (3 minutes from the metro);</li>
                    <li>Stable, fixed salary.</li>
                </ul>'
        ));

        $this->insert('vacancy', array(
            'name_uz' => 'Biznes markazining binolarini ijaraga olish bo\'yicha mutaxassis',
            'name_ru' => 'Специалист по аренде помещений Бизнес Центра',
            'name_en' => 'Specialist for the rental of premises of the Business Center',
            'text_uz' => '
                <h5>Vazifalari:</h5>
                <ul>
                    <li>Biznes-markaz mijozlari va mehmonlarini qabul qilish va tabriklash;</li>
                    <li>Buxgalteriya tizimida mehmon kartalarini ro\'yxatdan o\'tkazish va ro\'yxatdan o\'tkazish;</li>
                    <li>Qabul maydoni va mijozlar hududining holatini kuzatish.</li>
                </ul>
                <h5>Talablar:</h5>
                <ul>
                    <li>Kiyinish kodiga tayoq berishga tayyor; <br> <br></li>
                </ul>
                <h5>Shartlar:</h5>
                <ul>
                    <li>Rossiya Federatsiyasi Mehnat Kodeksi doirasida rasmiy ravishda ishga joylashish;</li>
                    <li>Ish vaqti 2/2 с 08.00-20.00;</li>
                    <li>Ish joyi: Avtozavodskaya metro stantsiyasi (metrogacha 3 daqiqa);</li>
                    <li>Barqaror, qat\'iy maosh .</li >
                </ul>
           ',
            'text_ru' => '
                <h5>Обязанности:</h5>
                <ul>
                    <li>Прием и приветствие клиентов и посетителей Бизнес-центра;</li>
                    <li>Оформление и регистрация карточек гостевых пропусков в учетной системе;</li>
                    <li>Контроль состояния зоны ресепшен и клиентской зоны.</li>
                </ul>
                <h5>Требования:</h5>
                <ul>
                    <li>Готовность придерживаться дресс-кода; <br> <br></li>
                </ul>
                <h5>Условия:</h5>
                <ul>
                    <li>Официальное трудоустройство по ТК РФ;</li>
                    <li>График работы 2/2 с 08.00-20.00;</li>
                    <li>Место работы на выбор: м.Автозаводская (3 мин. от метро);</li>
                    <li>Стабильный, фиксированный оклад.</li>
                </ul>
        ',
            'text_en' => '<h5>Duties:</h5>
                <ul>
                    <li>Reception and greetings of customers and visitors of the Business Center;</li>
                    <li>Registration and registration of guest pass cards in the accounting system;</li>
                    <li>Monitoring the state of the reception area and the customer area.</li>
                </ul>
                <h5>Requirements:</h5>
                <ul>
                    <li>Willing to stick to the dress code; <br> <br></li>
                </ul>
                <h5>Условия:</h5>
                <ul>
                    <li>Official employment under the Labor Code of the Russian Federation;</li>
                    <li>Schedule 2/2 from 08.00-20.00;</li>
                    <li>Place of work to choose from: metro station Avtozavodskaya (3 minutes from the metro);</li>
                    <li>Stable, fixed salary.</li>
                </ul>'
        ));

        $this->insert('vacancy', array(
            'name_uz' => 'Biznes markazining rahbari',
            'name_ru' => 'Руководитель бизнес-центра',
            'name_en' => 'Head of the business center',
            'text_uz' => '
                <h5>Vazifalari:</h5>
                <ul>
                    <li>Biznes-markaz mijozlari va mehmonlarini qabul qilish va tabriklash;</li>
                    <li>Buxgalteriya tizimida mehmon kartalarini ro\'yxatdan o\'tkazish va ro\'yxatdan o\'tkazish;</li>
                    <li>Qabul maydoni va mijozlar hududining holatini kuzatish.</li>
                </ul>
                <h5>Talablar:</h5>
                <ul>
                    <li>Kiyinish kodiga tayoq berishga tayyor; <br> <br></li>
                </ul>
                <h5>Shartlar:</h5>
                <ul>
                    <li>Rossiya Federatsiyasi Mehnat Kodeksi doirasida rasmiy ravishda ishga joylashish;</li>
                    <li>Ish vaqti 2/2 с 08.00-20.00;</li>
                    <li>Ish joyi: Avtozavodskaya metro stantsiyasi (metrogacha 3 daqiqa);</li>
                    <li>Barqaror, qat\'iy maosh .</li >
                </ul >
            ',
            'text_ru' => '
                <h5>Обязанности:</h5>
                <ul>
                    <li>Прием и приветствие клиентов и посетителей Бизнес-центра;</li>
                    <li>Оформление и регистрация карточек гостевых пропусков в учетной системе;</li>
                    <li>Контроль состояния зоны ресепшен и клиентской зоны.</li>
                </ul>
                <h5>Требования:</h5>
                <ul>
                    <li>Готовность придерживаться дресс-кода; <br> <br></li>
                </ul>
                <h5>Условия:</h5>
                <ul>
                    <li>Официальное трудоустройство по ТК РФ;</li>
                    <li>График работы 2/2 с 08.00-20.00;</li>
                    <li>Место работы на выбор: м.Автозаводская (3 мин. от метро);</li>
                    <li>Стабильный, фиксированный оклад.</li>
                </ul>
            ',
            'text_en' => '<h5>Duties:</h5>
                <ul>
                    <li>Reception and greetings of customers and visitors of the Business Center;</li>
                    <li>Registration and registration of guest pass cards in the accounting system;</li>
                    <li>Monitoring the state of the reception area and the customer area.</li>
                </ul>
                <h5>Requirements:</h5>
                <ul>
                    <li>Willing to stick to the dress code; <br> <br></li>
                </ul>
                <h5>Условия:</h5>
                <ul>
                    <li>Official employment under the Labor Code of the Russian Federation;</li>
                    <li>Schedule 2/2 from 08.00-20.00;</li>
                    <li>Place of work to choose from: metro station Avtozavodskaya (3 minutes from the metro);</li>
                    <li>Stable, fixed salary.</li>
                </ul>'
        ));

        $this->insert('vacancy', array(
            'name_uz' => 'Biznes markazidagi tozalash menejeri',
            'name_ru' => 'Менеджер клининга в Бизнес-Центр',
            'name_en' => 'Cleaning Manager at the Business Center',
            'text_uz' => '
                <h5>Vazifalari:</h5>
                <ul>
                    <li>Biznes-markaz mijozlari va mehmonlarini qabul qilish va tabriklash;</li>
                    <li>Buxgalteriya tizimida mehmon kartalarini ro\'yxatdan o\'tkazish va ro\'yxatdan o\'tkazish;</li>
                    <li>Qabul maydoni va mijozlar hududining holatini kuzatish.</li>
                </ul>
                <h5>Talablar:</h5>
                <ul>
                    <li>Kiyinish kodiga tayoq berishga tayyor; <br> <br></li>
                </ul>
                <h5>Shartlar:</h5>
                <ul>
                    <li>Rossiya Federatsiyasi Mehnat Kodeksi doirasida rasmiy ravishda ishga joylashish;</li>
                    <li>Ish vaqti 2/2 с 08.00-20.00;</li>
                    <li>Ish joyi: Avtozavodskaya metro stantsiyasi (metrogacha 3 daqiqa);</li>
                    <li>Barqaror, qat\'iy maosh .</li>
                </ul >
      ',
            'text_ru' => '
                <h5>Обязанности:</h5>
                <ul>
                    <li>Прием и приветствие клиентов и посетителей Бизнес-центра;</li>
                    <li>Оформление и регистрация карточек гостевых пропусков в учетной системе;</li>
                    <li>Контроль состояния зоны ресепшен и клиентской зоны.</li>
                </ul>
                <h5>Требования:</h5>
                <ul>
                    <li>Готовность придерживаться дресс-кода; <br> <br></li>
                </ul>
                <h5>Условия:</h5>
                <ul>
                    <li>Официальное трудоустройство по ТК РФ;</li>
                    <li>График работы 2/2 с 08.00-20.00;</li>
                    <li>Место работы на выбор: м.Автозаводская (3 мин. от метро);</li>
                    <li>Стабильный, фиксированный оклад.</li>
                </ul>
            ',
            'text_en' => '<h5>Duties:</h5>
                <ul>
                    <li>Reception and greetings of customers and visitors of the Business Center;</li>
                    <li>Registration and registration of guest pass cards in the accounting system;</li>
                    <li>Monitoring the state of the reception area and the customer area.</li>
                </ul>
                <h5>Requirements:</h5>
                <ul>
                    <li>Willing to stick to the dress code; <br> <br></li>
                </ul>
                <h5>Условия:</h5>
                <ul>
                    <li>Official employment under the Labor Code of the Russian Federation;</li>
                    <li>Schedule 2/2 from 08.00-20.00;</li>
                    <li>Place of work to choose from: metro station Avtozavodskaya (3 minutes from the metro);</li>
                    <li>Stable, fixed salary.</li>
                </ul>'
        ));

        $this->insert('vacancy', array(
            'name_uz' => 'Moskva biznes markazlarida ofis ijarasi menejeri',
            'name_ru' => 'Менеджер по аренде офисов в Бизнес центрах Москвы',
            'name_en' => 'Office Rent Manager in Moscow Business Centers',
            'text_uz' => '
                <h5>Vazifalari:</h5>
                <ul>
                    <li>Biznes-markaz mijozlari va mehmonlarini qabul qilish va tabriklash;</li>
                    <li>Buxgalteriya tizimida mehmon kartalarini ro\'yxatdan o\'tkazish va ro\'yxatdan o\'tkazish;</li>
                    <li>Qabul maydoni va mijozlar hududining holatini kuzatish.</li>
                </ul>
                <h5>Talablar:</h5>
                <ul>
                    <li>Kiyinish kodiga tayoq berishga tayyor; <br> <br></li>
                </ul>
                <h5>Shartlar:</h5>
                <ul>
                    <li>Rossiya Federatsiyasi Mehnat Kodeksi doirasida rasmiy ravishda ishga joylashish;</li>
                    <li>Ish vaqti 2/2 с 08.00-20.00;</li>
                    <li>Ish joyi: Avtozavodskaya metro stantsiyasi (metrogacha 3 daqiqa);</li>
                    <li>Barqaror, qat\'iy maosh .</li >
                </ul >
           ',
            'text_ru' => '
                <h5>Обязанности:</h5>
                <ul>
                    <li>Прием и приветствие клиентов и посетителей Бизнес-центра;</li>
                    <li>Оформление и регистрация карточек гостевых пропусков в учетной системе;</li>
                    <li>Контроль состояния зоны ресепшен и клиентской зоны.</li>
                </ul>
                <h5>Требования:</h5>
                <ul>
                    <li>Готовность придерживаться дресс-кода; <br> <br></li>
                </ul>
                <h5>Условия:</h5>
                <ul>
                    <li>Официальное трудоустройство по ТК РФ;</li>
                    <li>График работы 2/2 с 08.00-20.00;</li>
                    <li>Место работы на выбор: м.Автозаводская (3 мин. от метро);</li>
                    <li>Стабильный, фиксированный оклад.</li>
                </ul>
            ',
            'text_en' => '<h5>Duties:</h5>
                <ul>
                    <li>Reception and greetings of customers and visitors of the Business Center;</li>
                    <li>Registration and registration of guest pass cards in the accounting system;</li>
                    <li>Monitoring the state of the reception area and the customer area.</li>
                </ul>
                <h5>Requirements:</h5>
                <ul>
                    <li>Willing to stick to the dress code; <br> <br></li>
                </ul>
                <h5>Условия:</h5>
                <ul>
                    <li>Official employment under the Labor Code of the Russian Federation;</li>
                    <li>Schedule 2/2 from 08.00-20.00;</li>
                    <li>Place of work to choose from: metro station Avtozavodskaya (3 minutes from the metro);</li>
                    <li>Stable, fixed salary.</li>
                </ul>'
        ));

        $this->insert('vacancy', array(
            'name_uz' => 'Muhandis-biznes markazi',
            'name_ru' => 'Инженер Бизнес-центра',
            'name_en' => 'Engineer Business Center',
            'text_uz' => '
                <h5>Vazifalari:</h5>
                <ul>
                    <li>Biznes-markaz mijozlari va mehmonlarini qabul qilish va tabriklash;</li>
                    <li>Buxgalteriya tizimida mehmon kartalarini ro\'yxatdan o\'tkazish va ro\'yxatdan o\'tkazish;</li>
                    <li>Qabul maydoni va mijozlar hududining holatini kuzatish.</li>
                </ul>
                <h5>Talablar:</h5>
                <ul>
                    <li>Kiyinish kodiga tayoq berishga tayyor; <br> <br></li>
                </ul>
                <h5>Shartlar:</h5>
                <ul>
                    <li>Rossiya Federatsiyasi Mehnat Kodeksi doirasida rasmiy ravishda ishga joylashish;</li>
                    <li>Ish vaqti 2/2 с 08.00-20.00;</li>
                    <li>Ish joyi: Avtozavodskaya metro stantsiyasi (metrogacha 3 daqiqa);</li>
                    <li>Barqaror, qat\'iy maosh .</li >
                </ul >
          ',
            'text_ru' => '
                <h5>Обязанности:</h5>
                <ul>
                    <li>Прием и приветствие клиентов и посетителей Бизнес-центра;</li>
                    <li>Оформление и регистрация карточек гостевых пропусков в учетной системе;</li>
                    <li>Контроль состояния зоны ресепшен и клиентской зоны.</li>
                </ul>
                <h5>Требования:</h5>
                <ul>
                    <li>Готовность придерживаться дресс-кода; <br> <br></li>
                </ul>
                <h5>Условия:</h5>
                <ul>
                    <li>Официальное трудоустройство по ТК РФ;</li>
                    <li>График работы 2/2 с 08.00-20.00;</li>
                    <li>Место работы на выбор: м.Автозаводская (3 мин. от метро);</li>
                    <li>Стабильный, фиксированный оклад.</li>
                </ul>
           ',
            'text_en' => '<h5>Duties:</h5>
                <ul>
                    <li>Reception and greetings of customers and visitors of the Business Center;</li>
                    <li>Registration and registration of guest pass cards in the accounting system;</li>
                    <li>Monitoring the state of the reception area and the customer area.</li>
                </ul>
                <h5>Requirements:</h5>
                <ul>
                    <li>Willing to stick to the dress code; <br> <br></li>
                </ul>
                <h5>Условия:</h5>
                <ul>
                    <li>Official employment under the Labor Code of the Russian Federation;</li>
                    <li>Schedule 2/2 from 08.00-20.00;</li>
                    <li>Place of work to choose from: metro station Avtozavodskaya (3 minutes from the metro);</li>
                    <li>Stable, fixed salary.</li>
                </ul>'
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('vacancy');
    }
}
