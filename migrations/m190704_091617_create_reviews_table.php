<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%reviews}}`.
 */
class m190704_091617_create_reviews_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('reviews', [
            'id' => $this->primaryKey(),
            'avatar' => $this->string(255)->comment('Фото'),
            'fio' => $this->string(255)->comment('ФИО'),
            'permission_uz' => $this->string(255)->comment('Lavozimi'),
            'permission_ru' => $this->string(255)->comment('Должность'),
            'permission_en' => $this->string(255)->comment('Permission'),
            'title_uz' => $this->text()->comment('Sarlavha'),
            'title_ru' => $this->text()->comment('Заголовок'),
            'title_en' => $this->text()->comment('Title'),
            'text_uz' => $this->text()->comment('Matn'),
            'text_ru' => $this->text()->comment('Текст'),
            'text_en' => $this->text()->comment('text'),

        ]);
        $this->insert('reviews', array(
            'avatar' => 'review_img.png',
            'fio' => 'Кобаяси Косукэ',
            'permission_uz' => 'Katta hisob boshqaruvchisi',
            'permission_ru' => 'Старший менеджер по работе с клиентами',
            'permission_en' => 'Senior Account Manager',
            'title_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz professional uchrashuvni uchratasiz',
            'title_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в',
            'title_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in',
            'text_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.
  Biz sinab ko\'rilgan va sinab ko\'rilgan usuli va tajribali va ehtiyotkorlik bilan hamrohlik qilishingizni tavsiya qilamiz ...',
            'text_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках. 
 Мы предлагаем вам воспользоваться как проверенным и испытанным методом исследования, так и опытным и внимательным сопровождением...',
            'text_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.
  We suggest that you use both a tried and tested method of research, and an experienced and attentive accompaniment ...',

        ));

        $this->insert('reviews', array(
            'avatar' => 'review_img1.png',
            'fio' => 'Кобаяси Косукэ',
            'permission_uz' => 'Katta hisob boshqaruvchisi',
            'permission_ru' => 'Старший менеджер по работе с клиентами',
            'permission_en' => 'Senior Account Manager',
            'title_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz professional uchrashuvni uchratasiz',
            'title_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в',
            'title_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in',
            'text_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.
  Biz sinab ko\'rilgan va sinab ko\'rilgan usuli va tajribali va ehtiyotkorlik bilan hamrohlik qilishingizni tavsiya qilamiz ...',
            'text_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках. 
 Мы предлагаем вам воспользоваться как проверенным и испытанным методом исследования, так и опытным и внимательным сопровождением...',
            'text_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.
  We suggest that you use both a tried and tested method of research, and an experienced and attentive accompaniment ...',

        ));

        $this->insert('reviews', array(
            'avatar' => 'review_img2.png',
            'fio' => 'Кобаяси Косукэ',
            'permission_uz' => 'Katta hisob boshqaruvchisi',
            'permission_ru' => 'Старший менеджер по работе с клиентами',
            'permission_en' => 'Senior Account Manager',
            'title_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz professional uchrashuvni uchratasiz',
            'title_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в',
            'title_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in',
            'text_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.
  Biz sinab ko\'rilgan va sinab ko\'rilgan usuli va tajribali va ehtiyotkorlik bilan hamrohlik qilishingizni tavsiya qilamiz ...',
            'text_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках. 
 Мы предлагаем вам воспользоваться как проверенным и испытанным методом исследования, так и опытным и внимательным сопровождением...',
            'text_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.
  We suggest that you use both a tried and tested method of research, and an experienced and attentive accompaniment ...',

        ));

        $this->insert('reviews', array(
            'avatar' => 'review_img3.png',
            'fio' => 'Кобаяси Косукэ',
            'permission_uz' => 'Katta hisob boshqaruvchisi',
            'permission_ru' => 'Старший менеджер по работе с клиентами',
            'permission_en' => 'Senior Account Manager',
            'title_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz professional uchrashuvni uchratasiz',
            'title_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в',
            'title_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in',
            'text_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.
  Biz sinab ko\'rilgan va sinab ko\'rilgan usuli va tajribali va ehtiyotkorlik bilan hamrohlik qilishingizni tavsiya qilamiz ...',
            'text_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках. 
 Мы предлагаем вам воспользоваться как проверенным и испытанным методом исследования, так и опытным и внимательным сопровождением...',
            'text_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.
  We suggest that you use both a tried and tested method of research, and an experienced and attentive accompaniment ...',

        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('reviews');
    }
}
