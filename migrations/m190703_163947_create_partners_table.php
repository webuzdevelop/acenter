<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%partners}}`.
 */
class m190703_163947_create_partners_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('partners', [
            'id' => $this->primaryKey(),
            'name_uz' => $this->string(255)->comment('Nomi'),
            'name_ru' => $this->string(255)->comment('Наименование'),
            'name_en' => $this->string(255)->comment('Name'),
            'text_uz' => $this->text()->comment('Matn'),
            'text_ru' => $this->text()->comment('Текст'),
            'text_en' => $this->text()->comment('Text'),
            'photos' => $this->text()->comment('Фотографии')
        ]);

        $this->insert('partners', array(
            'name_uz' => 'Xona A',
            'name_ru' => 'Помешение А',
            'name_en' => 'The room A',
            'text_uz' => '
                <h5>Vazifalari:</h5>
                <ul>
                    <li>Biznes-markaz mijozlari va mehmonlarini qabul qilish va tabriklash;</li>
                    <li>Buxgalteriya tizimida mehmon kartalarini ro\'yxatdan o\'tkazish va ro\'yxatdan o\'tkazish;</li>
                    <li>Qabul maydoni va mijozlar hududining holatini kuzatish.</li>
                </ul>
                <h5>Talablar:</h5>
                <ul>
                    <li>Kiyinish kodiga tayoq berishga tayyor; <br> <br></li>
                </ul>
                <h5>Shart:</h5>
                <ul>
                    <li>Rossiya Federatsiyasi Mehnat Kodeksi doirasida rasmiy ravishda ishga joylashish;</li>
                    <li>Ish vaqti 2/2 с 08.00-20.00;</li>
                    <li>Ish joyi: Avtozavodskaya metro stantsiyasi (metrogacha 3 daqiqa);</li>
                    <li>Barqaror, qat\'iy maosh.</li>
                </ul>
                ',
            'text_ru' => '
                <h5>Обязанности:</h5>
                <ul>
                    <li>Прием и приветствие клиентов и посетителей Бизнес-центра;</li>
                    <li>Оформление и регистрация карточек гостевых пропусков в учетной системе;</li>
                    <li>Контроль состояния зоны ресепшен и клиентской зоны.</li>
                </ul>
                <h5>Требования:</h5>
                <ul>
                    <li>Готовность придерживаться дресс-кода; <br> <br></li>
                </ul>
                <h5>Условия:</h5>
                <ul>
                    <li>Официальное трудоустройство по ТК РФ;</li>
                    <li>График работы 2/2 с 08.00-20.00;</li>
                    <li>Место работы на выбор: м.Автозаводская (3 мин. от метро);</li>
                    <li>Стабильный, фиксированный оклад.</li>
                </ul>
              ',
            'text_en' => '<h5>Responsibility:</h5>
                <ul>
                    <li>Welcome and welcome customers and visitors of the Business Center;</li>
                    <li>Registration and registration of guest pass cards in the accounting system;</li>
                    <li>Monitoring the status of the reception area and the customer area.</li>
                </ul>
                <h5>Requirements:</h5>
                <ul>
                    <li>Willingness to adhere to the dress code; <br> <br></li>
                </ul>
                <h5>Conditions:</h5>
                <ul>
                    <li>Official employment in the Labor Code of the Russian Federation;</li>
                    <li>Schedule 2/2 from 08.00-20.00;</li>
                    <li>Place of work to choose: metro Avtozavodskaya (3 minutes from the metro);</li>
                    <li>Stable, fixed salary.</li>
                </ul>
               ',
            'photos' => 'partner-slider-img1.png**partner-slider-img2.png**partner-slider-img3.png**partner-slider-img4.png**',
        ));

        $this->insert('partners', array(
            'name_uz' => 'Xona B',
            'name_ru' => 'Помешение Б',
            'name_en' => 'The room B',
            'text_uz' => '
                <h5>Vazifalari:</h5>
                <ul>
                    <li>Biznes-markaz mijozlari va mehmonlarini qabul qilish va tabriklash;</li>
                    <li>Buxgalteriya tizimida mehmon kartalarini ro\'yxatdan o\'tkazish va ro\'yxatdan o\'tkazish;</li>
                    <li>Qabul maydoni va mijozlar hududining holatini kuzatish.</li>
                </ul>
                <h5>Talablar:</h5>
                <ul>
                    <li>Kiyinish kodiga tayoq berishga tayyor; <br> <br></li>
                </ul>
                <h5>Shart:</h5>
                <ul>
                    <li>Rossiya Federatsiyasi Mehnat Kodeksi doirasida rasmiy ravishda ishga joylashish;</li>
                    <li>Ish vaqti 2/2 с 08.00-20.00;</li>
                    <li>Ish joyi: Avtozavodskaya metro stantsiyasi (metrogacha 3 daqiqa);</li>
                    <li>Barqaror, qat\'iy maosh.</li>
                </ul>
                ',
            'text_ru' => '
                <h5>Обязанности:</h5>
                <ul>
                    <li>Прием и приветствие клиентов и посетителей Бизнес-центра;</li>
                    <li>Оформление и регистрация карточек гостевых пропусков в учетной системе;</li>
                    <li>Контроль состояния зоны ресепшен и клиентской зоны.</li>
                </ul>
                <h5>Требования:</h5>
                <ul>
                    <li>Готовность придерживаться дресс-кода; <br> <br></li>
                </ul>
                <h5>Условия:</h5>
                <ul>
                    <li>Официальное трудоустройство по ТК РФ;</li>
                    <li>График работы 2/2 с 08.00-20.00;</li>
                    <li>Место работы на выбор: м.Автозаводская (3 мин. от метро);</li>
                    <li>Стабильный, фиксированный оклад.</li>
                </ul>
               ',
            'text_en' => '<h5>Responsibility:</h5>
                <ul>
                    <li>Welcome and welcome customers and visitors of the Business Center;</li>
                    <li>Registration and registration of guest pass cards in the accounting system;</li>
                    <li>Monitoring the status of the reception area and the customer area.</li>
                </ul>
                <h5>Requirements:</h5>
                <ul>
                    <li>Willingness to adhere to the dress code; <br> <br></li>
                </ul>
                <h5>Conditions:</h5>
                <ul>
                    <li>Official employment in the Labor Code of the Russian Federation;</li>
                    <li>Schedule 2/2 from 08.00-20.00;</li>
                    <li>Place of work to choose: metro Avtozavodskaya (3 minutes from the metro);</li>
                    <li>Stable, fixed salary.</li>
                </ul>',
            'photos' => 'partner-slider-img1.png**partner-slider-img2.png**partner-slider-img3.png**partner-slider-img4.png**',
        ));

        $this->insert('partners', array(
            'name_uz' => 'Xona C',
            'name_ru' => 'Помешение C',
            'name_en' => 'The room C',
            'text_uz' => '
                <h5>Vazifalari:</h5>
                <ul>
                    <li>Biznes-markaz mijozlari va mehmonlarini qabul qilish va tabriklash;</li>
                    <li>Buxgalteriya tizimida mehmon kartalarini ro\'yxatdan o\'tkazish va ro\'yxatdan o\'tkazish;</li>
                    <li>Qabul maydoni va mijozlar hududining holatini kuzatish.</li>
                </ul>
                <h5>Talablar:</h5>
                <ul>
                    <li>Kiyinish kodiga tayoq berishga tayyor; <br> <br></li>
                </ul>
                <h5>Shart:</h5>
                <ul>
                    <li>Rossiya Federatsiyasi Mehnat Kodeksi doirasida rasmiy ravishda ishga joylashish;</li>
                    <li>Ish vaqti 2/2 с 08.00-20.00;</li>
                    <li>Ish joyi: Avtozavodskaya metro stantsiyasi (metrogacha 3 daqiqa);</li>
                    <li>Barqaror, qat\'iy maosh.</li>
                </ul>
                ',
            'text_ru' => '
                <h5>Обязанности:</h5>
                <ul>
                    <li>Прием и приветствие клиентов и посетителей Бизнес-центра;</li>
                    <li>Оформление и регистрация карточек гостевых пропусков в учетной системе;</li>
                    <li>Контроль состояния зоны ресепшен и клиентской зоны.</li>
                </ul>
                <h5>Требования:</h5>
                <ul>
                    <li>Готовность придерживаться дресс-кода; <br> <br></li>
                </ul>
                <h5>Условия:</h5>
                <ul>
                    <li>Официальное трудоустройство по ТК РФ;</li>
                    <li>График работы 2/2 с 08.00-20.00;</li>
                    <li>Место работы на выбор: м.Автозаводская (3 мин. от метро);</li>
                    <li>Стабильный, фиксированный оклад.</li>
                </ul>
                ',
            'text_en' => '<h5>Responsibility:</h5>
                <ul>
                    <li>Welcome and welcome customers and visitors of the Business Center;</li>
                    <li>Registration and registration of guest pass cards in the accounting system;</li>
                    <li>Monitoring the status of the reception area and the customer area.</li>
                </ul>
                <h5>Requirements:</h5>
                <ul>
                    <li>Willingness to adhere to the dress code; <br> <br></li>
                </ul>
                <h5>Conditions:</h5>
                <ul>
                    <li>Official employment in the Labor Code of the Russian Federation;</li>
                    <li>Schedule 2/2 from 08.00-20.00;</li>
                    <li>Place of work to choose: metro Avtozavodskaya (3 minutes from the metro);</li>
                    <li>Stable, fixed salary.</li>
                </ul>',
            'photos' => 'partner-slider-img1.png**partner-slider-img2.png**partner-slider-img3.png**partner-slider-img4.png**',
        ));

        $this->insert('partners', array(
            'name_uz' => 'Xona D',
            'name_ru' => 'Помешение Д',
            'name_en' => 'The room D',
            'text_uz' => '
                <h5>Vazifalari:</h5>
                <ul>
                    <li>Biznes-markaz mijozlari va mehmonlarini qabul qilish va tabriklash;</li>
                    <li>Buxgalteriya tizimida mehmon kartalarini ro\'yxatdan o\'tkazish va ro\'yxatdan o\'tkazish;</li>
                    <li>Qabul maydoni va mijozlar hududining holatini kuzatish.</li>
                </ul>
                <h5>Talablar:</h5>
                <ul>
                    <li>Kiyinish kodiga tayoq berishga tayyor; <br> <br></li>
                </ul>
                <h5>Shart:</h5>
                <ul>
                    <li>Rossiya Federatsiyasi Mehnat Kodeksi doirasida rasmiy ravishda ishga joylashish;</li>
                    <li>Ish vaqti 2/2 с 08.00-20.00;</li>
                    <li>Ish joyi: Avtozavodskaya metro stantsiyasi (metrogacha 3 daqiqa);</li>
                    <li>Barqaror, qat\'iy maosh.</li>
                </ul>
               ',
            'text_ru' => '
                <h5>Обязанности:</h5>
                <ul>
                    <li>Прием и приветствие клиентов и посетителей Бизнес-центра;</li>
                    <li>Оформление и регистрация карточек гостевых пропусков в учетной системе;</li>
                    <li>Контроль состояния зоны ресепшен и клиентской зоны.</li>
                </ul>
                <h5>Требования:</h5>
                <ul>
                    <li>Готовность придерживаться дресс-кода; <br> <br></li>
                </ul>
                <h5>Условия:</h5>
                <ul>
                    <li>Официальное трудоустройство по ТК РФ;</li>
                    <li>График работы 2/2 с 08.00-20.00;</li>
                    <li>Место работы на выбор: м.Автозаводская (3 мин. от метро);</li>
                    <li>Стабильный, фиксированный оклад.</li>
                </ul>
                ',
            'text_en' => '<h5>Responsibility:</h5>
                <ul>
                    <li>Welcome and welcome customers and visitors of the Business Center;</li>
                    <li>Registration and registration of guest pass cards in the accounting system;</li>
                    <li>Monitoring the status of the reception area and the customer area.</li>
                </ul>
                <h5>Requirements:</h5>
                <ul>
                    <li>Willingness to adhere to the dress code; <br> <br></li>
                </ul>
                <h5>Conditions:</h5>
                <ul>
                    <li>Official employment in the Labor Code of the Russian Federation;</li>
                    <li>Schedule 2/2 from 08.00-20.00;</li>
                    <li>Place of work to choose: metro Avtozavodskaya (3 minutes from the metro);</li>
                    <li>Stable, fixed salary.</li>
                </ul>',
            'photos' => 'partner-slider-img1.png**partner-slider-img2.png**partner-slider-img3.png**partner-slider-img4.png**',
        ));

        $this->insert('partners', array(
            'name_uz' => 'Xona E',
            'name_ru' => 'Помешение E',
            'name_en' => 'The room E',
            'text_uz' => '
                <h5>Vazifalari:</h5>
                <ul>
                    <li>Biznes-markaz mijozlari va mehmonlarini qabul qilish va tabriklash;</li>
                    <li>Buxgalteriya tizimida mehmon kartalarini ro\'yxatdan o\'tkazish va ro\'yxatdan o\'tkazish;</li>
                    <li>Qabul maydoni va mijozlar hududining holatini kuzatish.</li>
                </ul>
                <h5>Talablar:</h5>
                <ul>
                    <li>Kiyinish kodiga tayoq berishga tayyor; <br> <br></li>
                </ul>
                <h5>Shart:</h5>
                <ul>
                    <li>Rossiya Federatsiyasi Mehnat Kodeksi doirasida rasmiy ravishda ishga joylashish;</li>
                    <li>Ish vaqti 2/2 с 08.00-20.00;</li>
                    <li>Ish joyi: Avtozavodskaya metro stantsiyasi (metrogacha 3 daqiqa);</li>
                    <li>Barqaror, qat\'iy maosh.</li>
                </ul>
                ',
            'text_ru' => '
                <h5>Обязанности:</h5>
                <ul>
                    <li>Прием и приветствие клиентов и посетителей Бизнес-центра;</li>
                    <li>Оформление и регистрация карточек гостевых пропусков в учетной системе;</li>
                    <li>Контроль состояния зоны ресепшен и клиентской зоны.</li>
                </ul>
                <h5>Требования:</h5>
                <ul>
                    <li>Готовность придерживаться дресс-кода; <br> <br></li>
                </ul>
                <h5>Условия:</h5>
                <ul>
                    <li>Официальное трудоустройство по ТК РФ;</li>
                    <li>График работы 2/2 с 08.00-20.00;</li>
                    <li>Место работы на выбор: м.Автозаводская (3 мин. от метро);</li>
                    <li>Стабильный, фиксированный оклад.</li>
                </ul>
               ',
            'text_en' => '<h5>Responsibility:</h5>
                <ul>
                    <li>Welcome and welcome customers and visitors of the Business Center;</li>
                    <li>Registration and registration of guest pass cards in the accounting system;</li>
                    <li>Monitoring the status of the reception area and the customer area.</li>
                </ul>
                <h5>Requirements:</h5>
                <ul>
                    <li>Willingness to adhere to the dress code; <br> <br></li>
                </ul>
                <h5>Conditions:</h5>
                <ul>
                    <li>Official employment in the Labor Code of the Russian Federation;</li>
                    <li>Schedule 2/2 from 08.00-20.00;</li>
                    <li>Place of work to choose: metro Avtozavodskaya (3 minutes from the metro);</li>
                    <li>Stable, fixed salary.</li>
                </ul>',
            'photos' => 'partner-slider-img1.png**partner-slider-img2.png**partner-slider-img3.png**partner-slider-img4.png**',
        ));

        $this->insert('partners', array(
            'name_uz' => 'Xona F',
            'name_ru' => 'Помешение F',
            'name_en' => 'The room F',
            'text_uz' => '
                <h5>Vazifalari:</h5>
                <ul>
                    <li>Biznes-markaz mijozlari va mehmonlarini qabul qilish va tabriklash;</li>
                    <li>Buxgalteriya tizimida mehmon kartalarini ro\'yxatdan o\'tkazish va ro\'yxatdan o\'tkazish;</li>
                    <li>Qabul maydoni va mijozlar hududining holatini kuzatish.</li>
                </ul>
                <h5>Talablar:</h5>
                <ul>
                    <li>Kiyinish kodiga tayoq berishga tayyor; <br> <br></li>
                </ul>
                <h5>Shart:</h5>
                <ul>
                    <li>Rossiya Federatsiyasi Mehnat Kodeksi doirasida rasmiy ravishda ishga joylashish;</li>
                    <li>Ish vaqti 2/2 с 08.00-20.00;</li>
                    <li>Ish joyi: Avtozavodskaya metro stantsiyasi (metrogacha 3 daqiqa);</li>
                    <li>Barqaror, qat\'iy maosh.</li>
                </ul>
                ',
            'text_ru' => '
                <h5>Обязанности:</h5>
                <ul>
                    <li>Прием и приветствие клиентов и посетителей Бизнес-центра;</li>
                    <li>Оформление и регистрация карточек гостевых пропусков в учетной системе;</li>
                    <li>Контроль состояния зоны ресепшен и клиентской зоны.</li>
                </ul>
                <h5>Требования:</h5>
                <ul>
                    <li>Готовность придерживаться дресс-кода; <br> <br></li>
                </ul>
                <h5>Условия:</h5>
                <ul>
                    <li>Официальное трудоустройство по ТК РФ;</li>
                    <li>График работы 2/2 с 08.00-20.00;</li>
                    <li>Место работы на выбор: м.Автозаводская (3 мин. от метро);</li>
                    <li>Стабильный, фиксированный оклад.</li>
                </ul>
              ',
            'text_en' => '<h5>Responsibility:</h5>
                <ul>
                    <li>Welcome and welcome customers and visitors of the Business Center;</li>
                    <li>Registration and registration of guest pass cards in the accounting system;</li>
                    <li>Monitoring the status of the reception area and the customer area.</li>
                </ul>
                <h5>Requirements:</h5>
                <ul>
                    <li>Willingness to adhere to the dress code; <br> <br></li>
                </ul>
                <h5>Conditions:</h5>
                <ul>
                    <li>Official employment in the Labor Code of the Russian Federation;</li>
                    <li>Schedule 2/2 from 08.00-20.00;</li>
                    <li>Place of work to choose: metro Avtozavodskaya (3 minutes from the metro);</li>
                    <li>Stable, fixed salary.</li>
                </ul>',
            'photos' => 'partner-slider-img1.png**partner-slider-img2.png**partner-slider-img3.png**partner-slider-img4.png**',
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('partners');
    }
}
