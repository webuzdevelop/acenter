<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%about}}`.
 */
class m190702_135231_create_about_table extends Migration
{
    public function up()
    {
        $this->createTable('about', [
            'id' => $this->primaryKey(),
            'logo' => $this->string(255)->comment('Логотип'),
            'email' => $this->string(255)->comment('E-mail'),
            'phone' => $this->string(255)->comment('Телефон'),
            'google' => $this->string(255)->comment('Google'),
            'instagram' => $this->string(255)->comment('Instagram'),
            'facebook' => $this->string(255)->comment('Facebook'),
            'twitter' => $this->string(255)->comment('Twitter'),
            'coordinate_x' => $this->string(255)->comment('Coordinate X'),
            'coordinate_y' => $this->string(255)->comment('Coordinate Y'),
            'address_uz' => $this->text()->comment('Manzil'),
            'address_ru' => $this->text()->comment('Адрес'),
            'address_en' => $this->text()->comment('Address'),
            'title_uz' => $this->string()->comment('Sarlavha'),
            'title_ru' => $this->string()->comment('Заголовок'),
            'title_en' => $this->string()->comment('Title'),
            'image_about_company' => $this->string(255)->comment('Изображение о компании'),
            'about_text_uz' => $this->text()->comment('Kompaniya haqida'),
            'about_text_ru' => $this->text()->comment('О компании'),
            'about_text_en' => $this->text()->comment('About company'),
            'text_uz' => $this->text()->comment('Matn'),
            'text_ru' => $this->text()->comment('Текст'),
            'text_en' => $this->text()->comment('Text'),
            'partners_text_uz' => $this->text()->comment('Hamkor matni'),
            'partners_text_ru' => $this->text()->comment('Текст Партнера'),
            'partners_text_en' => $this->text()->comment('Partner Text'),
            'footer_text_uz' => $this->text()->comment('Futer Matni'),
            'footer_text_ru' => $this->text()->comment('Текст футера'),
            'footer_text_en' => $this->text()->comment('Footer Text'),

        ]);

        $this->insert('about',array(
            'logo' => 'logo.png',
            'email' => 'a-center@gmail.com',
            'phone' => '+998946352332',
            'google' => 'https://google.com',
            'instagram' => 'https://instagram.com',
            'facebook' => 'https://facebook.com',
            'twitter' => 'https://twitter.com',
            'coordinate_x' => '41.314354',
            'coordinate_y' => '69.265480',
            'address_uz' => 'Shayhontohur tuman, massiv 14, 2-dom',
            'address_ru' => 'Шайхонтохурский район, массив Ц 14, дом 2',
            'address_en' => 'district Shayhantahur, massive 14, The House 2',
            'title_uz' => 'Sizning tanlovingiz bo\'lgan A-Center ga xush kelibsiz. Siz bu yerda yoqimli va do\'stona muhitda professional uchrashuvlarni amalga oshirasiz.',
            'title_ru' =>'Мы рады приветствовать вас в офисе A-Center по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке.',
            'title_en' => 'We welcome you to the A-Center office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere.',
            'image_about_company' => 'image_about_company.png',
            'about_text_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'about_text_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'about_text_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'text_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'text_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках.',
            'text_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'partners_text_uz' => 'Sizni tanlagan qulay ofisga xush kelibsiz. Siz do\'stona va qulay muhitda professional uchrashuvni o\'tkazasiz. Kunni rohatlaning, siz yaxshi qo\'llardasiz.',
            'partners_text_ru' => 'Мы рады приветствовать вас в офисе easyys по вашему выбору. Вас встретит профессиональная встреча, в дружеской и непринужденной обстановке. Приятного вам свидания, вы в хороших руках. ',
            'partners_text_en' => 'We welcome you to the easyys office of your choice. You will meet a professional meeting in a friendly and relaxed atmosphere. Enjoy your date, you are in good hands.',
            'footer_text_uz' => 'Saytda e\'lon qilingan materiallarning barcha huquqlari PIK kompaniyalar Guruhi Kompaniyalariga tegishli. © 2000 - 2019. Ushbu saytda taqdim etilgan har qanday ma\'lumot faqat axborot maqsadida bo\'ladi va Rossiya Federatsiyasi Fuqarolik Kodeksining 437-moddasida belgilangan har qanday shartlarda ochiq taklif emas.
"PIK" kompaniyalar Guruhi PJSC-ning axborotni oshkor qilishni Internetda shuningdek, Rossiya Federatsiyasi Markaziy banki tomonidan axborotni oshkor qilish uchun akkreditatsiya qilingan axborot agentligining sahifasida topish mumkin - "Interfaks-TsRKI" MChJ
"PIK-Korporeyshn" mas\'uliyati cheklangan jamiyati (PIK kompaniyalari PJSC tomonidan nazorat qilinadigan) axborotlarini oshkor qilishni Internetda axborot agentligining sahifasida topish mumkin,',
            'footer_text_ru' => 'Все права на публикуемые на сайте материалы принадлежат ПАО «Группа Компаний ПИК» © 2000 — 2019. Любая информация, представленная на данном сайте, носит исключительно информационный характер и ни при каких условиях не является публичной офертой, определяемой положениями статьи 437 ГК РФ.
Раскрытие информации ПАО «Группа Компаний ПИК» также доступно в сети Интернет на странице информационного агентства, аккредитованного ЦБ РФ на раскрытие информации – ООО«Интерфакс-ЦРКИ»
Раскрытие информации ООО «ПИК-Корпорация» (подконтрольная организация ПАО «Группа Компаний ПИК») доступно в сети Интернет на странице информационного агентства,',
            'footer_text_en' => 'All rights to materials published on the site belong to PIK Group of Companies © 2000 - 2019. Any information presented on this site is for informational purposes only and is not a public offer under any conditions defined by article 437 of the Civil Code of the Russian Federation.
Information disclosure of PIK Group of Companies PJSC is also available on the Internet on the page of an information agency accredited by the Central Bank of the Russian Federation for information disclosure - Interfax-TsRKI LLC
Information disclosure of LLC PIK-Corporation (controlled by PIK Group of Companies PJSC) is available on the Internet at the information agency page,'
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('about');
    }
}
