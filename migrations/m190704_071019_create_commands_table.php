<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%commands}}`.
 */
class m190704_071019_create_commands_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('commands', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(255)->comment('ФИО'),
            'permission' => $this->string(255)->comment('Должность'),
            'email' => $this->string(255)->comment('E-mail'),
            'photo' => $this->string(255)->comment('Фотография'),
            'description_uz' => $this->text()->comment('Qisqacha malumot'),
            'description_ru' => $this->text()->comment('Описание'),
            'description_en' => $this->text()->comment('Description'),
            'phone' => $this->string(255)->comment('Телефон'),
        ]);

        $this->insert('commands',array(
        'fio' => 'Иванов Иван Иванович',
        'permission' => 'Директор',
        'email' => ' a-center@gmail.com',
        'photo' => 'commands_img1.png',
        'description_uz' => 'Tutaevskoye magistralida 8-sonli A-Markazga tashrif buyurishingiz va orzularingizni ro\'yobga chiqarish uchun vaqtingiz bor.',
        'description_ru' => 'Осталось 4 грандиозных дня, что бы придти на распродажу A-Center на Тутаевском шоссе, 8 и успеть осуществить свою мечту!',
        'description_en' => 'There are 4 grand days left to come to the A-Center sale on Tutaevskoye Highway, 8 and have time to fulfill your dream!'
    ));

        $this->insert('commands',array(
            'fio' => 'Фадеев Иван Останкович',
            'permission' => 'Директор',
            'email' => ' a-center@gmail.com',
            'photo' => 'commands_img2.png',
            'description_uz' => 'Tutaevskoye magistralida 8-sonli A-Markazga tashrif buyurishingiz va orzularingizni ro\'yobga chiqarish uchun vaqtingiz bor.',
            'description_ru' => 'Осталось 4 грандиозных дня, что бы придти на распродажу A-Center на Тутаевском шоссе, 8 и успеть осуществить свою мечту!',
            'description_en' => 'There are 4 grand days left to come to the A-Center sale on Tutaevskoye Highway, 8 and have time to fulfill your dream!'
        ));

        $this->insert('commands',array(
            'fio' => 'Баранов Руслан Радикович',
            'permission' => 'Директор',
            'email' => ' a-center@gmail.com',
            'photo' => 'commands_img3.png',
            'description_uz' => 'Tutaevskoye magistralida 8-sonli A-Markazga tashrif buyurishingiz va orzularingizni ro\'yobga chiqarish uchun vaqtingiz bor.',
            'description_ru' => 'Осталось 4 грандиозных дня, что бы придти на распродажу A-Center на Тутаевском шоссе, 8 и успеть осуществить свою мечту!',
            'description_en' => 'There are 4 grand days left to come to the A-Center sale on Tutaevskoye Highway, 8 and have time to fulfill your dream!'
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('commands');
    }
}
