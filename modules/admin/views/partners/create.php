<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Partners */

?>
<div class="partners-create">
    <?= $this->render('_form', [
        'model' => $model,
        'path' => $path,
    ]) ?>
</div>
