<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Partners */
?>
<div class="partners-update">

    <?= $this->render('_update_form', [
        'model' => $model,
        'path' => $path,
        'initialPreviewConfig' => $initialPreviewConfig,
    ]) ?>

</div>
