<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Partners */
?>
<div class="partners-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name_uz',
            'name_ru',
            'name_en',
            'text_uz:ntext',
            'text_ru:ntext',
            'text_en:ntext',
            'photos:ntext',
        ],
    ]) ?>

</div>
