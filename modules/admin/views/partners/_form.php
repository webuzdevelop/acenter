<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Partners */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partners-form padding_in_form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">Русский</a></li>
            <li><a href="#tab_2" data-toggle="tab">O'zbekcha</a></li>
            <li><a href="#tab_3" data-toggle="tab">English</a></li>
            <li><a href="#tab_4" data-toggle="tab">Фотографии</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
                <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'text_ru')->widget(CKEditor::className(), [
                    'editorOptions' => [
                        'height' => 250,
                        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ],
                ]) ?>
            </div>
            <div class="tab-pane" id="tab_2">
                <?= $form->field($model, 'name_uz')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'text_uz')->widget(CKEditor::className(), [
                    'editorOptions' => [
                        'height' => 250,
                        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ],
                ]) ?>
            </div>
            <div class="tab-pane" id="tab_3">
                <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'text_en')->widget(CKEditor::className(), [
                    'editorOptions' => [
                        'height' => 250,
                        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ],
                ]) ?>
            </div>
            <div class="tab-pane" id="tab_4">
                <?= $form->field($model, 'file[]')->widget(FileInput::classname(), [
                    'options' => ['accept' => 'image/*','multiple' => 'true'],
                    'pluginOptions' => [
                            'uploadUrl' => false,
                        'showCaption' => true,
                        'showRemove' => true,
                        'showUpload' => false,
                        'initialPreviewAsData'=>true,
                        'overwriteInitial'=>true,
                        ],
                    ]) ?>
            </div>
        </div>
    </div>


    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
