<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Sliders */
?>
<div class="sliders-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title_uz',
            'title_ru',
            'title_en',
            'text_uz:ntext',
            'text_ru:ntext',
            'text_en:ntext',
            'link',
            [
                'attribute'=>'photo',
                'width' => '70px',
                'format'=>'html',
                'value' => function ($data) {
                    if($data->photo != null)
                        $url = \Yii::getAlias('@web/uploads/sliders/').$data['photo'];
                    else
                        $url = \Yii::getAlias('@web/images/').'no-image.jpg';
                    return Html::img($url, ['width'=>'70','height'=>'50']);
                }
            ]
        ],
    ]) ?>

</div>
