<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Sliders */
/* @var $form yii\widgets\ActiveForm */
$model->photo != null ? $path = '/uploads/sliders/' . $model->photo : $path = '/images/no-image.jpg';

?>

    <div class="sliders-form padding_in_form">

        <?php $form = ActiveForm::begin(); ?>


        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">Русский</a></li>
                <li><a href="#tab_2" data-toggle="tab">O'zbekcha</a></li>
                <li><a href="#tab_3" data-toggle="tab">English</a></li>
                <li><a href="#tab_4" data-toggle="tab">Основная информация</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'text_ru')->textarea(['rows' => 6]) ?>
                </div>
                <div class="tab-pane" id="tab_2">
                    <?= $form->field($model, 'title_uz')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'text_uz')->textarea(['rows' => 6]) ?>
                </div>
                <div class="tab-pane" id="tab_3">
                    <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'text_en')->textarea(['rows' => 6]) ?>

                </div>
                <div class="tab-pane" id="tab_4">
                    <div class="row">
                        <div class="col-md-4">
                            <div id="sliders_img">
                                <?= Html::img($path, [
                                    'style' => 'width:150px; height:180px;',
                                    //'class' => 'img-circle',
                                ]) ?>
                            </div>

                            <?= $form->field($model, 'file')->fileInput(['accept' => 'image/*', 'class' => "poster23_image"]) ?>

                        </div>
                        <div class="col-md-8">
                            <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>
<?php
$this->registerJs(<<<JS

    var fileCollection = new Array();
    $(document).on('change', '.poster23_image', function(e){
        var files = e.target.files;
        $.each(files, function(i, file){
            fileCollection.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:auto; height:180px;" src="'+e.target.result+'"> ';
                $('#sliders_img').html('');
                $('#sliders_img').append(template);
            };
        });
    });
JS
);
?>