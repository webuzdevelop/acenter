<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Vacancy */
?>
<div class="vacancy-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name_uz',
            'name_ru',
            'name_en',
            'text_uz:ntext',
            'text_ru:ntext',
            'text_en:ntext',
        ],
    ]) ?>

</div>
