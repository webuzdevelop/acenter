<?php
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'name_uz',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name_ru',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'name_en',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'text_uz',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'text_ru',
        'format' => 'html',
        'contentOptions' => ['style' => 'white-space: normal;'],
        'content' => function ($data) {
            return substr($data->text_ru, 0, 250) . '...';             
        },
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'text_en',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{update} {delete}',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'',
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие?',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента'],
    ],

];   