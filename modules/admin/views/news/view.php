<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\News */
?>
<div class="news-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'fone',
                'width' => '70px',
                'format'=>'html',
                'value' => function ($data) {
                    if($data->fone != null)
                        $url = \Yii::getAlias('@web/uploads/news/').$data['fone'];
                    else
                        $url = \Yii::getAlias('@web/images/').'no-image.jpg';
                    return Html::img($url, ['width'=>'70','height'=>'50']);
                }
            ],
            [
                'attribute'=>'photo',
                'width' => '70px',
                'format'=>'html',
                'value' => function ($data) {
                    if($data->photo != null)
                        $url = \Yii::getAlias('@web/uploads/news/').$data['photo'];
                    else
                        $url = \Yii::getAlias('@web/images/').'no-image.jpg';
                    return Html::img($url, ['width'=>'70','height'=>'50']);
                }
            ],
            'date',
            'title_uz',
            'title_ru',
            'title_en',
            'text_uz:ntext',
            'text_ru:ntext',
            'text_en:ntext',
            'description_uz:ntext',
            'description_ru:ntext',
            'description_en:ntext',
        ],
    ]) ?>

</div>
