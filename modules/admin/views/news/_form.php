<?php

use johnitvn\ajaxcrud\BulkButtonWidget;
use kartik\grid\GridView;
use mihaildev\ckeditor\CKEditor;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */
$model->fone != null ? $path1 = '/uploads/news/' . $model->fone : $path1 = '/images/no-image.jpg';
$model->photo != null ? $path2 = '/uploads/news/' . $model->photo : $path2 = '/images/no-image.jpg';

?>


    <div class="about-form padding_in_form">
    <div class="grid-view is-bs3 hide-resize">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="pull-left">
                    Новости
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="rc-handle-container content">
                <?php $form = ActiveForm::begin(); ?>


                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">Русский</a></li>
                        <li><a href="#tab_2" data-toggle="tab">O'zbekcha</a></li>
                        <li><a href="#tab_3" data-toggle="tab">English</a></li>
                        <li><a href="#tab_4" data-toggle="tab">Основная информация</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'text_ru')->widget(CKEditor::className(), [
                                'editorOptions' => [
                                    'height' => 250,
                                    'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                                    'inline' => false, //по умолчанию false
                                ],
                            ]) ?>

                            <?= $form->field($model, 'description_ru')->widget(CKEditor::className(), [
                                'editorOptions' => [
                                    'height' => 250,
                                    'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                                    'inline' => false, //по умолчанию false
                                ],
                            ]) ?>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <?= $form->field($model, 'title_uz')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'text_uz')->widget(CKEditor::className(), [
                                'editorOptions' => [
                                    'height' => 250,
                                    'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                                    'inline' => false, //по умолчанию false
                                ],
                            ]) ?>

                            <?= $form->field($model, 'description_uz')->widget(CKEditor::className(), [
                                'editorOptions' => [
                                    'height' => 250,
                                    'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                                    'inline' => false, //по умолчанию false
                                ],
                            ]) ?>

                        </div>
                        <div class="tab-pane" id="tab_3">
                            <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'text_en')->widget(CKEditor::className(), [
                                'editorOptions' => [
                                    'height' => 250,
                                    'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                                    'inline' => false, //по умолчанию false
                                ],
                            ]) ?>

                            <?= $form->field($model, 'description_en')->widget(CKEditor::className(), [
                                'editorOptions' => [
                                    'height' => 250,
                                    'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                                    'inline' => false, //по умолчанию false
                                ],
                            ]) ?>

                        </div>
                        <div class="tab-pane" id="tab_4">
                            <div class="row">
                                <div class="col-md-6">
                                    <div id="news_fone_img">
                                        <?= Html::img($path1, [
                                            'style' => 'width:250px; height:180px;',
                                            //'class' => 'img-circle',
                                        ]) ?>
                                    </div>
                                    <?= $form->field($model, 'fone_file')->fileInput(['accept' => 'image/*', 'class' => "poster23_image"]) ?>
                                </div>
                                <div class="col-md-6">
                                    <div id="news_photo_img">
                                        <?= Html::img($path2, [
                                            'style' => 'width:250px; height:180px;',
                                            //'class' => 'img-circle',
                                        ]) ?>
                                    </div>
                                    <?= $form->field($model, 'photo_file')->fileInput(['accept' => 'image/*', 'class' => "poster24_image"]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <?php if (!Yii::$app->request->isAjax) { ?>
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                <?php } ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
<?php
$this->registerJs(<<<JS

    var fileCollection = new Array();
    $(document).on('change', '.poster23_image', function(e){
        var files = e.target.files;
        $.each(files, function(i, file){
            fileCollection.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:auto; height:180px;" src="'+e.target.result+'"> ';
                $('#news_fone_img').html('');
                $('#news_fone_img').append(template);
            };
        });
    });
    
    var fileCollection = new Array();
    $(document).on('change', '.poster24_image', function(e){
        var files = e.target.files;
        $.each(files, function(i, file){
            fileCollection.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:auto; height:180px;" src="'+e.target.result+'"> ';
                $('#news_photo_img').html('');
                $('#news_photo_img').append(template);
            };
        });
    });
JS
);
?>