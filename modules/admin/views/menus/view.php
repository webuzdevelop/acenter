<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Menus */
?>
<div class="menus-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'name_uz',
            'name_ru',
            'name_en',
            'title_uz',
            'title_ru',
            'title_en',
            [
                'attribute' => 'visible',
                'value' => function ($data) {
                    if ($data->visible)
                        return 'Да';
                    else return 'Нет';
                }
            ],
            'key',
        ],
    ]) ?>

</div>
