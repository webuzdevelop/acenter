<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Menus */
?>
<div class="menus-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
