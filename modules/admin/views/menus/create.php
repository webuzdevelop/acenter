<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Menus */

?>
<div class="menus-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
