<?php

use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Menus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menus-form padding_in_form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"> <a href="#tab_1" data-toggle="tab">Русский</a> </li>
            <li><a href="#tab_2" data-toggle="tab">O'zbekcha</a></li>
            <li><a href="#tab_3" data-toggle="tab">English</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
                <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'title_ru')->textarea(['rows' => 6]) ?>
            </div>
            <div class="tab-pane" id="tab_2">
                <?= $form->field($model, 'name_uz')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'title_uz')->textarea(['rows' => 6]) ?>
            </div>
            <div class="tab-pane" id="tab_3">
                <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'title_en')->textarea(['rows' => 6]) ?>
            </div>
        </div>
    </div>
                    <?= $form->field($model, 'visible')->checkbox(); ?>


    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
