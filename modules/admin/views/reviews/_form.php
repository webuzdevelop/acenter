<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Reviews */
/* @var $form yii\widgets\ActiveForm */
$model->avatar != null ? $path = '/uploads/review/' . $model->avatar : $path = '/images/no-image.jpg';
?>

    <div class="reviews-form padding_in_form">

        <?php $form = ActiveForm::begin(); ?>



        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">Русский</a></li>
                <li><a href="#tab_2" data-toggle="tab">O'zbekcha</a></li>
                <li><a href="#tab_3" data-toggle="tab">English</a></li>
                <li><a href="#tab_4" data-toggle="tab">Основная информация</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <?= $form->field($model, 'permission_ru')->textInput(['maxlength' => true]) ?>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'title_ru')->textarea(['rows' => 3]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'text_ru')->textarea(['rows' => 3]) ?>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab_2">
                    <?= $form->field($model, 'permission_uz')->textInput(['maxlength' => true]) ?>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'title_uz')->textarea(['rows' => 3]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'text_uz')->textarea(['rows' => 3]) ?>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab_3">
                    <?= $form->field($model, 'permission_en')->textInput(['maxlength' => true]) ?>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'title_en')->textarea(['rows' => 3]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'text_en')->textarea(['rows' => 3]) ?>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab_4">
                    <div class="row">
                        <div class="col-md-6">
                            <div id="avatar_img">
                                <?= Html::img($path, [
                                    'style' => 'width:180px; height:180px;',
                                    //'class' => 'img-circle',
                                ]) ?>
                            </div>
                            <?= $form->field($model, 'image')->fileInput(['accept' => 'image/*', 'class' => "poster23_image"]); ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>
<?php
$this->registerJs(<<<JS

    var fileCollection = new Array();
    $(document).on('change', '.poster23_image', function(e){
        var files = e.target.files;
        $.each(files, function(i, file){
            fileCollection.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:180px; height:180px;" src="'+e.target.result+'"> ';
                $('#avatar_img').html('');
                $('#avatar_img').append(template);
            };
        });
    });
JS
);
?>