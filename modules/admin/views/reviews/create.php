<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Reviews */

?>
<div class="reviews-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
