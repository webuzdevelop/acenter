<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Reviews */
?>
<div class="reviews-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'avatar',
                'width' => '70px',
                'format'=>'html',
                'value' => function ($data) {
                    if($data->avatar != null)
                        $url = \Yii::getAlias('@web/uploads/review/').$data['avatar'];
                    else
                        $url = \Yii::getAlias('@web/images/').'no-image.jpg';
                    return Html::img($url, ['width'=>'70','height'=>'50']);
                }
            ],
            'fio',
            'permission_uz',
            'permission_ru',
            'permission_en',
            'title_uz:ntext',
            'title_ru:ntext',
            'title_en:ntext',
            'text_uz:ntext',
            'text_ru:ntext',
            'text_en:ntext',
        ],
    ]) ?>

</div>
