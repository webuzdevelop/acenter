<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Dictionary */
?>
<div class="dictionary-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'key',
            'uzb:ntext',
            'rus:ntext',
            'eng:ntext',
        ],
    ]) ?>

</div>
