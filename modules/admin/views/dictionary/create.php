<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Dictionary */

?>
<div class="dictionary-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
