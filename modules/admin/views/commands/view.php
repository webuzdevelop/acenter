<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Commands */
?>
<div class="commands-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'fio',
            'permission',
            'email:email',
            [
                'attribute'=>'photo',
                'format'=>'html',
                'value' => function ($data) {
                    if($data->photo != null)
                        $url = \Yii::getAlias('@web/uploads/commands/').$data['photo'];
                    else
                        $url = \Yii::getAlias('@web/images/').'no-image.jpg';
                    return Html::img($url, ['width'=>'70','height'=>'50']);
                }
            ],
            'description_uz:ntext',
            'description_ru:ntext',
            'description_en:ntext',
            'phone',
        ],
    ]) ?>

</div>
