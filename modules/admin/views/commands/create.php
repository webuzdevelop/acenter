<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Commands */

?>
<div class="commands-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
