<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */
?>
<div class="feedback-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'email:email',
            'phone',
            'message:ntext',
            'date_cr',
        ],
    ]) ?>

</div>
