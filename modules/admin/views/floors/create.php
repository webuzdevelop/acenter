<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Floors */

?>
<div class="floors-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
