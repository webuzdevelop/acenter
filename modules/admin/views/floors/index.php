<?php

use mihaildev\ckeditor\CKEditor;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FloorsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


CrudAsset::register($this);

?>


<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab">Этажы</a></li>
        <li><a href="#tab_2" data-toggle="tab">Слайдер</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
            <div class="floors-index">
                <div id="ajaxCrudDatatable">
                    <?=GridView::widget([
                        'id'=>'crud-datatable1',
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'pjax'=>true,
                        'columns' => require(__DIR__.'/_columns.php'),
                        'toolbar'=> [
                            ['content'=>
                                Html::a('Создать', ['create'],
                                    ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-primary']).
                                Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Обновить']).
                                '{toggleData}'
                            ],
                        ],
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'type' => 'primary',
                            'heading' => '<i class="glyphicon glyphicon-list"></i> Список',
                            'before'=>'',
                            'after'=>BulkButtonWidget::widget([
                                                'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить все',
                                                    ["/admin/sliders/bulk-delete"] ,
                                                    [
                                                        "class"=>"btn btn-danger btn-xs",
                                                        'role'=>'modal-remote-bulk',
                                                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                                        'data-request-method'=>'post',
                                                        'data-confirm-title'=>'Подтвердите действие?',
                                                        'data-confirm-message'=>'Вы уверены что хотите удалить этих элементов'
                                                    ]),
                                            ]).
                                            '<div class="clearfix"></div>',
                        ]
                    ])?>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="tab_2">
            <div class="sliders-index">
                <div id="ajaxCrudDatatable">
                    <?=GridView::widget([
                        'id'=>'crud-datatable',
                        'dataProvider' => $dataProvider2,
                        'filterModel' => $searchModel2,
                        'pjax'=>true,
                        'columns' => require(__DIR__.'/_columns2.php'),
                        'toolbar'=> [
                            ['content'=>
                                Html::a('Создать', ['/admin/sliders/create', 'type' => 'navigation'],
                                    ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-primary']).
                                Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Обновить']).
                                '{toggleData}'
                            ],
                        ],
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'type' => 'primary',
                            'heading' => '<i class="glyphicon glyphicon-list"></i> Список',
                            'before'=>'',
                            'after'=>BulkButtonWidget::widget([
                                    'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить все',
                                        ["/admin/sliders/bulk-delete"] ,
                                        [
                                            "class"=>"btn btn-danger btn-xs",
                                            'role'=>'modal-remote-bulk',
                                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                            'data-request-method'=>'post',
                                            'data-confirm-title'=>'Подтвердите действие?',
                                            'data-confirm-message'=>'Вы уверены что хотите удалить этих элементов'
                                        ]),
                                ]).
                                '<div class="clearfix"></div>',
                        ]
                    ])?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
