<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Floors */
/* @var $form yii\widgets\ActiveForm */
$model->photo != null ? $path1 = '/uploads/floors/' . $model->photo : $path1 = '/images/no-image.jpg';
?>

<div class="floors-form padding_in_form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <div id="floors_img">
                <?= Html::img($path1, [
                    'style' => 'width:300px; height:200px;',
                    //'class' => 'img-circle',
                ]) ?>
            </div>
            <?= $form->field($model, 'file')->fileInput(['accept' => 'image/*', 'class' => "poster23_image"]) ?>
        </div>
        <div class="col-md-6">
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>

    </div>


  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
<?php
$this->registerJs(<<<JS

    var fileCollection = new Array();
    $(document).on('change', '.poster23_image', function(e){
        var files = e.target.files;
        $.each(files, function(i, file){
            fileCollection.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:auto; height:180px;" src="'+e.target.result+'"> ';
                $('#floors_img').html('');
                $('#floors_img').append(template);
            };
        });
    });
JS
);
?>