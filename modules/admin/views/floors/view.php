<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Floors */
?>
<div class="floors-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [
                'attribute'=>'photo',
                'width' => '100px',
                'format'=>'html',
                'value' => function ($data) {
                    if($data->photo != null)
                        $url = \Yii::getAlias('@web/uploads/').$data['photo'];
                    else
                        $url = \Yii::getAlias('@web/images/').'no-image.jpg';
                    return Html::img($url, ['width'=>'100','height'=>'50']);
                }
            ],
        ],
    ]) ?>

</div>
