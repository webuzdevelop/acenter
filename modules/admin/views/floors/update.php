<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Floors */
?>
<div class="floors-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
