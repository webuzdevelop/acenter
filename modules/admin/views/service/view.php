<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Service */
?>
<div class="service-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'photo',
                'width' => '70px',
                'format' => 'html',
                'value' => function ($data) {
                    if ($data->photo != null)
                        $url = \Yii::getAlias('@web/uploads/service/') . $data['photo'];
                    else
                        $url = \Yii::getAlias('@web/images/') . 'no-image.jpg';
                    return Html::img($url, ['width' => '70', 'height' => '50']);
                }
            ],
            'price',
            'name_uz',
            'name_ru',
            'name_en',
            [
                'attribute' => 'text_uz',
                'format' => 'html'
            ],
            [
                'attribute' => 'text_ru',
                'format' => 'html'
            ],
            [
                'attribute' => 'text_en',
                'format' => 'html'
            ],
        ],
    ]) ?>

</div>
