<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Shops */

?>
<div class="shops-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
