<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Shops */
?>
<div class="shops-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'logo',
                'width' => '70px',
                'format' => 'html',
                'value' => function ($data) {
                    if ($data->logo != null)
                        $url = \Yii::getAlias('@web/uploads/shops/') . $data['logo'];
                    else
                        $url = \Yii::getAlias('@web/images/') . 'no-image.jpg';
                    return Html::img($url, ['width' => '70', 'height' => '50']);
                }
            ],
            [
                'attribute'=>'photo1',
                'width' => '70px',
                'format' => 'html',
                'value' => function ($data) {
                    if ($data->photo1 != null)
                        $url = \Yii::getAlias('@web/uploads/shops/') . $data['photo1'];
                    else
                        $url = \Yii::getAlias('@web/images/') . 'no-image.jpg';
                    return Html::img($url, ['width' => '70', 'height' => '50']);
                }
            ],
            [
                'attribute'=>'photo2',
                'width' => '70px',
                'format' => 'html',
                'value' => function ($data) {
                    if ($data->photo2 != null)
                        $url = \Yii::getAlias('@web/uploads/shops/') . $data['photo2'];
                    else
                        $url = \Yii::getAlias('@web/images/') . 'no-image.jpg';
                    return Html::img($url, ['width' => '70', 'height' => '50']);
                }
            ],
            [
                'attribute' => 'floor_id',
                'value' => $model->floors->name
            ],
            'name',
            'title_uz',
            'title_ru',
            'title_en',
            'text_uz:ntext',
            'text_ru:ntext',
            'text_en:ntext',
            [
                'attribute' => 'text_top_uz',
                'format' => 'html'
            ],
            [
                'attribute' => 'text_top_ru',
                'format' => 'html'
            ],
            [
                'attribute' => 'text_top_en',
                'format' => 'html'
            ],
            [
                'attribute' => 'text_bottom_uz',
                'format' => 'html'
            ],
            [
                'attribute' => 'text_bottom_en',
                'format' => 'html'
            ],
            [
                'attribute' => 'text_top_ru',
                'format' => 'html'
            ]
        ],
    ]) ?>

</div>
