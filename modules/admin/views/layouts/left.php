<?php

use app\models\Users;
use yii\helpers\Url;
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <?php
        if(isset(Yii::$app->user->identity->id))
        {
            $role = Yii::$app->user->identity->role_id;
            $id = Yii::$app->user->identity->id;
            echo dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                    'items' => [
                        ['label' => 'Menu', 'options' => ['class' => 'header']],
                        ['label' => 'Настройки', 'icon' => 'unlock', 'items' => [
                            ['label' => 'Пользователи', 'icon' => 'users', 'url' => ['/admin/users']],
                            ['label' => 'О компании', 'icon' => 'book', 'url' => ['/admin/about']],
                            ['label' => 'Меню', 'icon' => 'bars', 'url' => ['/admin/menus']],
                            ['label' => 'Словарь', 'icon' => 'book', 'url' => ['/admin/dictionary']],
                        ]],
                        ['label' => 'Вакансии', 'icon' => 'user-plus', 'url' => ['/admin/vacancy']],
                        ['label' => 'Партнеры', 'icon' => 'users', 'url' => ['/admin/partners']],
                        ['label' => 'Обратная связь', 'icon' => 'envelope', 'url' => ['/admin/feedback']],
                        ['label' => 'Команды', 'icon' => 'group', 'url' => ['/admin/commands']],
                        ['label' => 'Отзывы', 'icon' => 'search', 'url' => ['/admin/reviews']],
                        ['label' => 'Слайдер', 'icon' => 'exchange', 'url' => ['/admin/sliders']],
                        ['label' => 'Новости', 'icon' => 'newspaper-o', 'url' => Url::to(['/admin/news', 'con' => 1])],
                        ['label' => 'Акции', 'icon' => 'newspaper-o', 'url' => Url::to(['/admin/news', 'con' => 2])],
                        ['label' => 'Услуги', 'icon' => 'cogs', 'url' => ['/admin/service']],
                        ['label' => 'Магазины', 'icon' => 'shopping-cart', 'url' => ['/admin/shops']],
                        ['label' => 'Этажы', 'icon' => 'bars', 'url' => ['/admin/floors']],
                        ['label' => 'Язык', 'icon' => 'book', 'url' => ['/admin/languages']],
                    ],
                ]
            );
        }
        ?>

    </section>

</aside>
