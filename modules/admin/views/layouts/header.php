<?php
use yii\helpers\Html;
use app\models\Soglosovaniya;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;
use yii\helpers\Url;
use app\models\Settings;

?>

    <header class="main-header">

        <?= Html::a('<span class="logo-mini">APP</span><span class="logo-lg">' . 'A-Center' /*Yii::$app->name*/ . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

        <nav class="navbar navbar-static-top" role="navigation">

            <a href="#" onclick="$.post('/site/menu-position');" class="sidebar-toggle" data-toggle="push-menu" role="button"><span class="sr-only">Toggle navigation</span> </a>

            <div class="navbar-custom-menu">

                <ul class="nav navbar-nav">

                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="hidden-xs"><?=Yii::$app->user->identity->name ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header">
                                <img src="/images/nouser.png" class="img-circle" alt="User Image"/>
                                <p> <?=Yii::$app->user->identity->name ?> </p>
                            </li>
                            <li class="user-footer">
                                <div class="pull-left">
                                    <?= Html::a('Изменить пароль', ['/admin/users/change', 'id' => Yii::$app->user->identity->id],
                                        ['role'=>'modal-remote','title'=> 'Изменить пароль','class'=>'btn btn-default btn-flat']); ?>
                                </div>
                                <div class="pull-right">
                                    <?= Html::a(
                                        'Выход',
                                        ['/admin/default/logout'],
                                        ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                    ) ?>
                                </div>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </nav>
    </header>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => "modal-lg",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>