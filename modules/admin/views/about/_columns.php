<?php

use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'photo',
        'width' => '70px',
        'format'=>'html',
        'value' => function ($data) {
            if($data->photo != null)
                $url = \Yii::getAlias('@web/uploads/sliders/').$data['photo'];
            else
                $url = \Yii::getAlias('@web/images/').'no-image.jpg';
            return Html::img($url, ['width'=>'70','height'=>'50']);
        }
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'type',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'title_uz',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'title_ru',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'title_en',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'text_uz',
//    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'text_ru',
     ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'text_en',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'link',
    // ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{leadUpdate} {leadDelete}',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'',
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие?',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента'],
        'buttons'  => [ 
            'leadUpdate' => function ($url, $model) {
                    $url = Url::to(['/admin/sliders/update', 'id' => $model->id]);
                return Html::a('<span class="fa fa-pencil"></span>', $url, [ 'role'=>'modal-remote', 'title'=>'Изменить', 'class'=>"mb-control", 'data-toggle'=>'tooltip']);
            },
            'leadDelete' => function ($url, $model) {
                    $url = Url::to(['/admin/sliders/delete', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'role'=>'modal-remote','title'=>'Удалить', 
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемент?',
                    ]);
            },
        ],
    ],

];   