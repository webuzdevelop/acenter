<?php

use johnitvn\ajaxcrud\BulkButtonWidget;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use johnitvn\ajaxcrud\CrudAsset; 

/* @var $this yii\web\View */
/* @var $model app\models\About */
/* @var $form yii\widgets\ActiveForm */
CrudAsset::register($this);
?>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

    <div class="about-form padding_in_form">
    <div class="grid-view is-bs3 hide-resize">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="pull-left">
                Изменить
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="rc-handle-container content">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Основная информация</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Русский</a></li>
                    <li><a href="#tab_3" data-toggle="tab">O'zbekcha</a></li>
                    <li><a href="#tab_4" data-toggle="tab">English</a></li>
                    <li><a href="#tab_5" data-toggle="tab">Слидер</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="row">
                            <div class="col-md-6">
                                <center>
                                    <div id="user_logo_file">
                                        <?= Html::img('/images/' . $model->logo, [
                                            'style' => 'width:180px; height:180px;',
                                            //'class' => 'img-circle',
                                        ]) ?>
                                    </div>
                                    <?= $form->field($model, 'logo_image')->fileInput(['multiple' => true, 'accept' => 'image/*', 'class' => "poster23_image"]) ?>
                                </center>
                            </div>

                            <div class="col-md-6">
                                <center>
                                    <div id="image_about_file">
                                        <?= Html::img('/images/' . $model->image_about_company, [
                                            'style' => 'width:180px; height:180px;',
                                            //'class' => 'img-circle',
                                        ]) ?>
                                    </div>
                                    <?= $form->field($model, 'about_company_img')->fileInput(['multiple' => true, 'accept' => 'image/*', 'class' => "poster24_image"]) ?>
                                </center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'twitter')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'google')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($model, 'facebook')->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'instagram')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                        <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'address_ru')->textarea(['rows' => 3]) ?>
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'about_text_ru')->textarea(['rows' => 6]) ?>
                                <?= $form->field($model, 'partners_text_ru')->textarea(['rows' => 6]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'text_ru')->textarea(['rows' => 6]) ?>
                                <?= $form->field($model, 'footer_text_ru')->textarea(['rows' => 6]) ?>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->

                    <div class="tab-pane" id="tab_3">
                        <?= $form->field($model, 'title_uz')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'address_uz')->textarea(['rows' => 3]) ?>
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'text_uz')->textarea(['rows' => 6]) ?>
                                <?= $form->field($model, 'partners_text_uz')->textarea(['rows' => 6]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'about_text_uz')->textarea(['rows' => 6]) ?>
                                <?= $form->field($model, 'footer_text_uz')->textarea(['rows' => 6]) ?>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab_4">
                        <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'address_en')->textarea(['rows' => 3]) ?>
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'text_en')->textarea(['rows' => 6]) ?>
                                <?= $form->field($model, 'partners_text_en')->textarea(['rows' => 6]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'about_text_en')->textarea(['rows' => 6]) ?>
                                <?= $form->field($model, 'footer_text_en')->textarea(['rows' => 6]) ?>

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_5">
                        <div class="sliders-index">
                            <div id="ajaxCrudDatatable">
                                <?=GridView::widget([
                                    'id'=>'crud-datatable',
                                    'dataProvider' => $dataProvider,
                                    'filterModel' => $searchModel,
                                    'pjax'=>true,
                                    'columns' => require(__DIR__.'/_columns.php'),
                                    'toolbar'=> [
                                        ['content'=>
                                            Html::a('Создать', ['/admin/sliders/create', 'type' => 'about_company'],
                                                ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-primary']).
                                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                                                ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Обновить']).
                                            '{toggleData}'
                                        ],
                                    ],
                                    'striped' => true,
                                    'condensed' => true,
                                    'responsive' => true,
                                    'panel' => [
                                        'type' => 'primary',
                                        'heading' => '<i class="glyphicon glyphicon-list"></i> Список',
                                        'before'=>'',
                                        'after'=>BulkButtonWidget::widget([
                                                'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить все',
                                                    ["/admin/sliders/bulk-delete"] ,
                                                    [
                                                        "class"=>"btn btn-danger btn-xs",
                                                        'role'=>'modal-remote-bulk',
                                                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                                        'data-request-method'=>'post',
                                                        'data-confirm-title'=>'Подтвердите действие?',
                                                        'data-confirm-message'=>'Вы уверены что хотите удалить этих элементов'
                                                    ]),
                                            ]).
                                            '<div class="clearfix"></div>',
                                    ]
                                ])?>
                            </div>
                        </div>
                        <?php Modal::begin([
                            "id"=>"ajaxCrudModal",
                            "footer"=>"",// always need it for jquery plugin
                        ])?>
                        <?php Modal::end(); ?>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-warning']) ?>
                <?= Html::a('Назадь', '/admin/about', ['class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    </div>
<?php
$this->registerJs(<<<JS

    var fileCollection = new Array();
    $(document).on('change', '.poster23_image', function(e){
        var files = e.target.files;
        $.each(files, function(i, file){
            fileCollection.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:180px; height:180px;" src="'+e.target.result+'"> ';
                $('#user_logo_file').html('');
                $('#user_logo_file').append(template);
            };
        });
    });
    
    var fileCollections = new Array();
    $(document).on('change', '.poster24_image', function(e){
        var files = e.target.files;
        $.each(files, function(i, file){
            fileCollections.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:180px; height:180px;" src="'+e.target.result+'"> ';
                $('#image_about_file').html('');
                $('#image_about_file').append(template);
            };
        });
    });

JS
);
?>