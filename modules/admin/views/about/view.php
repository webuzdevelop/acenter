<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\About */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Abouts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="about-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute'=>'logo',
                'format'=>'html',
                'value' => function ($data) {
                    if($data->logo != null)
                        $url = \Yii::getAlias('@web/uploads/').$data['logo'];
                    else
                        $url = \Yii::getAlias('@web/images/').'no-image.jpg';
                    return Html::img($url, ['width'=>'70','height'=>'50']);
                }
            ],
            'email:email',
            'phone',
            'google',
            'instagram',
            'facebook',
            'twitter',
            'coordinate_x',
            'coordinate_y',
            'address_uz:ntext',
            'address_ru:ntext',
            'address_en:ntext',
            'title_uz',
            'title_ru',
            'title_en',
            'text_left_uz:ntext',
            'text_left_ru:ntext',
            'text_left_en:ntext',
            'text_right_uz:ntext',
            'text_right_ru:ntext',
            'text_right_en:ntext',
        ],
    ]) ?>

</div>
