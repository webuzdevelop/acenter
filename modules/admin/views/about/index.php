<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use katzz0\yandexmaps\Map;
use katzz0\yandexmaps\JavaScript;
use katzz0\yandexmaps\objects\Placemark;
use katzz0\yandexmaps\Polyline;
use katzz0\yandexmaps\Point;
use katzz0\yandexmaps\Canvas as YandexMaps;

/* @var $this yii\web\View */
/* @ var $searchModel app\models\AboutSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'О компании';
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
$lang = $session['language'] == null ? 'ru' : $session['language'];

?>
<div class="about-index" style="margin-top: 10px">
    <div class="grid-view is-bs3 hide-resize">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="pull-left">
                    <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> Изменить',Url::to(['/admin/about/update']), ['class' => 'btn btn-block btn-warning btn-xs']) ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="rc-handle-container content-header">
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <tbody>
                        <tr>
                            <td rowspan="2"><img src="<?= '/images/'.$model->logo?>" alt="" style="width: 70px; height: 60px"></td>
                            <th style="color: #00a7d0"><?=$model->getAttributeLabel('email')?></th>
                            <td><?= $model->email?></td>
                            <th style="color: #00a7d0"><?=$model->getAttributeLabel('phone')?></th>
                            <td><?= $model->phone?></td>
                            <th style="color: #00a7d0"><?=$model->getAttributeLabel('address_ru')?></th>
                            <td><?= $model->address_ru?></td>
                        </tr>
                        <tr>
                            <th style="color: #00a7d0"><?=$model->getAttributeLabel('instagram')?></th>
                            <td><?= $model->instagram?></td>
                            <th style="color: #00a7d0"><?=$model->getAttributeLabel('facebook')?></th>
                            <td><?= $model->facebook?></td>
                            <th style="color: #00a7d0"><?=$model->getAttributeLabel('twitter')?></th>
                            <td><?= $model->twitter?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="grid-view is-bs3 hide-resize">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="pull-left">
                    Адрес
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="rc-handle-container">
                <?= YandexMaps::widget([
                    'htmlOptions' => [
                        'style' => 'height: 400px;',
                    ],
                    'map' => new Map('yandex_map', [
                        'center' => [$model->coordinate_x, $model->coordinate_y],
                        'zoom' => 12,
                        'controls' => [Map::CONTROL_ZOOM],
                        'behaviors' => [Map::BEHAVIOR_DRAG],
                        'type' => "yandex#map",
                    ],
                        [
                            'objects' => [
                                new Placemark(new Point($model->coordinate_x, $model->coordinate_y), [], [
                                    'draggable' => true,
                                    'preset' => 'islands#dotIcon',
                                    'iconColor' => 'red',
                                    'events' => [
                                        'dragend' => 'js:function (e) {
                                        //console.log(e.get(\'target\').geometry.getCoordinates());                                        
                                        var coords = e.get(\'target\').geometry.getCoordinates();
                                        //$( "#contacts-coordinate_x" ).val( coords[0]);
                                        //$( "#contacts-coordinate_y" ).val( coords[1]);
                                        $.get("/'.$lang.'/admin/about/set-coordinates",
                                        { "id" : 1, "coordinate_x" : coords[0], "coordinate_y" : coords[1] },
                                            function(data){ }
                                        );
                                    }',
                                    ]
                                ])
                            ]
                        ])
                ]) ?>

            </div>
        </div>
