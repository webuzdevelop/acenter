<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Shops;
use app\models\ShopsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * ShopsController implements the CRUD actions for Shops model.
 */
class ShopsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Shops models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new ShopsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Shops model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> 'Магазины',
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Shops model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Shops();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                $model->file = UploadedFile::getInstance($model, 'file');
                $model->photo1_file = UploadedFile::getInstance($model, 'photo1_file');
                $model->photo2_file = UploadedFile::getInstance($model, 'photo2_file');
                if(!empty($model->file))
                {
                    $model->file->saveAs('uploads/shops/' . time(). 'shops.' . $model->file->extension);
                    Yii::$app->db->createCommand()->update('shops', ['logo' =>time(). 'shops.' . $model->file->extension ], [ 'id' => $model->id ])->execute();
                }

                if(!empty($model->photo1_file))
                {
                    $model->photo1_file->saveAs('uploads/shops/' . time(). 'shops_photo1.' . $model->photo1_file->extension);
                    Yii::$app->db->createCommand()->update('shops', ['photo1' =>time(). 'shops_photo1.' . $model->photo1_file->extension ], [ 'id' => $model->id ])->execute();
                }

                if(!empty($model->photo2_file))
                {
                    $model->photo2_file->saveAs('uploads/shops/' . time(). 'shops_photo2.' . $model->photo2_file->extension);
                    Yii::$app->db->createCommand()->update('shops', ['photo2' =>time(). 'shops_photo2.' . $model->photo2_file->extension ], [ 'id' => $model->id ])->execute();
                }
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> 'Магазины',
                    'content'=>'<span class="text-success">Успешно выполнено</span>',
                    'footer'=> Html::button('Ок',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Создать",
                    'size'=> 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                
                $model->file = UploadedFile::getInstance($model, 'file');
                $model->photo1_file = UploadedFile::getInstance($model, 'photo1_file');
                $model->photo2_file = UploadedFile::getInstance($model, 'photo2_file');
                if(!empty($model->file))
                {
                    $model->file->saveAs('uploads/shops/' . time(). 'shops.' . $model->file->extension);
                    Yii::$app->db->createCommand()->update('shops', ['logo' =>time(). 'shops.' . $model->file->extension ], [ 'id' => $model->id ])->execute();
                }

                if(!empty($model->photo1_file))
                {
                    $model->photo1_file->saveAs('uploads/shops/' . time(). 'shops_photo1.' . $model->photo1_file->extension);
                    Yii::$app->db->createCommand()->update('shops', ['photo1' =>time(). 'shops_photo1.' . $model->photo1_file->extension ], [ 'id' => $model->id ])->execute();
                }

                if(!empty($model->photo2_file))
                {
                    $model->photo2_file->saveAs('uploads/shops/' . time(). 'shops_photo2.' . $model->photo2_file->extension);
                    Yii::$app->db->createCommand()->update('shops', ['photo2' =>time(). 'shops_photo2.' . $model->photo2_file->extension ], [ 'id' => $model->id ])->execute();
                }

                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Shops model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                $model->file = UploadedFile::getInstance($model, 'file');
                $model->photo1_file = UploadedFile::getInstance($model, 'photo1_file');
                $model->photo2_file = UploadedFile::getInstance($model, 'photo2_file');
                if(!empty($model->file))
                {
                    $model->file->saveAs('uploads/shops/' . strtotime('nov'). 'shops.' . $model->file->extension);
                    Yii::$app->db->createCommand()->update('shops', ['logo' =>strtotime('nov'). 'shops.' . $model->file->extension ], [ 'id' => $model->id ])->execute();
                }

                if(!empty($model->photo1_file))
                {
                    $model->photo1_file->saveAs('uploads/shops/' . strtotime('nov'). 'shops_photo1.' . $model->photo1_file->extension);
                    Yii::$app->db->createCommand()->update('shops', ['photo1' =>strtotime('nov'). 'shops_photo1.' . $model->photo1_file->extension ], [ 'id' => $model->id ])->execute();
                }

                if(!empty($model->photo2_file))
                {
                    $model->photo2_file->saveAs('uploads/shops/' . strtotime('nov'). 'shops_photo2.' . $model->photo2_file->extension);
                    Yii::$app->db->createCommand()->update('shops', ['photo2' =>strtotime('nov'). 'shops_photo2.' . $model->photo2_file->extension ], [ 'id' => $model->id ])->execute();
                }
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> 'Магазин',
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                     'title'=> "Изменить",
                     'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Shops model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Shops model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Shops model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Shops the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Shops::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
