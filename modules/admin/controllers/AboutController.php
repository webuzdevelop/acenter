<?php

namespace app\modules\admin\controllers;

use app\models\Sliders;
use app\models\SlidersSearch;
use Yii;
use app\models\About;
use app\models\AboutSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * AboutController implements the CRUD actions for About model.
 */
class AboutController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all About models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = About::findOne(1);
        return $this->render('index', ['model' => $model]);
    }

    /**
     * Displays a single About model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id)
        ]);
    }

    /**
     * Updates an existing About model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate()
    {
        $searchModel = new SlidersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'about');
        $model = $this->findModel(1);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->logo_image = UploadedFile::getInstance($model, 'logo_image');
            $model->about_company_img = UploadedFile::getInstance($model, 'about_company_img');
            if(!empty($model->logo_image))
            {
                $model->logo_image->saveAs('images/' . 'logo' .time().'.'. $model->logo_image->extension);
                Yii::$app->db->createCommand()->update('about', ['logo' => 'logo' .time().'.'. $model->logo_image->extension ], [ 'id' => 1 ])->execute();
            }
            if(!empty($model->about_company_img))
            {
                $model->about_company_img->saveAs('images/' . 'about'.time().'.' . $model->about_company_img->extension);
                Yii::$app->db->createCommand()->update('about', ['image_about_company' =>'about'.time().'.'.$model->about_company_img->extension],['id'=>1])->execute();
            }


            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the About model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return About the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = About::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSetCoordinates($id, $coordinate_x, $coordinate_y)
    {
        $model = About::findOne($id);
        $model->coordinate_x = $coordinate_x;
        $model->coordinate_y = $coordinate_y;
        $model->save();
    }
}
