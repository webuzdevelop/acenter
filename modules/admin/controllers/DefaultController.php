<?php

namespace app\modules\admin\controllers;

use app\models\LoginForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','login'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    public function actions()
    {
        $this->layout = '/main';
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect('/admin/default/login');
        else return $this->redirect('/admin/users');
    }

//    public function actionAvtorizatsiya()
//    {
//        if (isset(Yii::$app->user->identity->id)) {
//            return $this->render('error');
//        } else {
//            Yii::$app->user->logout();
//            $this->redirect(['login']);
//        }
//    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {

            return $this->render('index');
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/admin/default/index');
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        $this->goHome();
    }

    public function actionInstruksiya()
    {
        return $this->render('instruksiya');
    }

    public function actionMenuPosition()
    {
        $session = Yii::$app->session;
        if ($session['menu'] == null | $session['menu'] == 'large') $session['menu'] = 'small';
        else $session['menu'] = 'large';

        return $session['menu'];
    }

}
