<?php

namespace app\modules\admin;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        \Yii::$app->viewPath = '@app/modules/admin/views';
        $this->layout = '/main';
        parent::init();
        // custom initialization code goes here
    }
}
